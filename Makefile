CC=gcc

CFLAGS=-g -Wall `sdl2-config --cflags` -DHAVE_SDLMIXER
LFLAGS=`sdl2-config --libs` -lpng -lSDL2_mixer

RUNE_PATH=src

SOURCES= \
	$(RUNE_PATH)/libs/savepng/savepng.c \
	$(RUNE_PATH)/libs/binaryheap/binhl.c \
	$(RUNE_PATH)/libs/libcsslike/hash/hash.c \
	$(RUNE_PATH)/libs/libcsslike/hash/stash.c \
	$(RUNE_PATH)/libs/libcsslike/cssdom.c \
	$(RUNE_PATH)/libs/lazyass/lazyass.c \
	$(RUNE_PATH)/sdl2-x.c $(RUNE_PATH)/font2.c $(RUNE_PATH)/SDL2_particles.c \
	$(RUNE_PATH)/mainmenu.c \
	$(RUNE_PATH)/ai.c \
 	$(RUNE_PATH)/utils.c \
 	$(RUNE_PATH)/draw.c \
 	$(RUNE_PATH)/mana.c \
 	$(RUNE_PATH)/game.c \
 	$(RUNE_PATH)/ui.c \
 	$(RUNE_PATH)/runeconf.c \
 	$(RUNE_PATH)/rune2.c \
 	$(RUNE_PATH)/main.c

EXECUTABLE=rune

OBJECTS=$(SOURCES:.c=.o)

all: $(SOURCES) $(EXECUTABLE)

vendor:
	git submodule update --init --recursive
	git submodule foreach git checkout master
	git submodule foreach git pull

src/font2.c:
	make -C src/libs/SDL_inprint font2.c
	cp src/libs/SDL_inprint/font2.c src/.

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(CFLAGS) $(LFLAGS) $(OBJECTS) -o $@

.c.o: $<
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf src/*.o *.a $(EXECUTABLE) 
	
vendor-clean:
	rm -rf \
		src/libs/lazyass/*.o \
	 	src/libs/binaryheap/*.o \
	 	src/libs/libcsslike/*.o \
	 	src/libs/SDL_inprint/*.o \
		src/libs/SDL_inprint/font2.c \
	 	src/font2.c \
	 	src/libs/savepng/*.o