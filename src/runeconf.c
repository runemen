#include <stdio.h>
#include <SDL.h>

#include "libs/lazyass/lazyass.h"
#include "libs/libcsslike/cssdom.h"

#include "rune.h"
#include "runeconf.h"
#include "draw.h"
#include "visual.h"
#include "ai.h"

unit_p bunits[MAX_BUNITS];
house_p bhouses[MAX_BHOUSES];

Uint16 num_bunits;
Uint16 num_bhouses;

char pseudo_marker[80];

char unit_classes[MAX_BUNITS][128];
char house_classes[MAX_BHOUSES][128];

#define TL_OR_PIX(NAM, MULT) \
	if (NAM ## U[0] == 't' && NAM ## U[1] == 0) NAM = NAM * MULT; \
	else if (NAM ## U[0] != 'p' || NAM ## U[1] != 'x' || NAM ## U[2] != 0) { \
		fprintf(stderr, "Unknown unit '%s', please use 't' or 'px'\n", NAM ## U); \
		return -1; \
	}

void wipe_animation(animation_t *anim) {

	anim->num_axises = 0;

	anim->plane.x = anim->plane.y = 0;
	anim->plane.w = TILE_W;
	anim->plane.h = TILE_H;

}

void compute_houses() {

	int i;

	for (i = 0; i < MAX_BHOUSES; i++) {

		house_p *housep = &bhouses[i];

		housep->w = housep->body.plane.w / TILE_W;
		housep->h = housep->body.plane.h / TILE_H;

	}

}

void compute_units() {

	int i, j;

	for (i = 0; i < num_bunits; i++) {

		unit_p *unitp = &bunits[i];

		unitp->w = unitp->body.plane.w / TILE_W;
		unitp->h = unitp->body.plane.h / TILE_H;

		/* Copy body to face, but replace "frame" ref with "faceframe" ref */
		memcpy(&unitp->face, &unitp->body, sizeof(animation_t));
		for (j = 0; j < MAX_AAXIS; j++) {
			if (unitp->face.axis_modifier[j] == UAXIS_FRAME) {
				unitp->face.axis_modifier[j] = UAXIS_FACEFRAME;
			}
		} 
	}

}

/** Sprites... **/
void rcfg_parse_frames(animation_t *anim, Uint8 anim_index, const char *value) {
    char cut[1024];
    int num_frames;
    int frame;
    char *next;

    strncpy(cut, value, sizeof(cut) - 1);

	num_frames = 0;
	next = strtok(cut, ",");
	while (next) {
		frame = atoi(next);
		//printf("Animation %d, frame %d --> %d\n", anim_index, nf, fr);
		anim->frame[anim_index][num_frames] = frame;
		next = strtok(NULL, ",");
		num_frames++;
	}
	anim->num_frames[anim_index] = num_frames;
}

int rcfg_apply_animation(animation_t *anim, const char *name, const char *value) {

	int err = 0;

#define _eq(STR) (!strcasecmp(name, STR))
#define _veq(STR) (!strcasecmp(value, STR))
#define _sveq(STR) (!strcasecmp(subvalue, STR))

	if (_eq("image")) {

		anim->image = ASS_LoadTexture(value, &white);
		printf("Loading image: %s for someone...\n", value);

	}
	else
	if (_eq("tile-size")) {
		int a, b;
		char aU[4], bU[4];
		if (sscanf(value, "%d%s %d%s", &a, aU, &b, bU) == 4) {
			TL_OR_PIX(a, TILE_W);
			TL_OR_PIX(b, TILE_H);
		} else printf("Tile size must in Nu Nu format (N=number, u=unit(px,t))\n");
		anim->plane.w = a;
		anim->plane.h = b;
		//printf("Unit %d plane: %d, %d\n", id, unitp->plane.x, unitp->plane.y);
	}
	else
	if (_eq("tile-plane")) {
		int a, b, c, d;
		char aU[4], bU[4], cU[4], dU[4];
		//printf("Scanning '%s'\n", value);
		if (sscanf(value, "%d%s %d%s %d%s %d%s", &a, aU, &b, bU, &c, cU, &d, dU) == 8) {
			TL_OR_PIX(a, TILE_W);
			TL_OR_PIX(b, TILE_H);
			TL_OR_PIX(c, TILE_W);
			TL_OR_PIX(d, TILE_H);
			anim->plane.x = a;
			anim->plane.y = b;
			anim->plane.w = c;
			anim->plane.h = d;
		} else if (sscanf(value, "%d%s %d%s", &a, aU, &b, bU) == 4) {
			TL_OR_PIX(a, TILE_W);
			TL_OR_PIX(b, TILE_H);
			c = 512;
			d = 512;
			anim->plane.x = a;
			anim->plane.y = b;
		} else { printf("Tile plane must in Nu Nu or Nu Nu Nu Nu format (N=number, u=unit(px,t))\n"); return -1; }

		//printf("Unit %d plane: %d, %d\n", 0, anim->plane.x, anim->plane.y);
	}
	else
	if (_eq("tile-axis")) {
		int a, b, c, d;
		char subvalue[12], aU[4], bU[4];//, cU[4], dU[4];
		if (sscanf(value, "%s %d%s %d%s %d %d", subvalue, &a, aU, &b, bU, &c, &d) == 7) {
			TL_OR_PIX(a, TILE_W);
			TL_OR_PIX(b, TILE_H);
			c = c * anim->plane.w;
			d = d * anim->plane.h;
			//TL_OR_PIX(c, anim->plane.w);
			//TL_OR_PIX(d, anim->plane.h);
		} else if (sscanf(value, "%s %d %d", subvalue, &c, &d) == 3) {
			a = 0;
			b = 0;
			c = c * anim->plane.w;
			d = d * anim->plane.h;
			//TL_OR_PIX(c, anim->plane.w);
			//TL_OR_PIX(d, anim->plane.h);
		} else { printf("Tile axis must be in S D D or S Nu Nu D D format (D=number, N=number, u=unit(px,t), S=string)\n"); return -1;}
		//printf("Receiving offset '%s' multX %d multY %d offX %d offY %d\n",subvalue, a,b, c,d);

		Uint8 anim_name = 0xFF;

		     if (_sveq("num")) anim_name = UAXIS_TYPE;
		else if (_sveq("frame")) anim_name = UAXIS_FRAME;
		else if (_sveq("mode")) anim_name = UAXIS_MODE;

		if (anim_name != 0xFF) {
			SDL_Rect *taxis;
			int axis_id = -1;
			int i;
			for (i = 0; i < anim->num_axises; i++) {
				if (anim->axis_modifier[i] == anim_name) {
					axis_id = i;
					break;
				}
			}
			if (axis_id == -1) {
				axis_id = anim->num_axises++;
			}
			taxis = &anim->axis_offset[axis_id];
			taxis->x = a;
			taxis->y = b;
			taxis->w = c;
			taxis->h = d;
			anim->axis_modifier[axis_id] = anim_name;
		}
		else { printf("Please supply hardcoded axis identifier (num, frame)\n"); }
	}
	else err = 1; /* Not an animation param */

	return err;
#undef _sveq
#undef _veq
#undef _eq
}

/** Houses **/
int rcfg_house_count(dom_iter* this, void* ptr) { 

	int i;
	for (i = 0; i < MAX_BHOUSES; i++) {
		house_p *housep = &bhouses[i];

		wipe_animation(&housep->body);
		//wipe_animation(&housep->face);

        housep->icon = i;
        housep->body.image = ASS_LoadTexture("data/gfx/runelord.bmp", &white);

	}

	pseudo_marker[0] = 0;

	return MAX_BHOUSES; 
}

int rcfg_house_pick(dom_iter* this, void* ptr) {

	house_p *housep = ptr;

	if (ptr == bhouses) {
		this->cursor = 0;
	}

	this->next = (housep+1);
	this->parent = NULL;
	this->kids = NULL;

	if (this->cursor++ > num_bhouses) {
		this->next = NULL;
	}

	return 0;
}

char* rcfg_house_test(void *ptr, const char *name) {
#define _eq(STR) (!strcasecmp(name, STR))
	house_p *housep = (house_p *)ptr;
	int num = (housep - bhouses);

	if (_eq("type")) return "building";

	if (_eq("num")) {
		static char buf[12];
		snprintf(buf, sizeof(buf), "%d", num);
		return buf;
	}

	if (_eq("id")) {
		return housep->id;
	}

	if (_eq("class")) {
		return house_classes[num];
	}

	return "";
#undef _eq
}


int rcfg_house_apply(void *ptr, const char *name, const char *value) {
#define _eq(STR) (!strcasecmp(name, STR))
#define _veq(STR) (!strcasecmp(value, STR))
#define _sveq(STR) (!strcasecmp(subvalue, STR))

	house_p *housep = ptr;
	int num = (housep - bhouses);

//printf("	Applying value %s = %s for house %d %s (%s)\n", name, value, num, housep->title, housep->id);

	if (_eq("id")) {
		strncpy(housep->id, value, LEN_HOUSE_ID);

		/* If we have a pseudo-mark, auto-apply it to this house's class */
		if (pseudo_marker[0]) {
			char *cc = house_classes[num];
			strncat(cc, " ", sizeof(house_classes[0]));
			strncat(cc, pseudo_marker, sizeof(house_classes[0]));
		}
	}
	else
	if (_eq("pseudo")) {
		if (_veq("last")) {
			num_bhouses = num + 1;
			if (num_bhouses > MAX_BHOUSES) num_bhouses = MAX_HOUSES;
		}
	}
	else
	if (_eq("pseudo-mark")) {
		strncpy(pseudo_marker, value, sizeof(pseudo_marker));
	}
	else
	if (_eq("class")) {
		char *cc = house_classes[num];
		strncat(cc, " ", sizeof(house_classes[0]));
		strncat(cc, value, sizeof(house_classes[0]));
	}	
	else 
	if (_eq("title")) {
		strncpy(housep->title, value, LEN_HOUSE_TITLE);
	}
	else
	if (_eq("icon")) housep->icon = atoi(value);
	else
	if (_eq("gold")) housep->gold_cost = atoi(value);
	else
	if (!rcfg_apply_animation(&housep->body, name, value)) {
		//ok
	}
	else
	if (_eq("offset-unbuilt")) {
		int a, b;
		char aU[4], bU[4];
		if (sscanf(value, "%d%s %d%s", &a, aU, &b, bU) == 4) {
			if (aU[0] != 't' || aU[1] != 0) {printf("The only unit type supported is 't'\n");return -1;}
			if (bU[0] != 't' || bU[1] != 0) {printf("The only unit type supported is 't'\n");return -1;}
			housep->unbuilt.x = a * 16;
			housep->unbuilt.y = b * 16;
		} else { printf("Tile size must in `Nu Nu` format (N=number, u=unit(px,t))\n"); }
	}
	else
	if (_eq("animation-idle")) {
		rcfg_parse_frames(&housep->body, ANIM_BUILT, value);
	}
	else
	if (_eq("animation-building")) {
		rcfg_parse_frames(&housep->body, ANIM_BUILDING, value);
	}
	else
	if (_eq("animation-working")) {
		rcfg_parse_frames(&housep->body, ANIM_WORK, value);
	}
	else
	if (_eq("animation-damaged")) {
		rcfg_parse_frames(&housep->body, ANIM_DAMAGE, value);
	}
	else
	if (_eq("animation-icon")) {
		rcfg_parse_frames(&housep->body, ANIM_ICON, value);
	}
	else { printf("	Ignoring value %s = %s for house %d %s (%s)\n", name, value, num, housep->title, housep->id); }
	return 0;
#undef _sveq
#undef _veq
#undef _eq
}

/** Units **/
int rcfg_units_count(dom_iter* this, void* ptr) { 

	int i;
	for (i = 0; i < MAX_BUNITS; i++) {
		unit_classes[i][0] = 0;
		unit_p *unit = &bunits[i];
		
		wipe_animation(&unit->body);
		wipe_animation(&unit->face);

		unit->body.image = ASS_LoadTexture("data/gfx/runelord.bmp", &white);
	}

	pseudo_marker[0] = 0;

	return MAX_BUNITS; 
}

int rcfg_units_pick(dom_iter* this, void* ptr) {

	unit_p *unitp = ptr;

	if (ptr == bunits) {
		this->cursor = 0;
	}

	this->next = (unitp+1);
	this->parent = NULL;
	this->kids = NULL;

	if (this->cursor++ > num_bunits) {
		this->next = NULL;
	}

	return 0;
}

char* rcfg_units_test(void *ptr, const char *name) {
#define _eq(STR) (!strcasecmp(name, STR))
	unit_p *unitp = ptr;

	if (_eq("type")) return "unit";

	if (_eq("num")) {
		static char buf[12];
		sprintf(buf, "%d", (unitp-bunits));
		return buf;
	}

	if (_eq("id")) {
		return unitp->id;	
	}
	
	if (_eq("class")) {
		return unit_classes[(unitp-bunits)];
	}

//printf("Testing.. %s\n", name);

	return "";
#undef _eq
}

int rcfg_units_apply(void *ptr, const char *name, const char *value) {
#define _eq(STR) (!strcasecmp(name, STR))
#define _veq(STR) (!strcasecmp(value, STR))
#define _sveq(STR) (!strcasecmp(subvalue, STR))

	unit_p *unitp = ptr;
	int num = (unitp - bunits);
	
	if (_eq("id")) {
		strncpy(unitp->id, value, LEN_UNIT_ID);

		/* If we have a pseudo-mark, auto-apply it to this unit's class */
		if (pseudo_marker[0]) {		
			char *cc = unit_classes[num];
			strncat(cc, " ", sizeof(unit_classes[num]));
			strncat(cc, pseudo_marker, sizeof(unit_classes[num]));
		}
	}	
	else
	if (_eq("pseudo")) {
		if (_veq("last")) {
			num_bunits = num + 1;
			if (num_bunits > MAX_BUNITS) num_bunits = MAX_BUNITS;
		}
	}
	else
	if (_eq("pseudo-mark")) {
		strncpy(pseudo_marker, value, sizeof(pseudo_marker));
	}
	else
	if (_eq("class")) {

		char *cc = unit_classes[num];
		strncat(cc, " ", sizeof(unit_classes[0]));
		strncat(cc, value, sizeof(unit_classes[0]));
		//printf("Attaching class: %s\n", value);
	}	
	else 
	if (_eq("title")) {
			strncpy(unitp->title, value, LEN_UNIT_TITLE);
	}
	else
	if (_eq("stats")) {
		int a,b,c,d,e,f;
		if (sscanf(value, "%d %d %d %d %d %d", &a, &b, &c, &d, &e, &f) == 6) {
			unitp->base_stat[0] = a;
			unitp->base_stat[1] = b;
			unitp->base_stat[2] = c;
			unitp->base_stat[3] = d;
			unitp->base_stat[4] = e;
			unitp->base_stat[5] = f;
		} else printf("Stats must be in 1 2 3 4 5 6 format (six space-separated numbers)\n"); 
	}
	else
	if (_eq("str")) unitp->base_stat[0] = atoi(value);
	else
	if (_eq("dex")) unitp->base_stat[1] = atoi(value);
	else
	if (_eq("con")) unitp->base_stat[2] = atoi(value);
	else
	if (_eq("int")) unitp->base_stat[3] = atoi(value);
	else
	if (_eq("met")) unitp->base_stat[4] = atoi(value);
	else
	if (_eq("chr")) unitp->base_stat[5] = atoi(value);
	else
	if (_eq("skills")) {
		int a,b,c,d,e,f;
		if (sscanf(value, "%d %d %d %d %d %d", &a, &b, &c, &d, &e, &f) == 6) {
			unitp->base_stat[6] = a;
			unitp->base_stat[7] = b;
			unitp->base_stat[8] = c;
			unitp->base_stat[9] = d;
			unitp->base_stat[10] = e;
			unitp->base_stat[11] = f;
		} else printf("Skills must be in 1 2 3 4 5 6 format (six space-separated numbers)\n"); 
	}
	else
	if (_eq("perception")) {
		if (_veq("sight")) {
			unitp->perc = Sight;	
		}
		else if (_veq("smell")) {
			unitp->perc = Smell;
		}
		else fprintf(stderr, "Perception must be 'sight' or 'smell'\n");
	}
	else
	if (_eq("modus")) {
		     if (_veq("none"))   unitp->modus = MODUS_NONE;
		else if (_veq("worker")) unitp->modus = MODUS_WORKER;
		else if (_veq("hunter")) unitp->modus = MODUS_HUNTER;
		else if (_veq("soldier")) unitp->modus = MODUS_SOLDIER;
		else if (_veq("healer")) unitp->modus = MODUS_HEALER;
		else if (_veq("carrier")) unitp->modus = MODUS_CARRIER;
		else if (_veq("follower")) unitp->modus = MODUS_FOLLOWER;
		else if (_veq("buffer")) unitp->modus = MODUS_BUFFER;
		else if (_veq("debuffer")) unitp->modus = MODUS_DEBUFFER;
		else if (_veq("explorer")) unitp->modus = MODUS_EXPLORER;
		else fprintf(stderr, "Unrecognized modus operandi '%s'. See 'ai.h' MODUS_ defines.\n", value);
	}
	else
	if (_eq("scent-human")) unitp->base_scent[SCENT_HUMAN] = atoi(value);
	else
	if (_eq("scent-animal")) unitp->base_scent[SCENT_ANIMAL] = atoi(value);
	else
	if (_eq("scent-magic")) unitp->base_scent[SCENT_MAGIC] = atoi(value);
	else
	if (!rcfg_apply_animation(&unitp->body, name, value)) {
		//ok
	}
	else
	if (_eq("animation-idle")) {
		rcfg_parse_frames(&unitp->body, ANIM_IDLE, value);
	}
	else
	if (_eq("animation-walk")) {
		rcfg_parse_frames(&unitp->body, ANIM_WALK, value);
	}
	else
	if (_eq("animation-carry")) {
		rcfg_parse_frames(&unitp->body, ANIM_CARRY, value);
	}
	else
	if (_eq("animation-attack")) {
		rcfg_parse_frames(&unitp->body, ANIM_HIT, value);
	}
	else
	if (_eq("animation-death")) {
		rcfg_parse_frames(&unitp->body, ANIM_DEATH, value);
	}
	else
	if (_eq("animation-face")) {
		rcfg_parse_frames(&unitp->body, ANIM_FACE, value);
	}
	else	printf("	Ignoring value %s = %s for unit %d %s (%s)\n", name, value, num, unitp->title, unitp->id);
	return 0;
#undef _sveq
#undef _veq
#undef _eq
}

dom_iter rcfg_units_iterator = {
	NULL,
	NULL,
	NULL,
	NULL,
	
	NULL,
	NULL,
	NULL,
	0,
	
	&rcfg_units_count,
	&rcfg_units_pick,
	&rcfg_units_test,
	&rcfg_units_apply,
};

dom_iter rcfg_house_iterator = {
	NULL,
	NULL,
	NULL,
	NULL,
	
	NULL,
	NULL,
	NULL,
	0,
	
	&rcfg_house_count,
	&rcfg_house_pick,
	&rcfg_house_test,
	&rcfg_house_apply,
};

int cfg_load(const char *filename) {

    css_ruleset *sheet;

	SDL_RWops *file;
	
	file = SDL_RWFromFile(filename, "rb");
	if (file == NULL) return -1;

	css_parser *state = css_parser_create();

	char buf[1024];
	int n, err;
	while (1) {
		n = SDL_RWread(file, buf, sizeof(char), sizeof(buf));
		if (n <= 0) break;
		
		err = css_parse(state, buf, n);
		if (err) break;
	}

	SDL_RWclose(file);

	sheet = css_parser_done(state);
	if (sheet == NULL) return -1;

	num_bunits = MAX_BUNITS;
	num_bhouses = MAX_BHOUSES;

	//css_fprintf(stdout, mainCSS);
	css_cascade(&rcfg_units_iterator, &bunits,  sheet); 
	css_cascade(&rcfg_house_iterator, &bhouses, sheet);

	compute_units(&bunits, num_bunits);
	compute_houses(&bhouses, num_bhouses);

	return 0;
}