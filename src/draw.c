#include <SDL.h>

#include "libs/SDL_inprint/SDL2_inprint.h"

#include "game.h"
#include "ui.h"
#include "draw.h"
#include "rune.h"

SDL_Texture *tiles;
SDL_Texture *uibg;
SDL_Texture *small_font;
SDL_Texture *mid_font;
SDL_Texture *large_font;

SDL_Color white = { 0xFF, 0xFF, 0xFF, 255 };
SDL_Color black = { 0x00, 0x00, 0x00, 255 };
SDL_Color brown = { 0x6c, 0x44, 0x1c, 255 };
SDL_Color yellow = { 0xFF, 0xFF, 0x00, 255};
SDL_Color red    = { 0xFF, 0x00, 0x00, 255};
SDL_Color darkred= { 0x66, 0x11, 0x11, 255};
SDL_Color magenta = { 0xFF, 0x00, 0xFF, 255};
SDL_Color darkmagenta = { 0x80,0, 0x80, 255};

SDL_Color reds[16];

Uint32 state_colors[4] = {
	0xFFFFFF, 0xFF0000, 0x0000FF, 0x00FF00,
};

extern void SDL_ComputeGradient(SDL_Color *colors, int n, SDL_Color *start, SDL_Color *stop);
void prepare_colors() {

	SDL_Color start = { 0xFF, 0x00, 0x00 };
	SDL_Color end = { 0x66, 0x11, 0x11 };

	SDL_ComputeGradient(reds, 16, &start, &end);  

}
 
void MY_LineRect(SDL_Renderer *dst, SDL_Rect *dr, SDL_Color *color)
{
	SDL_SetRenderDrawColor(dst, color->r, color->g, color->b, color->a);
    SDL_RenderDrawRect(dst, dr);
}

void MY_PutPixel(SDL_Renderer *dest, Uint32 x, Sint32 y, SDL_Color *col)
{
    SDL_SetRenderDrawColor(dest, col->r, col->g, col->b, col->a);
    SDL_RenderDrawPoint(dest, x, y);
}

void MY_DrawVector(SDL_Renderer *surface, Sint32 x, Sint32 y, float a, int len, SDL_Color *color)
{
	int i;
	SDL_Rect *clip = NULL;//-&surface->clip_rect;	
	float c = cosf(a * M_PI / 180);  
	float s = sinf(a * M_PI / 180);
	for (i = 0; i < len; i++)
	{
		int nx = (int)(x + i * c);
		int ny = (int)(y + i * s);
 
		if (!clip || SDL_InBounds(nx, ny, clip))
		{
			MY_PutPixel(surface, nx, ny, color);
		}
	}
}

void MY_DrawLine(SDL_Renderer *dst, Sint32 x1, Sint32 y1, Sint32 x2, Sint32 y2, SDL_Color *color)
{
	Sint32 dx = x2 - x1; 
	Sint32 dy = y2 - y1;
	double angle = atan2(dy, dx) * 180 / M_PI;
	double distance = sqrt( (dx*dx) + (dy*dy) );

	SDL_SetRenderDrawColor(dst, color->r, color->g, color->b, color->a);
	//SDL_RenderDrawLine(dst, x1, y1, x2, y2);
	MY_DrawVector(dst, x1, y1, (float)angle, (int)distance, color);
}

void SDL_AlphaFill(SDL_Renderer *dest, Uint32 x, Sint32 y, Uint32 w, Uint32 h, SDL_Color *color)
{
	SDL_Rect dst = { x, y, w, h };
	//SDL_SetRenderDrawBlendMode(dest, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(dest, color->r, color->g, color->b, color->a);
    SDL_RenderFillRect(dest, &dst);
}
#define PIXEL_BY_PIXEL
void MY_DrawSineWave(SDL_Renderer *surface, Sint32 x, Sint32 y, float a, int len, SDL_Color *col)
{
	float i;
	SDL_Rect *clip = NULL;//&surface->clip_rect;	
	float c = cosf(a * M_PI / 180);  
	float s = sinf(a * M_PI / 180);
	float AMP = 2;
	float WAV = 4;

#ifndef PIXEL_BY_PIXEL
	int count = 0; SDL_Point points[1024];
#endif
    int freq = ui.freq;

int colorquant = 16;//((len+1) / 16);
	
	for (i = 0; i < len; i+= 0.5)//0.1)
	{
int j = (int)(freq) % colorquant;

		int nx = (int)(x + i * c);
		int ny = (int)(y + i * s);
		ny += ( cosf( (freq+i) / WAV) * AMP );
		nx += ( sinf( (freq+i) / WAV) * AMP );
#ifndef PIXEL_BY_PIXEL
		points[count].x = nx;
		points[count].y = ny;
		count++;
		if (count >= sizeof(points) - 2) break;
		continue;
#else
		if (!clip || SDL_InBounds(nx, ny, clip))
		{
		    MY_PutPixel(surface, nx, ny, &reds[j]);
			//SDL_AlphaFill(surface, nx, ny, 2, 2, color); 
		}
#endif
	}
#ifndef PIXEL_BY_PIXEL
	SDL_SetRenderDrawColor(surface, col->r, col->g, col->b, col->a);
	SDL_RenderDrawPoints(surface, points, count);
#endif
}
void MY_DrawSine(SDL_Renderer *dst, Sint32 x1, Sint32 y1, Sint32 x2, Sint32 y2, SDL_Color *color)
{
	Sint32 dx = x2 - x1; 
	Sint32 dy = y2 - y1;
	double angle = atan2(dy, dx) * 180 / M_PI;
	double distance = sqrt( (dx*dx) + (dy*dy) );

	MY_DrawSineWave(dst, x1, y1, (float)angle, (int)distance, color);
}

int unit_icon(unit_t *u) {
	if (u->link == NULL) /* Either rune lord either free man */
		if (u->ref_counts == 0) return 0; /* FREE MAN */
		else return 3; /* RUNE LORD */
	else	/* Either a vector either a devotee */
		if (u->ref_counts == 0) return 1; /* DEVOTEE */
		else return 2; /* VECTOR */
}

/* RUNE printf */
int rprintf(SDL_Renderer *renderer, Uint32 x, Uint32 y, const char *fmt, ...) {
	char buffer[1024];

	va_list argptr;
	va_start(argptr, fmt);

	vsnprintf(buffer, sizeof(buffer), fmt, argptr);

	va_end(argptr);

	inprint(renderer, buffer, x, y);

	return 0;
}


/* One tile */
void draw_tile(SDL_Renderer *screen, Uint8 id, Uint8 frame, Uint32 x, Uint32 y) {
	SDL_Rect src = { frame * 16, id * 16 , 16, 16 };
	SDL_Rect dst = { x, y, 16, 16 };
	SDL_RenderCopy(screen, tiles, &src, &dst);
}
/* W & H tile */
void draw_ctile(SDL_Renderer *screen, Uint8 id, Uint8 frame, Uint32 x, Uint32 y, Uint8 w, Uint8 h) {
	SDL_Rect src = { frame * 16, id * 16 , 16 * w, 16 * h };
	SDL_Rect dst = { x, y, 16 * w, 16 * h };
	SDL_RenderCopy(screen, tiles, &src, &dst);
}
/* Stretched tile */
void draw_stile(SDL_Renderer *screen, Uint8 id, Uint8 frame, Uint32 x, Uint32 y, Uint8 w, Uint8 h, Uint8 sw, Uint8 sh) {
	SDL_Rect src = { frame * TILE_W, id * TILE_W , w * TILE_W, h * TILE_H };
	SDL_Rect dst = { x, y, sw * TILE_W, sh * TILE_H };
	SDL_RenderCopy(screen, tiles, &src, &dst);
}

/* W & H tile on sx/sy source */
void draw_btile(SDL_Renderer *screen, Uint32 sy, Uint32 sx, Uint32 x, Uint32 y, Uint8 w, Uint8 h) {
	SDL_Rect src = { sx, sy, 16 * w, 16 * h };
	SDL_Rect dst = { x, y, 16 * w, 16 * h };
	SDL_RenderCopy(screen, tiles, &src, &dst);
}

void draw_visual(SDL_Renderer *renderer, Uint8 *refs[], animation_t *a, Uint32 x, Uint32 y) {
	int i;

	SDL_Rect dst = { x, y, a->plane.w, a->plane.h };
	//start with base plane...
	SDL_Rect src = { a->plane.x, a->plane.y, a->plane.w, a->plane.h };

	//...offset by each modifier axis
	for (i = 0; i < a->num_axises; i++) {
		Uint8 ask = a->axis_modifier[i];
		Uint8 ref = *(refs[ask]);
		src.x += ref * a->axis_offset[i].w + a->axis_offset[i].x;
		src.y += ref * a->axis_offset[i].h + a->axis_offset[i].y;
    }

	SDL_RenderCopy(renderer, a->image, &src, &dst);
}



void draw_blackbox(SDL_Renderer *screen, Uint32 x, Uint32 y, Uint32 w, Uint32 h) {
//	Uint32 brown = SDL_MapRGB(screen->format, 0x6c, 0x44, 0x1c);

	SDL_Rect box = { x, y, w, h };

    SDL_SetRenderDrawColor(screen, 0, 0, 0, 255);
	SDL_RenderFillRect(screen, &box);

	/*box.x++;
	box.y++;
	box.w-=2;
	box.h-=2;*/

    SDL_SetRenderDrawColor(screen, brown.r, brown.g, brown.b, 255);
    SDL_RenderDrawRect(screen, &box);
}


///////////

void draw_pools(SDL_Renderer *screen) {

    SDL_Color redish = { 0xff, 0, 0, 0x33 };
	SDL_Color greenish = { 0, 0xff, 0, 0x33 };
	SDL_Color black = { 0, 0, 0, 0xAA };
	SDL_Color white = { 0xff, 0xff, 0xff, 0xEE };	

	int i;
	for (i = 0 ; i < num_pools; i++) {
		pool_t *pool = &pools[i];
		unit_t *head = pool->head;

		Uint32 x = head->x * TILE_W - TILE_W;
		Uint32 y = head->y * TILE_H - TILE_H;

	x -= (i % 4) * 8;
	y -= (i % 4) * 16;

		x = x - ui.vx + game_map.x;
		y = y - ui.vy + game_map.y;

		SDL_Rect mini = { x, y, 64, 10 };

		SDL_AlphaFill(screen, x, y, mini.w, mini.h, &black);
		MY_LineRect(screen, &mini, &redish);

//		draw_tile(screen, TILE_STAT_Y, TILE_STAT_X + pool->stat, x , y);

		int links  = 0;		
		/* Draw kids overlays */
		//unit_t *lim = (head->link == pool->tail ? NULL : pool->head);
		unit_t *it = pool->tail;
		while (it) {
			Uint32 sx = it->x * TILE_W + it->ox - ui.vx + game_map.x;
			Uint32 sy = it->y * TILE_H + it->oy - ui.vy + game_map.y;

			MY_DrawLine(screen, sx + 8, sy + 4, x, y + mini.h/2, &redish);

			it = it->link;
			if (it == head) break;
			links++;
		}

		infont(small_font);
		incolor1(&white);
		rprintf(screen, x + 3, y + 1, "%02d %s:%03d", links+1, stat_names[pool->stat], pool->pool);

		Uint32 sx = head->x * TILE_W + it->ox - ui.vx + game_map.x;
		Uint32 sy = head->y * TILE_H + it->oy - ui.vy + game_map.y;

		MY_DrawLine(screen, sx + 8, sy + 4, x + mini.w/2, y + mini.h/2, &greenish);

	}
	
	//TODO: remove
	infont(large_font);
}
void draw_paths(SDL_Renderer *screen, Sint8 sel, Uint16 x, Uint16 y) {
#ifndef DEBUG_PATHFIND
	int i,j;
	if (sel == -1) return;
	unit_t *su = &units[sel];
	for (i = 0; i < 16; i++) {
		if (su->path[i] == -1) break;
		int x, y;
		
		y = su->path[i] / level_w;
		x = su->path[i] % level_w;

		Uint32 tx = x * TILE_W - ui.vx + game_map.x;
		Uint32 ty = y * TILE_H - ui.vy + game_map.y;

		SDL_Color color = { 255, 255, 255, 128 + i * 8 };
		SDL_AlphaFill(screen, tx, ty, TILE_W, TILE_H, &color);

	}

	SDL_AlphaFill(screen,
		su->tx * TILE_W - ui.vx + game_map.x,
		su->ty * TILE_H - ui.vy + game_map.y, TILE_W, TILE_H, &yellow);
#else
	int i,j;
	if (sel == -1) return;
	unit_t *su = &units[sel];
	for (j = 0; j < LEVEL_H; j++) {
		for (i = 0; i < LEVEL_W; i++) {
			Uint32 pos = j * LEVEL_W + i;
			Uint32 tx = i * TILE_W - ui.vx + game_map.x;
			Uint32 ty = j * TILE_H - ui.vy + game_map.y;
			char buf[16];
			char vd = '>' - 1;
			if (su->prev[pos] == -1) vd = '!';
			else if (su->prev[pos] - pos == -1) vd = '<';
			else if (su->prev[pos] - pos < -1) vd = '^';
			else if (su->prev[pos] - pos > 1) vd = 'v';

			infont(small_font);
			incolor(0x00FFFFFF, 0);
			//rprintf(screen, tx, ty, "%ld%c", (su->d[pos]/25 > 9 ? 9 : su->d[pos]/25), vd);
			rprintf(screen, tx, ty, "%ld", su->d[pos]/10);

			//Uint32 color = SDL_MapRGBA(screen->format, 
			SDL_Color color = { su->d[pos], su->d[pos], su->d[pos], su->d[pos] };
			SDL_AlphaFill(screen, tx + x, ty + y, TILE_W, TILE_H, &color);
		}
	}

	SDL_AlphaFill(screen,
		su->tar_x * TILE_W - ui.vx + game_map.x,
		su->tar_y * TILE_H - ui.vy + game_map.y, TILE_W, TILE_H, &red);

	
	SDL_AlphaFill(screen,
		su->tx * TILE_W - ui.vx + game_map.x,
		su->ty * TILE_H - ui.vy + game_map.y, TILE_W, TILE_H, &yellow);

	i = ui.hover_tx;
	j = ui.hover_ty;
	Uint32 pos = j * LEVEL_W + i;
	Uint32 tx = i * TILE_W - ui.vx + game_map.x;
	Uint32 ty = j * TILE_H - ui.vy + game_map.y;

	infont(small_font);
	incolor(0x00FF0000, 0);
	//rprintf(screen, tx, ty, "%ld%c", (su->d[pos]/25 > 9 ? 9 : su->d[pos]/25), vd);
	rprintf(screen, tx, ty, "%ld", su->d[pos]/10);

	//TODO: remove
	infont(large_font);
#endif
}
void draw_overlays(SDL_Renderer *screen, Sint8 sel, Uint8 flip, Uint8 block, unit_t *from) {
	if (sel == -1) return;
	int i;
	if (block == 0) for (i = 0; i < num_units; i++) units[i].block = 0;
	unit_t *su = &units[sel];
	Uint32 sx = su->x * TILE_W + su->ox - ui.vx + game_map.x;
	Uint32 sy = su->y * TILE_H + su->oy - ui.vy + game_map.y;
	su->block = 1;
	//Uint32 colors[16] = { 0x00FF0033, 0xFF000033 };
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		Uint32 x = u->x * TILE_W + u->ox - ui.vx + game_map.x;
		Uint32 y = u->y * TILE_H + u->oy - ui.vy + game_map.y;

		if (u->tile == 0 || u == from) continue;
		if (u->link == su ) {
			MY_DrawSine(screen, sx + 8, sy + 4, x + 8, y + 4, reds);//colors[flip]);
			//SDL_DrawLine(screen, sx + 8, sy + 4, x + 8, y + 4, colors[flip]);
			if (!u->block) draw_overlays(screen, i,  flip, 1, su);
		}
		if (su->link == u) {
			MY_DrawSine(screen, x + 8, y + 4, sx + 8, sy + 4, reds);//colors[1 - flip]);
			//SDL_DrawLine(screen, sx + 8, sy + 4, x + 8, y + 4, colors[flip]);
			if (!u->block) draw_overlays(screen, i, flip, 1, su);
		}
	}
}

void draw_goldbox(SDL_Renderer *screen, Uint32 x, Uint32 y, Uint8 tile, int val) {
	Uint32 w = 72;
	Uint32 h = 14;

	SDL_Rect border  = { x, y, w, h };	
	SDL_Rect filler  = { x+1, y+1, w-2, h-2 };

	SDL_SetRenderDrawColor(screen, 0, 0, 0, 255); //black
	SDL_RenderFillRect(screen, &filler);

	SDL_SetRenderDrawColor(screen, brown.r, brown.g, brown.b, 255);
	SDL_RenderDrawRect(screen, &border);

	infont(large_font);
	incolor(0x00FFFFFF, 0);
	rprintf(screen, x+14, y+3, "% 7d", val);

	draw_tile(screen, TILE_RES_Y, TILE_RES_X + tile, x+4, y+4);
}

void draw_buildset(SDL_Renderer *screen) {

	int x = buildbox.x;
	int y = buildbox.y;

	int i;
	for (i = 0; i < 2; i++) {
		int hover = 0;
		int pushin_x = 1;
		int pushin_y = 1;
		if (ui.setflag == i) {
			hover = 2;
			pushin_x = 0;
			pushin_y = 0;
		}
		else {
			if (ui.hover == overFlagButton && ui.hover_id == i) {
				hover = 1;
			}
		}

		draw_tile(screen, TILE_BTN_Y + 1, TILE_BTN_X + hover, x, y);
		draw_tile(screen, TILE_FLAG_Y, TILE_FLAG_X + i, x - pushin_x, y - pushin_y);

		x += 17;
		if (x >= buildbox.x + buildbox.w - TILE_W/2) {
			x = buildbox.x;
			y += 17;
		}
	}
	for (i = 0; i < HMENU_ITEMS; i++) {

		house_p *m = &bhouses[i];	

		int hover = 0;
		int pushin_x = 1;
		int pushin_y = 1;
		if (ui.builder == i) {
			hover = 2;
			pushin_x = 0;
			pushin_y = 0;
		}
		else {
			if (ui.hover == overBuildButton && ui.hover_id == i) {
				hover = 1;
			}
		}

		draw_tile(screen, TILE_BTN_Y + 1, TILE_BTN_X + hover, x, y);
		draw_tile(screen, 0, m->icon + 1, x - pushin_x, y - pushin_y);

		x += 17;
		if (x >= buildbox.x + buildbox.w - TILE_W/2) {
			x = buildbox.x;
			y += 17;
		}
	}
}

void draw_minimap(SDL_Renderer *screen) {
	Uint32 w = minimap.w;
	Uint32 h = minimap.h;

	Uint32 x = minimap.x;
	Uint32 y = minimap.y;

//	draw_blackbox(screen, x, y, w, h);

	x += 1;
	y += 1;	
	w -= 2;
	h -= 2;

	SDL_Rect dem = { x, y, w, h };
	SDL_SetRenderDrawColor(screen, 0x11, 0x33, 0x11, 255);
    SDL_RenderFillRect(screen, &dem);

	int zY = h / level_h;
	int zX = w / level_w;

	int rw = level_w * zX;
	int rh = level_h * zY;

	int offX = w % level_w / 2;
	int offY = h % level_h / 2;

	x += offX;
	y += offY;

	int i;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		unit_p *proto = &bunits[u->tile];

        SDL_Color *color;

		color = &factions[u->faction].color;

		if (ui.unit == i) {
		    color = &white;
		}
		SDL_Rect trg = { x + u->x * zX, y + (u->y-(proto->h-1)) * zY, proto->w * zX, proto->h * zY };
        SDL_SetRenderDrawColor(screen, color->r, color->g, color->b, 255);
        SDL_RenderFillRect(screen, &trg);		
	}
	for (i = 0; i < num_houses; i++) {
		house_t *h = &houses[i];

        SDL_Color color;
        color.a = 255;
        color.r = 0; color.g = 0; color.b = 0xff;
		if (ui.house == i) {
		    color.r = 0xff; color.g = 0xff; color.b = 0xff;
		}
		SDL_Rect trg = { x + h->x * zX, y + (h->y) * zY, h->w * zX, h->h * zY };
		SDL_SetRenderDrawColor(screen, color.r, color.g, color.b, color.a);
        SDL_RenderFillRect(screen, &trg);
	}
	int j;
	for (j = 0; j < LEVEL_H; j++) {
		for (i = 0; i < LEVEL_W; i++) {
			if (fog[j][i]) {
				SDL_Rect trg = { x + i * zX, y + j * zY, zX, zY };
				SDL_SetRenderDrawColor(screen, 0,0,0, 255);
                SDL_RenderFillRect(screen, &trg);
			}
		}
	}

	int vx = (x) + ui.vx / (TILE_W / zX);
	int vy = (y) + ui.vy / (TILE_H / zY);

	int vw = rw / ((float)level_w * TILE_W / (game_map.w - 1)) + 1;
	int vh = rh / ((float)level_h * TILE_H / (game_map.h - 1)) + 1;

	//SDL_Rect vport = { vx, vy, vw, vh }; // Most accurate
	//SDL_Rect vport = { vx - offX, vy - offY, vw + offX * 2, vh + offY * 2 }; // Prettiest
	SDL_Rect vport = { vx - 1, vy - 1, vw + 2, vh + 2 }; // Middle ground

	MY_LineRect(screen, &vport, &white);

	MY_LineRect(screen, &minimap, &brown);

}

void draw_unitname(SDL_Renderer *screen, unit_t *u, Uint32 y, Uint8 offset, Uint16 xoffset) {
	unit_p *p = &bunits[u->tile];
	Uint32 x = ui.log_width - PANE_WIDTH + xoffset;

	//draw_blackbox(screen, x, y+2, w, h);

	/* :( draw face in bad way */
	if (!u->tile) draw_tile(screen, 0, 0, x, y); 
	else
		draw_visual(screen, u->axis_refs, &p->face, x, y - (p->h-1)*TILE_H);

	incolor1(&bunits[u->tile].color);
	incolor(0x00ffffff, 0);
	inprint(screen, u->name, x + 17, y + offset);
	//inprint(screen, bunits[u->tile].title, x + 17, y+8+3);
}

void draw_progbar(SDL_Renderer *renderer, int val, int max, const char *buf, Uint32 x, Uint32 y, SDL_Color *colors[3]) {

	SDL_Rect dest = { x, y, 96, 9 };

	if (val < 0) val = 0;

	int centerX = 0;
	char buf2[80];
	sprintf(buf2, buf, val, max);
	centerX = (dest.w - strlen(buf2) * 6) / 2;

	MY_LineRect(renderer, &dest, colors[0]);

	dest.x += 1;
	dest.y += 1;
	dest.w -= 2;
	dest.h -= 2;

	int prop = val * 100 / max;
	int pix_horz =  dest.w * prop / 100;

	SDL_AlphaFill(renderer, dest.x, dest.y, pix_horz, dest.h, colors[1]);

	infont(small_font);
	incolor1(colors[2]);
	rprintf(renderer, x + centerX, y + 1, buf, val, max);
	//rprintf(renderer, x, y + 1, buf, val, max);

}

void draw_pinbox(SDL_Renderer *screen) {

	Uint32 x = pinbox.x;
	Uint32 y = pinbox.y;

	SDL_Rect pinbtn = { 0, 0, 16, 16 };

	int i, highlight_pin;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		if (u->tile && u->pin) {

			/* Draw name */
			draw_unitname(screen, u, y, 6, 0);

			/* Draw pin */
			pinbtn.x = x + 120;
			pinbtn.y = y + 4; 

			highlight_pin = 0;
			if (ui.hover == overListPin && ui.hover_id == i) highlight_pin = 1;

			draw_tile(screen, TILE_UICO_Y, TILE_UICO_X + 5 - highlight_pin, pinbtn.x, pinbtn.y);

			y += 16;
		}
	}

}

void draw_flagbox(SDL_Renderer *renderer) {

	const char flag_names[2][80] = {
		"Explore Flag",
		"Wanted Flag",
	};

	Uint32 w = selbox.w;
	Uint32 h = selbox.h;

	Uint32 x = selbox.x;
	Uint32 y = selbox.y;

	draw_blackbox(renderer, x, y, w, h);

	x += BOX_PADDING;
	y += BOX_PADDING;
	w -= BOX_PADDING * 2;
	h -= BOX_PADDING * 2;

	flag_t *f = &your->flags[ui.flag];

	draw_tile(renderer, TILE_FLAG_Y, TILE_FLAG_X + f->type, x, y); /* flag icon */
	y += 3; /* small offset after icon */

	incolor(0x00FFFFFF, 0);
	infont(large_font);
	inprint(renderer, flag_names[f->type], x + 17, y);
	y += 8;

	y += 16;
	x += 16;
	draw_goldbox(renderer, x, y, 1, f->reward);

	int hl = 0;

	if (ui.hover == overFlagPlus) hl = 1 + ui.pushing;

	x = plusbtn.x;
	y = plusbtn.y;

	draw_tile(renderer, TILE_BTN_Y+1, TILE_BTN_X + hl, x, y);
	
	draw_tile(renderer, TILE_FLAG_Y, TILE_FLAG_X+3, x, y);

}

void draw_housebox_visitors(SDL_Renderer *screen, house_t *h, Uint32 x, Uint32 y) {
	/* visitors */
	int i, j = 0;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		if (u->visiting != h) continue;

		infont(mid_font);
		draw_unitname(screen, u, y, 2, 0);

		incolor(0x00999999, 0);
		infont(small_font);
		rprintf(screen, x + 17, y + 8 + 2, "PROG: %d (%d gold)", u->progress, u->carry_gold);

		j++;
		y += 16;
	}
}

void draw_housetabs(SDL_Renderer *renderer) {

	Uint32 x = tabicon.x;
	Uint32 y = tabicon.y;

	Uint32 color = 0xCCCCCC;

	int i;
	for (i = 0; i < MAX_HOUSETABS; i++) {

		color = 0xCCCCCC;
		if (i == ui.housetab) color = 0xFFFFFF;
		else if (ui.hover == overUnitTab && i == ui.hover_id) color = 0xEEEEEE;

		incolor(color, 0);
		inprint(renderer, housetabs_names[i], x, y);

		x += tabicon.w;
		if (x + tabicon.w >= ui.log_width) {
			x = tabicon.x;
			y += tabicon.h;
		}
	}
}

void draw_housebox(SDL_Renderer *screen) {

	Uint32 w = selbox.w;
	Uint32 ph = selbox.h;

	Uint32 x = selbox.x;
	Uint32 y = selbox.y;

	draw_blackbox(screen, x, y, w, ph);

	x += BOX_PADDING;
	y += BOX_PADDING;
	//w -= BOX_PADDING * 2;
	//h -= BOX_PADDING * 2;

	house_t *h = &houses[ui.house];	
	house_p *p = &bhouses[h->tile];

	draw_tile(screen, 0, h->tile+1, x, y); /* house icon */
	y += 3; /* small offset after icon */

	incolor(0x00FFFFFF, 0);

	infont(large_font);
	inprint(screen, "Something", x + 17, y);
	y += 8;

	infont(large_font);
	inprint(screen, p->title, x + 17, y);
	y += 8;

	infont(small_font);
	rprintf(screen, x + 17, y, "HP: %d/%d", h->hp, h->max_hp);
	y += 8;

	/* Draw flag */
	if (h->flag_id[ui.faction] != -1)
		draw_tile(screen, TILE_FLAG_Y, TILE_FLAG_X + 1, flagicon.x, flagicon.y); /* flag icon */

	draw_housetabs(screen);

	/* visitors */
	draw_housebox_visitors(screen, h, x, y);
}

void draw_unitbox_tab1(SDL_Renderer *screen, unit_t *u, Uint32 x, Uint32 y) {

	int icon;

	unit_p *p = &bunits[u->tile];

	Uint32 color;

	infont(large_font);

	icon = unit_icon(u); 

	/* Print state (and draw icon) */
			draw_tile(screen, TILE_UICO_Y, TILE_UICO_X + icon, x + 26, y + 106); 
			incolor(state_colors[icon], 0);
			inprint(screen, state_names[icon], x + 16 + 17, y+106);
			incolor(0xFFFFFFFF, 0);

	/* Draw stats */
	int i;
	int hover;
	for (i = 0; i < MAX_STAT; i++) {
		hover = 0;
		y += 14;
		if (ui.stat == i) hover = 2;
		else {
			if (ui.hover == overUnitStat && ui.hover_id == i) {
				hover = 1;
			}
		}
		if (u->link != NULL) hover = -1;
		//if (u->link && u->link_stat == i) hover = 0;
		draw_tile(screen, TILE_BTN_Y, TILE_BTN_X + hover, x , y);

		draw_tile(screen, TILE_STAT_Y, TILE_STAT_X + i, x , y);
		incolor(0xFFFFFF,0);
		rprintf(screen, x + 17, y + 3, "%02d", u->base_stat[i]);

		Uint32 col;
		if (u->calc_stat[i] > u->base_stat[i]) col = 0x00FF00;
		else if (u->calc_stat[i] < u->base_stat[i]) col = 0xFF0000;
		else col = 0xFFFFFF;

		incolor(col,0);
		rprintf(screen, x + 17*2 + 7, y + 3, "%02d", u->calc_stat[i]);
	}

	/* Draw skills */
	y -= 14 * MAX_STAT;
	x += 64;
	for (i = 0; i < MAX_STAT; i++) {
		int tile = i;
		hover = 0;
		y += 14;
		int hx = 0;
		int hy = 0;
		if (ui.btn == i) { hover = 2; hx = 1; hy = 1; }
		else {
			if (ui.hover == overUnitSkill && ui.hover_id == i) {
				hover = 1;
			}
		}

		/* Hack -- perception can take 2 different icons */
		if (i == 4 && p->perc != Smell) tile += 1; 

		/* Hack -- offset "speech" icon by 1 */
		if (i == 5) tile += 1;

		//if (u->tile != U_PEASANT) hover = -1;
		draw_tile(screen, TILE_BTN_Y, TILE_BTN_X + hover, x, y);

		draw_tile(screen, TILE_STAT_Y + 3, TILE_STAT_X + tile, x+hx, y+hy);

		incolor(0xFFFFFF, 0);
		rprintf(screen, x + 17, y + 3, "%02d", u->base_skill[i]);

		if (u->calc_skill[i] > u->base_skill[i]) color = 0x00FF00;
		else if (u->calc_skill[i] < u->base_skill[i]) color = 0xFF0000;
		else color = 0xFFFFFF;

		incolor(color, 0);
		rprintf(screen, x + 17*2 + 7, y + 3, "%02d", u->calc_skill[i]);
	}

}

void draw_unitbox_tab2(SDL_Renderer *renderer, unit_t *u, Uint32 x, Uint32 y) {
	unit_p *p = &bunits[u->tile];

	/* Random info I */
	infont(large_font);
	incolor1(&white);
	rprintf(renderer, x, y + 16, "Level: %02d", u->level);
	/* exp */
	SDL_Color *exp_colors[3] = { &darkmagenta, &magenta, &darkmagenta };
	draw_progbar(renderer, u->exp, 1000, "Exp: %d/%d", x + 17 - 4, y + 16 + 8, exp_colors);

	/* Random info II */
	infont(mid_font);
	rprintf(renderer, x, y + 32, "Class: %s", p->title);

	/* Random info III */
	infont(small_font);
	incolor1(&white);
	rprintf(renderer, x, y + 64 + 8 * 0, "Fame: %d", u->fame);
	rprintf(renderer, x, y + 64 + 8 * 1, "Happiness: %d%", u->happiness);
	rprintf(renderer, x, y + 64 + 8 * 2, "Wealth: %d", u->gold);
	SDL_Color *nrg_colors[3] = { &brown, &yellow, &brown };
	draw_progbar(renderer, u->energy, (u->calc_stat[S_DEX]+1) * 10, "Energy: %d", x, y + 64 + 8 * 3, nrg_colors);
}

void draw_unitbox_tab_runes(SDL_Renderer *screen, unit_t *u, Uint32 x, Uint32 y) {

	int icon;
	unit_p *p = &bunits[u->tile];

	icon = unit_icon(u); 

	infont(large_font);

	/* Print state (and draw icon) */
			draw_tile(screen, TILE_UICO_Y, TILE_UICO_X + icon, x + 26, y + 106); 
			incolor(state_colors[icon], 0);
			inprint(screen, state_names[icon], x + 16 + 17, y+106);
			incolor(0xFFFFFFFF, 0);

	/* Draw stats */
	int i;
	int hover;
	Uint32 col;
	char *s;
	int stat = 0;
	for (i = 0; i < MAX_STAT; i++) {
		hover = 0;
		y += 14;
		if (ui.stat == i) hover = 2;
		else {
			if (ui.hover == overUnitStat && ui.hover_id == i) {
				hover = 1;	
			}
		}
		if (u->link != NULL) hover = -1;
		//if (u->link && u->link_stat == i) hover = 0;
		draw_tile(screen, TILE_BTN_Y, TILE_BTN_X + hover, x , y);

		draw_tile(screen, TILE_STAT_Y, TILE_STAT_X + i, x , y);
		
		if (u->link == NULL || u->link_stat != i) {
			if (u->ref_count[i] == 0) icon = 0;
			if (u->ref_count[i] >  0) icon = 3;
		} else {
			if (u->ref_count[i] == 0) icon = 1;
			if (u->ref_count[i] >  0) icon = 2;
		}

		if (icon == 3) { col = 0x00FF00; s = "<"; stat = u->calc_stat[i]; }
		else if (icon == 1) { col = 0xFF0000; s= ">"; stat = u->base_stat[i]; }
		else if (icon == 2) { col = 0x0000FF; s= ">"; stat = u->rune_stat[i]; }
		else { col = 0xFFFFFF; s = ""; stat = 0; }

		infont(large_font);
		incolor(col,0);
		rprintf(screen, x + 17, y + 3, "%02d%s", stat, s);

		/* Print relation */
		infont(mid_font);
		if (u->link && u->link_stat == i)
			draw_unitname(screen, u->link, y - 4, 7, 48);
		else if (u->ref_count[i]) {
			incolor(col,0);
			rprintf(screen, x + 48, y + 3, "%d dvt", u->ref_count[i]);
		}
	}

}

void draw_unitbox_tab_ai(SDL_Renderer *renderer, unit_t *u, Uint32 x, Uint32 y) {
	unit_p *p = &bunits[u->tile];

	infont(large_font);
	incolor1(&white);
	rprintf(renderer, x, y + 16 + 8 * 0, "Desire: %s", desire_names[u->top_desire]);
	rprintf(renderer, x, y + 16 + 8 * 1, "Method: %s", desire_names[u->top_method]);

	rprintf(renderer, x, y + 16 + 8 * 2, "Strategy: %s", start_names[u->strategy]);
	rprintf(renderer, x, y + 16 + 8 * 3, "Tactic: %s", tact_names[u->tactic]);

	infont(mid_font);
	incolor1(&white);


	rprintf(renderer, x, y + 16 + 8 * 5, "Target:");

	switch (u->target_type) {
		case TARGET_TILE:
			rprintf(renderer, x, y + 16 + 8 * 6, "Some Tile");
		break;
		case TARGET_FLAG:
			rprintf(renderer, x, y + 16 + 8 * 6, "Some Flag");
		break;
		case TARGET_HOUSE:
			rprintf(renderer, x, y + 16 + 8 * 6, "%s: %s", bhouses[houses[u->target_id].tile].title, houses[u->target_id].title);
		break;
		case TARGET_UNIT:
		rprintf(renderer, x, y + 16 + 8 * 5, "Target: Unit %d", u->target_id);
			draw_unitname(renderer, &units[u->target_id], y + 16 + 8 * 6 - 4, 6, 16);			
		break;
		case TARGET_NONE:
		default:
			rprintf(renderer, x, y + 16 + 8 * 5, "Target: None");
		break;
	}

	int i, steps = 0;
	for (i = 0; i < 16; i++) {
		if (u->path[i] == -1) break;
		steps++;
	}

	rprintf(renderer, x, y + 16 + 8 * 8, "Steps: %d", steps);

}

void draw_unittabs(SDL_Renderer *renderer) {

	Uint32 x = tabicon.x;
	Uint32 y = tabicon.y;

	Uint32 color = 0xCCCCCC;

	int i;
	for (i = 0; i < MAX_UNITTABS; i++) {

		color = 0xCCCCCC;
		if (i == ui.unittab) color = 0xFFFFFF;
		else if (ui.hover == overUnitTab && i == ui.hover_id) color = 0xEEEEEE;

		incolor(color, 0);
		inprint(renderer, unittabs_names[i], x, y);

		x += tabicon.w;
		if (x + tabicon.w >= ui.log_width) {
			x = tabicon.x;
			y += tabicon.h;
		}
	}
}

void draw_unitbox(SDL_Renderer *screen) {

	Uint32 w = selbox.w;
	Uint32 h = selbox.h;

	Uint32 x = selbox.x;
	Uint32 y = selbox.y;

	draw_blackbox(screen, x, y, w, h);

	x += BOX_PADDING;
	y += BOX_PADDING;
	w -= BOX_PADDING * 2;
	h -= BOX_PADDING * 2;

	unit_t *u = &units[ui.unit];
	unit_p *p = &bunits[u->tile];

	/* :( draw face in bad way */
	if (!u->tile) draw_tile(screen, 0, 0, x, y); 
	else
		draw_visual(screen, u->axis_refs, &p->face, x, y - (p->h-1)*TILE_H);

	/* Print name */
			infont(large_font);
			inprint(screen, u->name, x + 17, y + 3);
			incolor1(&bunits[u->tile].color);
			inprint(screen, bunits[u->tile].title, x + 17, y+8+3);

	/* Draw pin */
	draw_tile(screen, TILE_UICO_Y, TILE_UICO_X + 4 + u->pin, unitpin.x, unitpin.y);

	/* Draw flag */
	if (u->flag_id[ui.faction] != -1)
		draw_tile(screen, TILE_FLAG_Y, TILE_FLAG_X + 1, flagicon.x, flagicon.y); /* flag icon */

	/* Draw health */
	SDL_Color *hp_colors[3] = {	&darkred, &red,	&darkred };
	draw_progbar(screen, (u->max_hp - u->dmg), u->max_hp, "HP: %d/%d", x + 17, y + 8 + 3, hp_colors);

	x += BOX_PADDING;
	y += BOX_PADDING;
	w -= BOX_PADDING * 2;
	h -= BOX_PADDING * 2;

	y += BOX_PADDING;

	/* Draw tabs */
	draw_unittabs(screen);

	/* Draw contents */
	switch (ui.unittab) {
		case -1:
		case 0:
			draw_unitbox_tab1(screen, u, x, y);
		break;
		case 1:
			draw_unitbox_tab2(screen, u, x, y);
		break;
		case 4:
			draw_unitbox_tab_runes(screen, u, x, y);
		break;
		case 5:
			draw_unitbox_tab_ai(screen, u, x, y);
		break;
		default:
		break;
	}
	
}

void draw_hintbox(SDL_Renderer *screen) {

	Uint32 w = PANE_WIDTH;
	Uint32 h = 128;

	Uint32 x = ui.log_width - w;
	Uint32 y = hintbox.y;

	draw_blackbox(screen, x, y, w, h);

    x += BOX_PADDING;
    y += BOX_PADDING;
	w -= BOX_PADDING * 2;
	h -= BOX_PADDING * 2;

	/* Hack -- minus 1 pixel */
	x -= 1;

    if (ui.hintType == 0) { // Building

        house_p *h = &bhouses[ui.hint];

		infont(large_font);
        incolor(0xffffff, 0);
        inprint(screen, h->title, x, y);

    }
    
    if (ui.hintType == 1) { // Stat

		infont(large_font);
        incolor(0xffffff, 0);
        inprint(screen, stat_long_names[ui.hint], x, y);

		y += 32;
		infont(small_font);
        incolor(0x999999, 0);
        inprint(screen, stat_descriptions[ui.hint], x, y);

		y += 32;
		infont(mid_font);
        incolor(0xffffff, 0);
        rprintf(screen, x, y, "1 forcible (100 blood ore)");

    }
    /* TODO: remove this */    
    infont(large_font);
}

void draw_forcible(SDL_Renderer *screen) {
	draw_tile(screen, 22, 4, ui.x, ui.y);

	unit_t *u = &units[ui.unit];
	Uint32 x = u->x * TILE_W + u->ox - ui.vx + game_map.x;
	Uint32 y = u->y * TILE_H + u->oy - ui.vy + game_map.y;

	MY_DrawSine(screen, ui.x, ui.y, x + TILE_W/2, y + TILE_H/4, reds);

	if (ui.hover == overUnit) {
		unit_t *u2 = &units[ui.hover_id];
		Uint8 ok = 1;
		if (u2 == u) ok = 0;
		SDL_Color color = { 0, 0, 0, 0x33 };
		if (ok) 	color.g = 0xFF;
		else		color.r = 0xFF;
		SDL_AlphaFill(screen,
			u2->x * TILE_W + u2->ox - ui.vx + game_map.x,
			u2->y * TILE_H + u2->oy - ui.vy + game_map.y, TILE_W, TILE_H, &color);
	}
}

void draw_builder(SDL_Renderer *screen) {

    SDL_Color color = { 0, 0, 0, 0x33 };
	house_p *h = &bhouses[ui.builder];

	/* For mouse-less devices, show the whole grid */
	if (ui.no_mouse) {
		//int i, j;		

		return;
	}

	if (ui.hover_xcollide)	color.r = 0xFF;	/* colliding, make it red */
	else            		color.g = 0xFF; /* not colliding, make it green */

	SDL_AlphaFill(screen,
		ui.hover_tx * TILE_W - ui.vx + game_map.x,
		ui.hover_ty * TILE_H - ui.vy + game_map.y,
		h->w * TILE_W, h->h * TILE_H, &color);
}

void draw_setflag(SDL_Renderer *renderer) {

    SDL_Color color = { 0, 0, 0, 0x33 };

	if (ui.hover_xcollide != ui.setflag || ui.hover == overFlag)
		color.r = 0xFF; /* make it red */
	else
		color.g = 0xFF; /* make it green */

	SDL_AlphaFill(renderer,
		ui.hover_tx * TILE_W - ui.vx + game_map.x,
		ui.hover_ty * TILE_H - ui.vy + game_map.y,
		1 * TILE_W, 1 * TILE_H, &color);


//	draw_tile(renderer, TILE_FLAG_Y, TILE_FLAG_X + ui.setflag, ui.hover_tx, ui.hover_ty);
	draw_tile(renderer, TILE_FLAG_Y, TILE_FLAG_X + ui.setflag,
		ui.hover_tx * TILE_W - ui.vx + game_map.x,
		ui.hover_ty * TILE_H - ui.vy + game_map.y
	);

}

void draw_selector(SDL_Renderer *screen, Uint8 top, Uint32 x, Uint32 y, Uint8 w, Uint8 h) {
	if (w > 2) {
		if (top)	draw_stile(screen, TILE_SEL_Y, TILE_SEL_X + 2, x, y + (h-1)*TILE_H - (h-1)*TILE_H, 2, 1, w, h);
		else    	draw_stile(screen, TILE_SEL_Y, TILE_SEL_X + 4, x, y + (h-1)*TILE_H - (h-1)*TILE_H, 2, 1, w, h);
	}
	else if (w == 2) {
		if (top)	draw_ctile(screen, TILE_SEL_Y, TILE_SEL_X + 2, x, y + (h-1)*TILE_H, 2, 1);
		else    	draw_ctile(screen, TILE_SEL_Y, TILE_SEL_X + 4, x, y + (h-1)*TILE_H, 2, 1);
	} else {
		if (top)	draw_tile(screen, TILE_SEL_Y, TILE_SEL_X, x, y + (h-1)*TILE_H);
		else    	draw_tile(screen, TILE_SEL_Y, TILE_SEL_X + 1, x, y + (h-1)*TILE_H);
	} 
}

void draw_unbuilt_house(SDL_Renderer *screen, house_t *h, int sel) {

	Uint32 x = h->x * TILE_W - ui.vx + game_map.x;
	Uint32 y = h->y * TILE_H - ui.vy + game_map.y;
	Uint32 pix_w = h->w * TILE_W;
	Uint32 pix_h = h->h * TILE_H;
	Uint8 tile = h->tile;

	house_p *bp = &bhouses[tile];

	if (sel) draw_selector(screen, 1, x, y, h->w, h->h);

	draw_btile(screen, 
		bp->body.plane.y + bp->unbuilt.y,
		bp->body.plane.x + bp->unbuilt.x,
		x, y, h->w, h->h);

	SDL_Rect built;
	
	built.x = bp->body.plane.x + bp->body.axis_offset[0].x;
	built.y = bp->body.plane.y + bp->body.axis_offset[0].y;

	int prop = h->hp * 100 / h->max_hp;
	int pix_vert =  pix_w * prop / 100;
	int pix_horz =  pix_h * prop / 100;
	//printf("Percent built: %d (in pix: %d)\n", prop, pix);
	{
		SDL_Rect src = { built.x + (pix_w - pix_vert), built.y, pix_vert, TILE_H * h->h };
		SDL_Rect dst = { x  + (pix_w - pix_vert), y, pix_vert, TILE_H * h->h };
		SDL_RenderCopy(screen, tiles, &src, &dst);
	}
	{
		SDL_Rect src = { built.x, built.y + (pix_h - pix_horz), TILE_W * h->w, pix_horz };
		SDL_Rect dst = { x, y + (pix_h - pix_horz), TILE_W * h->w, pix_horz };
        SDL_RenderCopy(screen, tiles, &src, &dst);
	}

	if (sel) draw_selector(screen, 0, x, y, h->w, h->h);
}

inline void draw_house(SDL_Renderer *screen, house_t *h, Uint32 x, Uint32 y) {
	house_p *p = &bhouses[h->tile];
	draw_visual(screen, h->axis_refs, &p->body, x, y);
}

void draw_houses(SDL_Renderer *screen) {
	int i;
	for (i = 0; i < num_houses; i++) {
		house_t *h = &houses[i];
		//if (h->tile == 0) continue;
		Uint32 x = h->x * TILE_W - ui.vx + game_map.x;
		Uint32 y = h->y * TILE_H - ui.vy + game_map.y;

		if (h->built == 0) { draw_unbuilt_house(screen, h, (ui.house==i)); continue; }

		house_p *bp = &bhouses[h->tile];

		if (ui.house == i) draw_selector(screen, 1, x, y, h->w, h->h);

		draw_visual(screen, h->axis_refs, &bp->body, x, y);

		if (ui.house == i) draw_selector(screen, 0, x, y, h->w, h->h);
	}
}

inline void draw_unit(SDL_Renderer *screen, unit_t *u, Uint32 x, Uint32 y) {
	unit_p *p = &bunits[u->tile];
	draw_visual(screen, u->axis_refs, &p->body, x, y);
}

void draw_units(SDL_Renderer *screen) {
	int i;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		unit_p *proto = &bunits[u->tile];

		Uint32 x = u->x * TILE_W + u->ox - ui.vx + game_map.x;
		Uint32 y = u->y * TILE_H + u->oy - ui.vy + game_map.y;

        if (proto->h <= 0) {
            fprintf(stderr, "Very bad unit (%p)'%s' -- [%d,%d -- %d,%d]!\n", proto, proto->id,
                proto->body.plane.x, proto->body.plane.y, proto->w, proto->h);
            exit(-1);
        }

		y -= TILE_H * (proto->h - 1);

		if (u->visiting) continue;

		if (ui.unit == i) draw_selector(screen, 1, x, y, proto->w, proto->h);

		//draw_unit(screen, u, x, y);
		draw_visual(screen, u->axis_refs, &proto->body, x, y);

		if (ui.unit == i) draw_selector(screen, 0, x, y, proto->w, proto->h);

		draw_tile(screen, TILE_UICO_Y, TILE_UICO_X + unit_icon(u), x, y);

		if (u->baloon)
		draw_tile(screen, TILE_BALOON_Y, TILE_BALOON_X + u->baloon, x - 7, y - 7);		
	}

}

void draw_flags(SDL_Renderer *renderer) {
	int i;
	for (i = 0; i < your->num_flags; i++) {
		flag_t *f = &your->flags[i];

		Uint32 x = f->x * TILE_W - ui.vx + game_map.x;
		Uint32 y = f->y * TILE_H - ui.vy + game_map.y;

		Uint8 small = 0;

		if (f->type == 1) {

			if (f->house) {

				house_t *h = f->house;
				x = h->x * TILE_W - ui.vx + game_map.x;
				y = h->y * TILE_H - ui.vy + game_map.y;

				f->x = h->x;
				f->y = h->y;
			}
			if (f->unit) {

				unit_t *u = f->unit;
				unit_p *p = &bunits[u->tile];
				if (u->visiting) continue;
				x = u->x * TILE_W + u->ox - ui.vx + game_map.x;
				y = u->y * TILE_H + u->oy - ui.vy + game_map.y;

				f->x = u->x;
				f->y = u->y - (p->h-1);

				y -= (p->h-1) * TILE_H;

				small = 1;
			}


		}

		if (ui.flag == i) draw_selector(renderer, 1, x, y, 1, 1);

		draw_tile(renderer, TILE_FLAG_Y, TILE_FLAG_X + f->type + small, x, y);

		if (ui.flag == i) draw_selector(renderer, 0, x, y, 1, 1);

	}

}


int foglet(int x, int y) {
	int mask[3][3] = { { 1 } };
	if (y > 0) {
		if (x > 0) mask[0][0] = fog[y - 1][x - 1];
		mask[0][1] = fog[y - 1][x];
		if (x < LEVEL_W) mask[0][2] = fog[y - 1][x + 1];
	}
	if (y < LEVEL_H) {
		if (x > 0) mask[2][0] = fog[y + 1][x - 1];
		mask[2][1] = fog[y + 1][x];
		if (x < LEVEL_W) mask[2][2] = fog[y + 1][x + 1];
	}
	if (x > 0) {
		mask[1][0] = fog[y][x - 1];
	}
	if (x < LEVEL_W) {
		mask[1][2] = fog[y][x + 1];
	}
	if (fog[y][x] == 0) return 0;

	if (!fog[y-1][x] && !fog[y+1][x-1] && !fog[y+1][x+1]) return 0;
	if (!fog[y][x-1] && !fog[y-1][x+1] && !fog[y+1][x+1]) return 0;
	if (!fog[y][x+1] && !fog[y-1][x-1] && !fog[y+1][x-1]) return 0;
	if (!fog[y+1][x] && !fog[y-1][x-1] && !fog[y-1][x+1]) return 0;
	if (!fog[y-1][x] && !fog[y][x-1] && !fog[y+1][x+1]) return 0;
	if (!fog[y+1][x] && !fog[y][x+1] && !fog[y-1][x-1]) return 0;
	if (!fog[y+1][x] && !fog[y][x-1] && !fog[y-1][x+1]) return 0;
	if (!fog[y-1][x] && !fog[y][x+1] && !fog[y+1][x-1]) return 0;

	if (!fog[y+1][x-1] && !fog[y-1][x+1] && !fog[y+1][x+1] && fog[y][x-1] && fog[y-1][x] ) return 11;
	if (!fog[y+1][x-1] && !fog[y][x+1] && fog[y][x-1] && fog[y-1][x] ) return 11;
	if (!fog[y-1][x+1] && !fog[y+1][x] && fog[y][x-1] && fog[y-1][x] ) return 11;
	if (!fog[y+1][x] && !fog[y][x+1] && fog[y][x-1] && fog[y-1][x] ) return 11;

	if (!fog[y-1][x-1] && !fog[y+1][x-1] && !fog[y-1][x+1] && fog[y-1][x] && fog[y+1][x] ) return 19;
	if (!fog[y-1][x+1] && !fog[y][x-1] && fog[y][x+1] && fog[y+1][x] ) return 19;
	if (!fog[y+1][x-1] && !fog[y-1][x] && fog[y][x+1] && fog[y+1][x] ) return 19;
	if (!fog[y-1][x] && !fog[y][x-1] && fog[y][x+1] && fog[y+1][x] ) return 19;

	if (!fog[y-1][x-1] && !fog[y+1][x+1] && fog[y-1][x+1] && !fog[y+1][x] && fog[y][x+1] && fog[y-1][x] ) return 13;
	if (!fog[y-1][x-1] && !fog[y+1][x] && fog[y][x+1] && fog[y-1][x] ) return 13;
	if (!fog[y+1][x+1] && !fog[y][x-1] && fog[y][x+1] && fog[y-1][x] ) return 13;
	if (!fog[y][x-1] && !fog[y+1][x] && fog[y][x+1] && fog[y-1][x] ) return 13;

	if (!fog[y-1][x-1] && !fog[y+1][x+1] && !fog[y-1][x+1] && fog[y][x-1] && fog[y+1][x] ) return 17;
	if (!fog[y][x+1] && !fog[y-1][x] && fog[y][x-1] && fog[y+1][x] ) return 17;
	if (!fog[y-1][x-1] && !fog[y][x+1] && fog[y][x-1] && fog[y+1][x] ) return 17;
	if (!fog[y+1][x+1] && !fog[y-1][x] && fog[y][x-1] && fog[y+1][x] ) return 17;

	if (fog[y][x-1] == 0 && fog[y][x+1] == 0) return 0;
	if (fog[y-1][x] == 0 && fog[y+1][x] == 0) return 0;
	
	if (fog[y-1][x] && fog[y+1][x]) {
		if (!fog[y-1][x-1] && !fog[y+1][x+1] && fog[y-1][x+1]) return 14;
		if (!fog[y+1][x-1] && !fog[y-1][x+1] && fog[y+1][x+1]) return 16;
	}

	if (fog[y - 1][x + 1] && fog[y][x - 1] && fog[y][x + 1] && !fog[y + 1][x - 1] && !fog[y + 1][x + 1]) return 2;
	if (fog[y + 1][x + 1] && fog[y][x - 1] && fog[y][x + 1] && !fog[y - 1][x - 1] && !fog[y - 1][x + 1]) return 8;
	if (fog[y - 1][x] && fog[y + 1][x] && !fog[y - 1][x + 1] && !fog[y + 1][x + 1]) return 4;
	if (fog[y - 1][x] && fog[y + 1][x] && !fog[y - 1][x - 1] && !fog[y + 1][x - 1]) return 6;

	if (fog[y][x + 1] && fog[y + 1][x] && !fog[y + 1][x + 1]) return 1;
	if (fog[y][x - 1] && fog[y + 1][x] && !fog[y + 1][x - 1]) return 3;
	if (fog[y][x + 1] && fog[y - 1][x] && !fog[y - 1][x + 1]) return 7;
	if (fog[y][x - 1] && fog[y - 1][x] && !fog[y - 1][x - 1]) return 9;

	if (!fog[y][x + 1]) return 4;	
	if (!fog[y][x - 1]) return 6;
	if (!fog[y - 1][x]) return 8;
	if (!fog[y + 1][x]) return 2;

	return 5;
}
void draw_scent(SDL_Renderer *screen) {

	int tx, ty;
	for (ty = 0; ty < LEVEL_H; ty++) {	
		for (tx = 0; tx < LEVEL_W; tx++) {
			
			Uint32 x = tx * TILE_W - ui.vx + game_map.x;
			Uint32 y = ty * TILE_H - ui.vy + game_map.y;
			int n;
            SDL_Color color = { 0xff, 0xff, 0x00, 0x33 };
		    color.b = scent_human[ty][tx]/10;

			for (n = 0; n < scent_human[ty][tx]/16; n++)

			SDL_AlphaFill(screen, x + rand()%TILE_W/4, y + rand()%TILE_H/4, TILE_W, TILE_H, &color);
		}
	}
}

void draw_fog(SDL_Renderer *screen) {

	int tx, ty;
	Uint8 fols_x[20] =  {-1, 0, 1, 2, 0, 1, 2, 0, 1, 2, 1, 0, 1, 2, 0, 1, 2, 0, 1, 2 };
	Uint8 fols_y[20] =  { 0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 1, 3, 3, 3, 4, 4, 4, 5, 5, 5 };

	for (ty = 0; ty < LEVEL_H; ty++) {	
		for (tx = 0; tx < LEVEL_W; tx++) {

			Uint32 x = tx * TILE_W - ui.vx + game_map.x;
			Uint32 y = ty * TILE_H - ui.vy + game_map.y;

			Uint8 f = foglet(tx, ty);
			fog[ty][tx] = f;

			draw_tile(screen, TILE_FOG_Y + fols_y[f], TILE_FOG_X + fols_x[f], x, y);  
		}
	}

}

void draw_log(SDL_Renderer *renderer, log_t *log) {

	int i;

	Uint32 y = ui.log_height - 16;

	for (i = 0; i < 16; i++) {
	
		//incolor(log->color[i], 0);
		infont(mid_font);
		incolor(0xffffffff, 0);
		inprint(renderer, log->message[i], 0, y);

		y -= 16;
	
	}
	infont(large_font);
}


void draw_ui(SDL_Renderer *screen) {
	SDL_Rect src = { 0, 0, 640, 480 };
	SDL_Rect dst = { 0, 0, ui.log_width, ui.log_height };

	if (ui.draw_uibg)
	SDL_RenderCopy(screen, uibg, &src, &dst);

	draw_goldbox(screen, 2, 3, 0, your->ore);
	draw_goldbox(screen, 80, 3, 1, your->gold);

	draw_minimap(screen);

	if (ui.unit != -1) draw_unitbox(screen);
	else if (ui.house != -1) draw_housebox(screen);
	else if (ui.flag != -1) draw_flagbox(screen);
	else {
		draw_buildset(screen);
		draw_pinbox(screen);
	}

	if (ui.hint != -1) draw_hintbox(screen);

	if (ui.draw_log) draw_log(screen, &gamelog); 
}

void draw_screen(SDL_Renderer *screen) {

	SDL_SetRenderDrawColor(screen, 0x11, 0x33, 0x11, 255);
	SDL_RenderClear(screen);

	draw_houses(screen);

	draw_units(screen);

	if (ui.draw_fog) draw_fog(screen);
	if (ui.draw_scent) draw_scent(screen);

	draw_flags(screen);

	draw_particles(screen, 100,100);

	if (ui.draw_overlays) draw_overlays(screen, ui.unit, 0, 0, NULL);

	if (ui.draw_path) draw_paths(screen, ui.unit, 0, 0);

	if (ui.draw_pools) draw_pools(screen);

	if (ui.setflag > -1) draw_setflag(screen);
	if (ui.builder > -1) draw_builder(screen);
	if (ui.stat > -1) draw_forcible(screen);

	draw_ui(screen);

	/* FPS: */
	infont(small_font);
	incolor(0xffffff, 0);
	rprintf(screen, ui.log_width - ui.log_width / 2, 4, "FPS: %d", ui.fps);
}


