#ifndef _RUNEDRAW_H
#define _RUNEDRAW_H

extern SDL_Texture *tiles;
extern SDL_Texture *uibg;
extern SDL_Texture *large_font;
extern SDL_Texture *mid_font;
extern SDL_Texture *small_font;

/* Color names ... */
extern SDL_Color white;
extern SDL_Color black;
extern SDL_Color magenta;

extern SDL_Color reds[16];

/* Hardcoded locations on the tileset */

#define TILE_BUILD_X 20
#define TILE_BUILD_Y 0

#define TILE_SEL_X 6
#define TILE_SEL_Y 15

#define TILE_BTN_X 7
#define TILE_BTN_Y 19

#define TILE_STAT_X 5
#define TILE_STAT_Y 18

#define TILE_UICO_X 7
#define TILE_UICO_Y 16

#define TILE_BALOON_X 5
#define TILE_BALOON_Y 17

#define TILE_FOG_X	1
#define TILE_FOG_Y	16

#define TILE_RES_X 5
#define TILE_RES_Y 16

#define TILE_BUTTON_X 5
#define TILE_BUTTON_Y 20

#define TILE_FLAG_X 5
#define TILE_FLAG_Y 24

/* Externals */
extern void draw_particles(SDL_Renderer *target, Uint32 bx, Uint32 by);

/* Public */
extern void prepare_colors();
extern void draw_tile(SDL_Renderer *screen, Uint8 id, Uint8 frame, Uint32 x, Uint32 y);

extern void draw_ui(SDL_Renderer *screen);
extern void draw_screen(SDL_Renderer *screen);

extern int rprintf(SDL_Renderer *renderer, Uint32 x, Uint32 y, const char *fmt, ...);

#endif