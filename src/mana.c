#include <SDL.h>
#include "rune.h"
#include "libs/binaryheap/binhl.h"

/* Define this to get lots of _printf's */
//#define DEBUG_MANA_POOLS

/* DYNAMIC POOLS are pools that are managed in real-time, i.e. pools that can 
split into two and join together. STATIC POOLS are re-built each game turn
from the linked list information. DYNAMIC POOLS are buggy/don't work properly.
So I'm sticking with STATIC POOLS for now. They are easier to work with too. */
/* #define DYNAMIC_POOLS */

#ifdef DEBUG_MANA_POOLS
#define _printf(...) fprintf (stdout, __VA_ARGS__)
#else
#define _printf(...) { }
#endif

extern int num_units;
extern int num_pools;

extern unit_t units[MAX_UNITS];
extern pool_t pools[MAX_POOLS];

int create_pool(int id1, int id2, Uint8 stat) {
	int pid = num_pools;
	pool_t *pool = &pools[pid];

	unit_t *u = &units[id1];
	unit_t *u2 = &units[id2];

	pool->head = u2;
	pool->tail = u;
	pool->stat = stat;	

	u->pool = pool;
	_printf("CREATED POOL!\n");	

	num_pools++;
	return pid;
}

int is_unit_looped(unit_t *start, int stat) {
	int i;
	for (i = 0; i < num_units; i++) units[i].block = 0;
	unit_t *iter = start->link;
	while (iter) {
		if (iter->link_stat != stat) break;
		if (iter->block == 1) break;
		if (iter == start) return 1;	
		iter->block = 1;
		iter = iter->link;
	}
	return 0;
}

void rebuild_pools() {

	num_pools = 0;
int k;for (k = 0; k < MAX_STAT; k++) {

	int i;

	binh_list uunits = { 0 };
	for (i = 0; i < num_units; i++) {
		units[i].pool = NULL;
		binhl_push(&uunits, i, units[i].ref_count[k]);
	}

	_printf("\n       Rebuilding mana pools for [%d] (found %d units for it)\n", k, uunits.len);

	while (uunits.len) {

		i = binhl_pop(&uunits);
		unit_t *u = &units[i];

		printf("Trying unit %s\n", u->name);

		if (u->pool) { printf ("Already proc\n"); continue;}//already procesed
		if (!u->link) { printf("no link\n"); continue;}
		int stat = u->link_stat;

		if (stat != k) {printf("Wrong stat! %d\n", stat);continue;}

		_printf("Following unit %s (count: %d)\n", u->name, u->ref_count[stat]);

		int pid = create_pool(i, 0, u->link_stat);

		pool_t *pool = &pools[pid];

		u->pool = pool;

		int making_open_chain = !is_unit_looped(u, stat);
		u = u->link;
		while (u) {
			//pif (u->ref_count > 1) return;

			_printf("Now unit %s\n", u->name);

			if (u == pool->tail) {_printf("1\n");break;}	
			pool->head = u;

			if (making_open_chain && is_unit_looped(u, stat)) {_printf("2\n");break;}

			if (u->pool) break;
			u->pool = pool;
			_printf("Adding him to this pool\n");
			//if (u->link->ref_count > 1) break;

			if (u->link_stat != stat) {
				/* Re-add him */
				if (u->link) u->pool = NULL;
				//binhl_push(&uunits, i, 0);
				break;
			}
			u = u->link;
		}
	}


}/////
printf("Total pools: %d\n", num_pools);
#ifdef DEBUG_MANA_POOLS
	int i;	
	for (i = 0; i < num_pools; i++) {
		pool_t *pool = &pools[i];
		unit_t *iter = pool->tail;
		int j = 0;
		_printf("Created pool %p(%d), head -- %s, tail -- %s\n", pool, i, (pool->head ? pool->head->name : "NULL"), (pool->tail ? pool->tail->name : "NULL"));
		while(iter) {
			_printf("Member %d -- %s, linked to %p (%s) pool\n", j, iter->name, iter->pool, iter->pool == pool ? "THIS" : "OTHER");
			if (pool->head == iter) break;
			iter = iter->link;
			j++;
		}
	}
#endif
}

void add_link(int id1, int id2, Uint8 stat) {
	unit_t *u = &units[id1];
	unit_t *u2 = &units[id2];
	
	u->baloon = 1;
	u2->baloon = 4;	

	u->link = u2;
	u->link_id = id2;
	u->link_stat = stat;
	u2->ref_count[stat]++;
	u2->ref_counts++;

	/* Mana Pools */
#ifdef DYNAMIC_MANA_POOLS
	add_wtf(id1, id2, stat);
#else
	rebuild_pools();
#endif
}

void update_stats(int reset) 
{
	int i, j;
	if (reset) {
		for (i = 0; i < num_units; i++) {
			units[i].block = 0;
			for (j = 0; j < MAX_STAT; j++) {
				units[i].rune_stat[j] = 0;		
			}
		}
	}

	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		for (j = 0; j < MAX_STAT; j++) {
			u->calc_stat[j] = u->base_stat[j] + u->rune_stat[j];
			if (u->calc_stat[j] > 99) u->calc_stat[j] = 99;
			u->stat_bid[j] = 0;
		}
	}
}

#define IN_POOL(POOL, UNIT) ((UNIT)->pool == (POOL))
#define IS_LOOPED(POOL) ((POOL)->head->link == (POOL)->tail && (POOL)->head->link_stat == (POOL)->stat)
#define IS_ADJACENT(POOL) ((POOL)->head->pool != (POOL) && (POOL)->head->pool)

void collect_pools() {
	int i;

	update_stats(1);	

	binh_list kpools = { 0 }; /* while iterating, collect "adjacent" pools in here */

	_printf("Collecting energy: \n");

	for (i = 0; i < num_pools; i++) {
		pool_t *pool = &pools[i];
		unit_t *iter = pool->tail;
		int rc = 0;
		int closed = IS_LOOPED(pool);
		do {
			_printf("On unit: %s\n", iter->name);
			if (!closed && iter == pool->head) break; /* Do not collect from heads of open chains */ 
			pool->pool += iter->base_stat[pool->stat];
			iter->rune_stat[pool->stat] -= iter->base_stat[pool->stat];
			if (iter->ref_count[pool->stat] > rc) rc = iter->ref_count[pool->stat];
			if (iter == pool->head) break; /* We're now looping in a closed chain */
			iter = iter->link;

		} while (iter);// && iter->pool == pool);

		_printf(" pool #%d [%s-%s (%d)] -- collected %d\n",i, pool->tail->name,pool->head->name, rc, pool->pool);

		if (IS_ADJACENT(pool)) binhl_push(&kpools, i, rc);		
	}
/*
}
void prop_pools() {

	binh_list kpools = { 0 };
	int i;
	for (i = 0; i < num_pools; i++) {
		if (IS_ADJACENT(&pools[i]) binhl_push(&kpools, i, pools[i].tail->ref_count); 
	}
*/
	/* forward adjacent pools */ 
	while(kpools.len) {
		i = binhl_pop(&kpools);	

		pool_t *pool = &pools[i];
		
		_printf("Forwarding pool #%d [%s-%s (%d)]\n", i, pool->tail->name, pool->head->name, pool->head->ref_count[pool->stat]);		

		if (pool->head->pool == NULL) {
			_printf("Nowhere to forward! :((\n");
			continue;
		}

		pool_t *nextp = pool->head->pool;
		nextp->pool += pool->pool;
		pool->pool = 0;
	}

	update_stats(0);
}

void distrib_pools() {
	int i;

//	binh_list kpools = { 0 };
	//for (i = 0; i < num_pools; i++)
		//binhl_push(&kpools, i, pools[i].tail->ref_count); 

	for (i = 0; i < num_pools; i++) {
//	while(kpools.len) {
//		i = binhl_pop(&kpools);	

		_printf("Distributing pool #%d\n",i);

		pool_t *pool = &pools[i];

		if (IS_LOOPED(pool)) { /* Closed chain */

			/* Pick bidders */
			binh_list uunits = { 0 };
			int num_total = 0, num_bids = 0;
			int large_value = num_units * 10; /* using this to invert priority */

			_printf("Distributing closed chain:\n");

			unit_t *iter = pool->tail;
			do {

				int priority = iter->stat_bid[pool->stat] + iter->pin;

				_printf("Examining %d=%s (priority: %d, sort: %d)\n", num_total, iter->name, priority, large_value - priority);

				binhl_push(&uunits, num_total++, large_value - priority);

				if (priority) num_bids++;

				iter = iter->link;

			} while (iter != pool->tail);

			if (num_bids == 0) num_bids = num_total; /* zero bids == everyone */

			_printf("Picked %d units (of %d) for distribution\n", num_bids, num_total);

			/* Distribute among them */
			Uint16 share = pool->pool / num_bids;
			Uint16 bonus = pool->pool % num_bids;

			while (num_bids--) {

				int j = binhl_pop(&uunits); /* "j" is "offset from tail" */

				_printf("Need to give to %d=", j);

				for (iter = pool->tail; j > 0; j--) /* grab a unit using "j" */
				{
					_printf("%s->", iter->name);
					iter = iter->link;
				}

				_printf("\nGiving %d to %s\n", share, iter->name);
				iter->rune_stat[pool->stat] += share;
				//iter->calc_stat[pool->stat] += share;
			}

			_printf("Giving extra %d to %s\n", bonus, iter->name);
			iter->rune_stat[pool->stat] += bonus;
			//iter->calc_stat[pool->stat] += bonus;

		} else { /* Open chain */

			/* Just give everything to pool's "head" */
			pool->head->rune_stat[pool->stat] += pool->pool;
			//iter->calc_stat[pool->stat] += pool->pool;
		}

		/* Drain the pool */
		pool->pool = 0;
	}

	update_stats(0);
}


/* For testing: */
void random_links() {
	int j = 100;
	while (j--) {
		Uint8 u = rand() % num_units;
		int w = rand() % 5;
		while (w--) {
			Uint8 u2 = rand() % num_units;
			if (u2 != u) {
				add_link(u, u2, 1);
			}
		}	
	}
}








#ifdef DYNAMIC_POOLS
void add_to_pool(pool_t *pool, int id1, int id2, Uint8 stat) {
	unit_t *u = &units[id1];
	unit_t *u2 = &units[id2];
	
	if (pool->head->link == u2) {
		_printf("Adding to the top\n");
		//pool->head->pool = pool;
		pool->head = u2;
		u->pool = pool;
	} else if (pool->tail == u2) {
		_printf("Adding to the tail\n");
		pool->tail = u;
		u->pool = pool;
	} else {
		_printf("Adding to the middle\n");
		create_pool(id1, id2, stat);
	}
}

pool_t* find_pool(int unit_id, Uint8 stat) {
	unit_t *iter = &units[unit_id];
	while (iter) {
		_printf("Searching for pool..%s\n", iter->name);
		if (iter->pool)
			_printf("I have one!");
		else
			_printf("I don't have one!\n");
		if (iter->pool)// && iter->pool->stat == stat)
			return iter->pool;
		_printf("Moving to %p\n",iter->link);
		if (iter->link_stat == stat)
			iter = iter->link;
		else break;
	}
	int i;
	pool_t *good = NULL; 
	for (i =0; i < num_pools; i++) {
		if (pools[i].head ==&units[unit_id]
		&& pools[i].stat == stat) {
			if (good) return NULL;
			good = &pools[i];
		} 
	}
	return good;
}

void add_wtf(int id1, int id2, Uint8 stat) {
	unit_t *u = &units[id1];
	unit_t *u2 = &units[id2];

	pool_t *pool = 0;
	pool = find_pool(id1, stat);
	if (pool == NULL) {
//		pool = 
		create_pool(id1, id2, stat);
	} else {
		add_to_pool(pool, id1, id2, stat);
	}
}

unit_t* find_prev(pool_t *pool, unit_t *who) {
	unit_t*iter = pool->tail;
	while (iter->link) {
		if (iter->link == who) break;
		iter = iter->link;
	}
	return iter;
}
pool_t* clone_pool(pool_t *pool) {
	int pid = num_pools;
	memcpy(&pools[pid], pool, sizeof(pool_t));
	num_pools++;
	return &pools[pid];
}
void switch_pool(unit_t *tail, unit_t *border, pool_t *new_pool) {
	unit_t*iter = tail;
	pool_t *pool = tail->pool;
	while (iter) {
		if (iter->pool == pool) iter->pool = new_pool;
		if (iter->link == border) break;
		iter = iter->link;
	}
}

void remove_pool(pool_t *pool) {
	int i = 0;
	switch_pool(pool->tail, pool->head, NULL);	
	for (i = 0; i < num_pools; i++) {
		if (&pools[i] == pool) {
			if (i < num_pools-1) {
				memcpy(&pools[i], &pools[num_pools], sizeof(pool_t));
			}
			num_pools--;
		}
	}
}
 
void split_pool(pool_t *pool, unit_t *u) {

	/* Before we even start the split, let's consider the
	 * alternatives:
	 * o - x - o 		In this case, we just remove.
	 * o - o - x - o	Do not create right hand pool, just CUT 	
	 * o - x - o - o	Mutate left-hand pool 
	 */

	 /* O - X - O / aka the Trigraph is easy to spot */
	 if (pool->tail->link == u && u->link == pool->head)
	 {
	 	_printf("It's just a trigraph!\n");
	 	remove_pool(pool);
	 	return;
	 }
	 /* O - ? - ? - O / ok this is getty lame */
	 if (pool->tail->link && pool->tail->link->link && pool->tail->link->link->link == pool->head)
	 {
	 		unit_t *prev = find_prev(pool, u);	 
	 	if (u->link == pool->head) {
	 		_printf("It's a riftized foragraph!\n");
	 		pool->head = prev;
	 	}
	 	if (pool->tail->link == u) {
	 		_printf("It's a leftized foragrpah...!\n");
	 		switch_pool(pool->tail, u, NULL);
	 		pool->tail = u->link;
	 	}
	 	
	 	return;
	 
	 }

	pool_t *nextp = clone_pool(pool);
	unit_t *prev = find_prev(pool, u);

	pool->head = prev;		
	nextp->tail = u->link;

	if (nextp->tail == nextp->head) {
		_printf("Pool is now empty, remove it!\n");
		remove_pool(nextp);
	} 
	else 
	{
		unit_t *mark = nextp->tail;
		while (mark) {
			if (mark->pool == pool)
				mark->pool = nextp;
			if (mark->link->link_stat == u->link_stat)/* ??? was mark->link for some reason ??? */
				mark = mark->link;
			else break;
		} 
	}
	if (pool->tail == pool->head) {
		_printf("Pool is now empty, remove it!\n");
		remove_pool(pool);
	}	
}

pool_t* find_Xpool(int unit_id, Uint8 stat) {
	unit_t *iter = &units[unit_id];
	pool_t *good = NULL; 
	while (iter) {
		_printf("%s links to ...", iter->name);
		if (iter->link) _printf("%s ", iter->link->name);
		_printf(" and has connection to pool %p\n", iter->pool);
		if (iter->pool && iter->pool->stat == stat)
			return iter->pool;
		//if (iter->link_stat == stat)
			iter = iter->link;
		//else break;
	}
	int i;
	good = NULL;
	for (i =0; i < num_pools; i++) {
		if (pools[i].head ==&units[unit_id]
		) {
//			if (good) 
			return &pools[i];
			//good = ;
		} 
	}
	return good;
}

void remove_wtf(int id1) {
	unit_t *u = &units[id1];
	
	pool_t *pool = 0;
	pool = find_pool(id1, u->link_stat);
	
	if (!pool) {
		_printf("No pool found...\n");	
		pool = find_Xpool(id1, u->link_stat);	
	}	
	if (!pool) {
		_printf("Still no pool!\n");
		return;
	}
	
	if (pool->tail == u) {
		_printf("Was at the very very end!\n");
		pool->tail = pool->tail->link;
		_printf("Shrinking pool\n");
		if (pool->tail == pool->head) {
			_printf("Pool is now empty, remove it!\n");
			remove_pool(pool);
			return;//or continue;
		}
	}
	else if (pool->head == u) {
		_printf("At the very head!\n");
		_printf("Fidn me prev...");
		
		unit_t *prev = find_prev(pool, u);
		pool->head = prev;
		if (pool->tail == pool->head) {
			_printf("Pool is now empty, remove it!\n");
			remove_pool(pool);
			return;//or continue;
		}
	}
	else {
		split_pool(pool, u);
	}
	_printf("HIS POOL %p!\n",pool);
}
#endif
