#ifndef _RUNE_VISUAL_H
#define _RUNE_VISUAL_H

#include <SDL.h>

#define MAX_AAXIS 4
#define MAX_AANIM 8
#define MAX_AFRAME 16

typedef struct animation_t {

	SDL_Texture *image;
	SDL_Rect plane;

	SDL_Rect axis_offset[MAX_AAXIS];
	Uint8 axis_modifier[MAX_AAXIS];
	Uint8 num_axises;

	Sint8 frame[MAX_AANIM][MAX_AFRAME];
	Uint8 num_frames[MAX_AANIM];

} animation_t;

#endif