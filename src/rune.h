#ifndef _RUNE_H
#define _RUNE_H

#include <SDL.h>
#include "visual.h"

#define MAX_FACTIONS 8

#define MAX_UNITS 512
#define MAX_HOUSES 128
#define MAX_FLAGS 64	/* (per faction) */

#define MAX_POOLS MAX_UNITS

#define TILE_W	16
#define TILE_H	16

#define MAX_ANIM 6
#define ANIM_IDLE	0
#define ANIM_WALK	1
#define ANIM_CARRY	2
#define ANIM_HIT	3
#define ANIM_DEATH	4
#define ANIM_FACE	5

#define ANIM_BUILDING	0
#define ANIM_BUILT   	1
#define ANIM_WORK   	3
#define ANIM_DAMAGE   	4
#define ANIM_ICON   	5

#define UAXIS_TYPE 0
#define UAXIS_MODE 1
#define UAXIS_FRAME 2
#define UAXIS_FACEFRAME 3

#define HAXIS_TYPE 0
#define HAXIS_STATE 1
#define HAXIS_FRAME 2

#define HACT_NONE       	0
#define HACT_UNLOAD_GOLD	0x0001 
#define HACT_UNLOAD_ORE 	0x0002
#define HACT_XXX1       	0x0004
#define HACT_XXX2       	0x0008
#define HACT_XXX3       	0x0010
#define HACT_XXX5       	0x0020
#define HACT_XXX6       	0x0040
#define HACT_XXX7       	0x0080

//#define DEBUG_PATHFIND

/* How much "energy" various unit actions cost */
#define ECOST_SLIDE	5
#define ECOST_VISIT	10
#define ECOST_WALK	10

typedef enum MoveMethod {
	Walk, Ride, Fly, Trample, Skyride, 
} MoveMethod;
typedef enum AttackMethod {
	Melee, Ranged, Cast, Talk
} AttackMethod;
typedef enum SpellType {
	None, Heal, Enchant, Descent, Burry, Summon, Protect, Earthskin
} SpellType;
typedef enum PerceptionMethod {
	Sight, Smell
} PerceptionMethod;


#define LEN_UNIT_ID 	32
#define LEN_UNIT_TITLE	80
#define LEN_UNIT_NAME	80

#define LEN_HOUSE_ID	32
#define LEN_HOUSE_TITLE	80
#define LEN_HOUSE_NAME	80

#define LEN_FACTION_TITLE 80

#define MAX_UNITTABS 6
#define MAX_HOUSETABS 6

#define MAX_SCENT 8
#define SCENT_HUMAN 	0
#define SCENT_ANIMAL	1
#define SCENT_MAGIC 	2

#define MM_MAX 4	
#define MM_WALK	0
#define MM_RIDE 1
#define MM_FLY	2
#define MM_TRAMPLE 3

#define AM_MAX  	4
#define AM_MELEE	0
#define AM_RANGED	1
#define AM_CAST 	2
#define AM_TALK 	3

#define SPELL_MAX	10
#define SPELL_MAGICMISSILE	0
#define SPELL_HEAL	1
#define SPELL_ENCHANT	2
#define SPELL_DESCENT	3
#define SPELL_BURRY	4

#define MAX_SKILLS	7
#define SKILL_BUILD 	0
#define SKILL_REPAIR	1
#define SKILL_MELEE 	2
#define SKILL_RANGED	3
#define SKILL_RIDE  	4
#define SKILL_MAGERY	5
#define SKILL_TRADE 	6

#define MAX_STAT	6
#define S_STR	0
#define S_DEX	1
#define S_CON	2
#define S_INT	3
#define S_MET	4
#define S_CHR	5
#define MAX_PARAMS (MAX_STAT + MAX_STAT)

#define HMENU_ITEMS 10

#define MAX_RESOURCES 7
#define RES_GOLD    	0
#define RES_ORE     	1
#define RES_FORCIBLE	2
#define RES_SWORD   	3
#define RES_BOW     	4
#define RES_HORSE   	5
#define RES_GRAAK   	6

typedef struct house_p house_p;
typedef struct house_t house_t;
typedef struct unit_t unit_t;
typedef struct unit_p unit_p;

typedef struct faction_t faction_t;
typedef struct flag_t flag_t;

typedef struct pool_t pool_t;

struct flag_t {

	Uint8 faction;
	Uint8 type;

	Uint16 reward;

	Uint8 x;
	Uint8 y;
	house_t *house;
	unit_t *unit;
	Uint8 w;
	Uint8 h;

	unit_t *units[8];
	int num_units;
};

struct faction_t {
	char title[LEN_FACTION_TITLE];
	SDL_Color color;

	Uint32 gold;
	Uint32 ore;

	Uint8 gold_tax;
	Uint8 ore_tax;
	
	flag_t flags[MAX_FLAGS];
	int num_flags;
};

struct unit_p {
	char id[LEN_UNIT_ID];
	char title[LEN_UNIT_TITLE];
	SDL_Color color;

	Sint8 base_stat[MAX_STAT];

	AttackMethod attack;
	MoveMethod move;
	SpellType spell;
	PerceptionMethod perc;

	Uint8 base_scent[MAX_SCENT];

	Uint32 flags;
	Uint8 modus;

	Uint8 w;
	Uint8 h;

	animation_t body;
	animation_t face;
};

struct house_p {
    char id[LEN_HOUSE_ID];
	char title[LEN_HOUSE_TITLE];

	Uint32 gold_cost;
	Uint32 ore_cost;

	Uint8 w;
	Uint8 h;

	/* Visuals */
	Uint8 icon;
	SDL_Rect unbuilt;
	animation_t body;
};

struct house_t {
	Uint8 tile;
	char title[LEN_HOUSE_TITLE];
	Uint32 gold_cost;
	Uint8 w;
	Uint8 h;

	Uint8 x;
	Uint8 y;

	Uint8 faction;
	Sint8 flag_id[MAX_FACTIONS];	/* Back-ref to assigned flags */

	Uint8 capacity;
	Uint8 visitors;

	Uint16 hp;
	Uint16 max_hp;

	Uint8 built;	/* Construction finished */

	/* Visuals */
	Sint8 framecnt;	/* Frame counter */
	Uint8 frame;	/* Current animation frame */
	Uint8 *axis_refs[MAX_AAXIS]; /* reservered */
};

struct unit_t {
	Uint8 tile;

	Sint8 base_stat[MAX_STAT];
	Sint8 rune_stat[MAX_STAT];
	Sint8 calc_stat[MAX_STAT];

	Uint8 stat_bid[MAX_STAT];

	Uint8 base_skill[MAX_SKILLS];
	Uint8 calc_skill[MAX_SKILLS];

	char name[LEN_UNIT_NAME];

	Uint16 gold;
	Uint16 fame;
	Sint16 happiness;

	Uint16 dmg;
	Uint16 max_hp;
	Uint8 level;
	Uint8 exp;

	Uint16 progress;
	Uint16 carry_gold;
	Uint16 carry_ore;

	house_t *visiting;	/* link to container house (i.e. "gold mine") */

	unit_t  *inside;	/* link to container unit (i.e. "transport ship") */
	Uint8 capacity;		/* number of child units this can hold */
	Uint8 passengers;	/* number of child units currently holding */

	Uint8 x;
	Uint8 y;

	Uint8 tx;
	Uint8 ty;

	Sint8 ox;
	Sint8 oy;

	Uint32 energy;

	Uint8 pin;
	Uint8 mode;
	Uint8 baloon;
	
	Sint8 faction;	/* Faction id */
	Sint8 flag_id[MAX_FACTIONS];

	Uint32 color;

	Uint8 top_desire; /* AI-related */
	Uint8 top_method;
	Uint8 modus;
	Uint8 strategy;
	Uint8 tactic;
	Uint8 step;

	Uint8 block;	/* Useless variable to mark during iteration to avoid endless recursion */

	unit_t *link;
	Uint8 ref_count[MAX_STAT];
	Uint16 link_id;
	Uint8 link_stat;

	pool_t *pool;
	Uint8 ref_counts;

	int path[16];
	Uint8 target_type;
	Uint16 target_id;
#ifdef DEBUG_PATHFIND
	long d[256000]; /* d[i] is the length of the shortest path between the source (s) and node i */
	int prev[256000]; /* prev[i] is the node that comes right before i in the shortest path from the source to i*/
	int tar_x;
	int tar_y;
#endif

	/* Visuals */
	Uint8 frame;
	Sint8 framecnt;
	Uint8 faceframe;
	Uint8 *axis_refs[MAX_AAXIS]; /* reservered */

	/* Need refresh */
	struct {
		int stats;	/* Need to update stats due to magic linking */
		int skills; /* Need to update skills due to stats change/level up/equipment change */
		int target; /* Need to find new target */
		int path;   /* Need to find new path to target */
	} refresh;
};

struct pool_t {
	unit_t *tail;
	unit_t *head;
	Uint8  stat;
	Uint16 pool;
};

#endif