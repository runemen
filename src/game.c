#include "game.h"
#include "mana.h"

int level_w;
int level_h;

int turns = 0;

unit_t units[MAX_UNITS];
house_t houses[MAX_HOUSES];
int num_houses = 0;
int num_units = 0;

pool_t pools[MAX_POOLS];
int num_pools = 0;

faction_t factions[8];
faction_t *your;

/*** ***/
const char housetabs_names[MAX_HOUSETABS][80] = {
	"Info", "Items", "Visitors", "Research", "Upgrade", "Home"
};
const char unittabs_names[MAX_UNITTABS][80] = {
	"Info", "Stats", "Skills", "Items", "Runes", "Home"
};

const char desire_names[MAX_DESIRES][80] = {
	"None", "Rune", "Gold", "Power", "Love", "Protect", "Destroy", "Killall", "Helpall",
};
const char start_names[MAX_STRATEGIES][80] = {
	"None", "Work", "Hunt", "Kill", "Heal", "Hide"
};
const char tact_names[MAX_TACTICS][80] = {
	"None", "Mine", "Build", "Repair", "Train", "Buy", "Charm", "Attack", "Hide", "Run"
};
const char modus_names[MAX_MODUS][80] = {
	"",
};

const char stat_names[MAX_STAT][80] = {
	"STR", "DEX", "CON", "INT", "MET", "CHR",
};

const char stat_long_names[MAX_STAT][80] = {
	"Strength", "Dexterity", "Constitution", "Intelligence",
	"Metablosim", "Charisma"
};

const char stat_descriptions[MAX_STAT][1024] = {
	/* STR */ "",
	/* DEX */ "",
	/* CON */ "",
	/* INT */ "Improves magic ability,\nmemory and wits.",
	/* MET */ "",
	/* CHR */ "",
};

const char state_names[4][80] = {
	"Free Man", "Devotee", "Vector", "Rune Lord"
};

Uint8 scent_human[LEVEL_H][LEVEL_W];

Uint8 fog[LEVEL_H][LEVEL_W] = { { 1 } };	/* could be condensed to 1-bpp */

int  flag_grid_i[LEVEL_H][LEVEL_W];
int  unit_grid_i[LEVEL_H][LEVEL_W];
int house_grid_i[LEVEL_H][LEVEL_W];

unit_t*  unit_grid[LEVEL_H][LEVEL_W];
house_t* house_grid[LEVEL_H][LEVEL_W];


/* Logging */
log_t gamelog;

void log_reset(log_t *log) {

	memset(log, 0, sizeof(log_t));

}

void log_add(log_t *log, const char *buf) {

	int i;

	//TODO: make a circular buffer
	if (log->num >= LOG_MESSAGE_MAX - 1) {
		for (i = 1; i < log->num; i++) {
			memcpy(log->message[i - 1], log->message[i], 256);
		}
		log->num--;
	}

	i = log->num;

	strncpy(log->message[i], buf, sizeof(log->message[i]));

	log->num++;
	//log->tail++;

}

void log_addf(log_t *log, char *fmt, ...) {
	char buffer[1024];

	va_list argptr;
	va_start(argptr, fmt);

	vsnprintf(buffer, sizeof(buffer), fmt, argptr);

	va_end(argptr);

	log_add(log, buffer);

	return;
}
