#ifndef _RUNE_GAME_H
#define _RUNE_GAME_H

#include "rune.h"
#include "runeconf.h"

#include "ai.h"
/* Static data */
extern const char housetabs_names[MAX_HOUSETABS][80];
extern const char unittabs_names[MAX_UNITTABS][80];
extern const char desire_names[MAX_DESIRES][80];
extern const char start_names[MAX_STRATEGIES][80];
extern const char tact_names[MAX_TACTICS][80];
extern const char modus_names[MAX_MODUS][80];
extern const char stat_names[MAX_STAT][80];
extern const char stat_long_names[MAX_STAT][80];
extern const char stat_descriptions[MAX_STAT][1024];

extern const char state_names[4][80];

/* Level data */
/* TODO: put it into a struct */
#define LEVEL_W	32
#define LEVEL_H	32

extern int level_w;
extern int level_h;

extern int turns;

extern unit_t units[MAX_UNITS];
extern house_t houses[MAX_HOUSES];
extern int num_houses;
extern int num_units;

extern pool_t pools[MAX_POOLS];
extern int num_pools;

extern faction_t factions[MAX_FACTIONS];
extern faction_t* your;

extern int  flag_grid_i[LEVEL_H][LEVEL_W];
extern int  unit_grid_i[LEVEL_H][LEVEL_W];
extern int house_grid_i[LEVEL_H][LEVEL_W];

extern unit_t*  unit_grid[LEVEL_H][LEVEL_W];
extern house_t* house_grid[LEVEL_H][LEVEL_W];

extern Uint8 scent_human[LEVEL_H][LEVEL_W];
extern Uint8 fog[LEVEL_H][LEVEL_W];

/* Map checks */
extern int find_flagT(Uint8 tx, Uint8 ty);
extern int find_houseT(Uint8 tx, Uint8 ty);
extern int find_unitT(Uint8 tx, Uint8 ty);
extern int xcollide(Uint8 tx, Uint8 ty, Uint8 w, Uint8 h);

/* Logging */
#define LOG_MESSAGE_MAX 16

typedef struct log_t {

	char message[LOG_MESSAGE_MAX][256];
	Uint32 age[LOG_MESSAGE_MAX];
	Uint32 color[LOG_MESSAGE_MAX];

	int num;
	int head;
	int tail;

} log_t;

extern log_t gamelog;

extern void log_reset(log_t *log);
extern void log_add(log_t *log, const char *buf);
extern void log_addf(log_t *log, char *fmt, ...);

#endif
