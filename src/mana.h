#ifndef _RUNE_MANA_H
#define _RUNE_MANA_H

#include <SDL.h>
#include "rune.h"

extern void add_link(int id1, int id2, Uint8 stat);

extern void rebuild_pools();

extern void update_stats(int reset);

extern void collect_pools();
extern void distrib_pools();


#endif