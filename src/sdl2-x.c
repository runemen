/* Definitive version of sdl-x. Please symlink here OR DIE */
#include <SDL.h>

#ifdef HAVE_SCALE2X
#include "scale2x.h"
#endif

#ifdef HAVE_SDLMIXER
#include <SDL/SDL_mixer.h>
Mix_Chunk *sounds[255];
int loaded_wavs = 0;
//int audio_open = 0;
#endif

#define SDL_CloneSurfaceX(SURFACE, SIZE) SDL_CreateRGBSurface(SURFACE->flags, SURFACE->w * SIZE, SURFACE->h * SIZE, SURFACE->format->BitsPerPixel, \
		SURFACE->format->Rmask, SURFACE->format->Gmask,	SURFACE->format->Bmask,	SURFACE->format->Amask) 
#define SDL_CloneSurfaceR(SURFACE) SDL_CreateRGBSurface(SURFACE->flags, SURFACE->h, SURFACE->w, SURFACE->format->BitsPerPixel, \
		SURFACE->format->Rmask, SURFACE->format->Gmask,	SURFACE->format->Bmask,	SURFACE->format->Amask) 

Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

/*
 * Rotate surface 90 degrees (returns NEW surface)
 */
SDL_Surface* rot90_surface(SDL_Surface* surface)
{
	SDL_Surface* new_surface = NULL;
	Uint8 bpp = surface->format->BytesPerPixel;
	Uint8 *source, *dest, p;
	Uint16 x, y;

	new_surface = SDL_CloneSurfaceR(surface);
//	SDL_SetPalette(new_surface, SDL_LOGPAL | SDL_PHYSPAL,	surface->format->palette->colors, 0,	surface->format->palette->ncolors);	

	if (new_surface == NULL) {
		fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
		return NULL;
	}

	source = (Uint8*)(surface->pixels);
	dest = (Uint8*)(new_surface->pixels);

	for(y = 0; y < surface->h; y++)
	for(x = 0; x < surface->w; x++)
	{
	int dx = new_surface->h - y - 1;
	int dy = x;
		for (p = 0; p < bpp; p++)
		{
			dest[ dy * (new_surface->w * bpp) + (dx * bpp) + p ] = 
			source[ y * (surface->w * bpp) + (x * bpp) + p ];
		}
	}
	return new_surface;
}

/*
 * Flip surface horizontally (returns NEW surface)
 */
SDL_Surface* flip_surface(SDL_Surface* surface)
{
	SDL_Surface* new_surface = NULL;
	Uint8 bpp = surface->format->BytesPerPixel;
	Uint8 *source, *dest, p;
	Uint16 x, y;	

	new_surface = SDL_CloneSurfaceX(surface, 1);
//	SDL_SetPalette(new_surface, SDL_LOGPAL | SDL_PHYSPAL,	surface->format->palette->colors, 0,	surface->format->palette->ncolors);	

	if (new_surface == NULL) {
		fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
		return NULL;
	}

	source = (Uint8*)(surface->pixels);
	dest = (Uint8*)(new_surface->pixels);

	for(y = 0; y < new_surface->h; y++)
	for(x = 0; x < new_surface->w; x++)
		for (p = 0; p < bpp; p++)	
		{
			dest[ y * (new_surface->w * bpp) + (x * bpp) + p ] = 
			source[ (y + 1) * (new_surface->w * bpp) - ((x+1) * bpp) + p ];
		}

	return new_surface;
}

/*
 * Double surface in size (dumb)
 */
void sizex(SDL_Surface *surface, SDL_Surface *new_surface, Uint8 size) 
{
	Uint32 x, y;
	Uint8 bpp = surface->format->BytesPerPixel, cx, cy, p;
	Uint8 *source, *dest;

	source = (Uint8*)(surface->pixels);
	dest = (Uint8*)(new_surface->pixels);

	for(y = 0; y < surface->h; y++)
	for(x = 0; x < surface->w; x++)
		for (p = 0; p < bpp; p++)
			for (cy = 0; cy < size; cy++)
			for (cx = 0; cx < size; cx++)
			{
				dest[ (y * size + cy) * (new_surface->w * bpp) + ( (x * size + cx) * bpp) + p ] = 
				source[ y * (surface->w * bpp) + (x * bpp) + p ];
			}
}

SDL_Surface* sizex_surface(SDL_Surface* surface, Uint8 size)
{
	SDL_Surface* new_surface = NULL;

	new_surface = SDL_CloneSurfaceX(surface, size);
//	SDL_SetPalette(new_surface, SDL_LOGPAL | SDL_PHYSPAL,	surface->format->palette->colors, 0,	surface->format->palette->ncolors);

	sizex(surface, new_surface, size);

	return new_surface;
}

/* 
 * Prepare matching surface and perform AdvancedMAME's scale2x implementation
 */
#ifdef HAVE_SCALE2X
SDL_Surface* scale2x_surface(SDL_Surface* surface)
{
	SDL_Surface* new_surface = NULL;

	new_surface = SDL_CloneSurfaceX(surface, 2);
	SDL_SetPalette(new_surface, SDL_LOGPAL | SDL_PHYSPAL,	surface->format->palette->colors, 0,	surface->format->palette->ncolors);

	scale2x(surface, new_surface);

	return new_surface;
}
#endif



#ifndef HAVE_SDLMIXER
	void init_sound() { }
	int load_sound(const char * buf) { return -1; }
	void play_sound(int id) { }
	void close_sound() { }
#else
/*
 * Init sound
 */
void init_sound() {
	/* Desired audio parameters */
	int audio_rate;
	Uint16 audio_format;
	int audio_channels;
	int loops = 2;
	int i;

	/* Initialize variables */
	audio_rate = 44100;//MIX_DEFAULT_FREQUENCY;
	audio_format = MIX_DEFAULT_FORMAT;
	audio_channels = 2;

	/* Open the audio device */
	if (Mix_OpenAudio(audio_rate, audio_format, audio_channels, 4096) < 0) {
		fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
	} else {
		Mix_QuerySpec(&audio_rate, &audio_format, &audio_channels);
		printf("Opened audio at %d Hz %d bit %s", audio_rate,
			(audio_format&0xFF),
			(audio_channels > 2) ? "surround" :
			(audio_channels > 1) ? "stereo" : "mono");
		if ( loops ) {
		  printf(" (looping)\n");
		} else {
		  putchar('\n');
		}
	}
	//audio_open = 1;

	// allocate 16 mixing channels
//Mix_AllocateChannels(16);
//Mix_ChannelFinished(channel_complete_callback);
}

/*
 * Load a sound
 */
void assign_sound(int id, Mix_Chunk *wave) {
	sounds[id] = wave;
}
int load_sound(const char * buf) {
	/* Load the requested wave file */
	Mix_Chunk *wave = Mix_LoadWAV(buf);
	if ( wave == NULL ) {
		fprintf(stderr, "Couldn't load %s: %s\n", buf, SDL_GetError());
		return -1;
	}
	sounds[loaded_wavs++] = wave;
	printf("Sound %d = %s\n", loaded_wavs-1, buf);
	return (loaded_wavs-1);
}

/*
 * Make a sound
 */
void play_sound(int id) {
	Mix_Chunk *wav = sounds[id];

	/* Start playing */
	Mix_PlayChannel(-1, wav, 0);
}

/*
 * Close sound
 */
void close_sound() {

}
#endif

/**
 ** Lazy Helpers
 **/
SDL_Rect* lazy_rect(Uint32 x, Uint32 y, Uint32 w, Uint32 h) {
	static SDL_Rect rects[16];
	static Uint8 index = 0;

	if (index++ > 16) index = 0;

	rects[index].x = x;
	rects[index].y = y;
	rects[index].w = w;
	rects[index].h = h;

	return &rects[index];
}
Uint32 lazy_timer(Uint8 id) {
	static Uint32 times[16] = /* Make sure 16 zeros are here :( */ 
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	Uint32 passed, microsec;

	microsec = SDL_GetTicks();
	passed = (!times[id] ? 0 : microsec - times[id]);
	times[id] = microsec;

	return passed;
}
Uint32 lazy_delay(Uint8 id, Uint32 test) {
	static Uint32 times[16] = /* Make sure 16 zeros are here :( */ 
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	Uint32 passed, microsec;

	microsec = SDL_GetTicks();
	if (!times[id]) times[id] = microsec;
	passed = microsec - times[id];
	if (passed >= test) {
	    times[id] = microsec;
		passed = 0;
	}
	return passed;
}

/**
 ** HSV tools
 **/
/* Convert RGB  to HSV (RGBSV are in 0-255 range, H might be not... :( ) */
void setHSV(Uint8 r, Uint8 g, Uint8 b, Uint8 *h, Uint8 *s, Uint8 *v) 
{
	// RGB are from 0..1, H is from 0..360, SV from 0..1

	Uint8 maxC = b;		if (maxC < g) maxC = g;		if (maxC < r) maxC = r;
	Uint8 minC = b;		if (minC > g) minC = g;		if (minC > r) minC = r;

	double R = (double)r / 255;
	double G = (double)g / 255;
	double B = (double)b / 255;

	double M = (double)maxC / 255;
	double m = (double)minC / 255;
	double C = M - m;

	double V = M;
	double S = 0;
	double H = 0;

	if (C == 0)
	{
		H = 0;
		S = 0;
	}
	else
	{
		double dR = 60 * (M - R) / C + 180;
		double dG = 60 * (M - G) / C + 180;
		double dB = 60 * (M - B) / C + 180;
		if (r == maxC)			H = dB - dG;
		else if (g == maxC)		H = 120 + dR - dB;
		else				H = 240 + dG - dR;
		if (H < 0)	H += 360;
		if (H >= 360)	H -= 360;
		S = C / M;
	}
	*h = H;
	*s = S * 255;
	*v = V * 255;
}
/* Convert HSV to RGB */
void setRGB(Uint8 h, Uint8 s, Uint8 v, Uint8 *r, Uint8 *g, Uint8 *b)
{
	double p, q, t;
	Uint8 f;

	if (s == 0)	
	{
		*r = *g = *b = v;
		return;
	}
	f = ((h % 60) * 255) / 60;
	h /= 60;
	p = (v * (256 - s)) / 256;
	q = (v * (256 - (s * f) / 256)) / 256;
	t = (v * (256 - (s * (256 - f)) / 256)) / 256;
	switch( h ) 
	{
		case 0:		*r = v;		*g = t;		*b = p;	break;
		case 1:		*r = q;		*g = v;		*b = p;	break;
		case 2:		*r = p;		*g = v;		*b = t;	break;
		case 3:		*r = p;		*g = q;		*b = v;	break;
		case 4:		*r = t;		*g = p;		*b = v;	break;
		default:	*r = v;		*g = p;		*b = q;	break;
	}
}

typedef struct SDL_Shade {
	Uint8 h;
	Uint8 s;
	Uint8 v;
	Uint8 a;
} SDL_Shade;

void SDL_ComputeGradient(SDL_Color *colors, int n, SDL_Color *start, SDL_Color *stop) {

	SDL_Shade _start;
	SDL_Shade _stop;

	setHSV(start->r, start->g, start->b, &_start.h, &_start.s, &_start.v); 
	setHSV(stop->r, stop->g, stop->b, &_stop.h, &_stop.s, &_stop.v);

	int i = 0;
	double f = 0;
	double step = 1.0f / n;
	//printf("STEP: %f\n", step);
	for (i = 0; i < n; i++) {
		int r, g, b;
		//linear: pu = p0 + u(p1 - p0)
		/*
		double H = (double)_start.h + f * (_stop.h - _start.h);
		double S = (double)_start.s + f * (_stop.s - _start.s);
		double V = (double)_start.v + f * (_stop.v - _start.v);
		setRGB((int)H, (int)S, (int)V , &r, &g, &b);
		*/

		double R = (double)start->r + f * (stop->r - start->r);
		double G = (double)start->g + f * (stop->g - start->g);
		double B = (double)start->b + f * (stop->b - start->b);
		r = (int)R; g = (int)G; b = (int)B;

//printf("COLOR %d - %02x %02x %02x \n", i, r,g,b);
		colors->r = r;
		colors->g = g;
		colors->b = b;
		colors->a = 0xff;
		colors++;
		f += step;
	}
}
