#include "rune.h"
#include "mana.h"
#include "game.h"
#include "ai.h"

#include "libs/binaryheap/binhl.h"

/* Define this to get lots of _printf's */
//#define DEBUG_UNIT_AI

#ifdef DEBUG_UNIT_AI
#define _printf(...) fprintf (stdout, __VA_ARGS__)
#else
#define _printf(...) { }
#endif


int map_cost(unit_t *u, int x, int y, int dx, int dy);

#define GRAPHSIZE (LEVEL_W * LEVEL_H)
#define DJ_INFINITY (GRAPHSIZE * GRAPHSIZE)
#define LEVEL_POS(X,Y) ((Y) * LEVEL_W + (X)) 

typedef int levelpos_t;

int get_closest(const long *cost, levelpos_t src, int *xx, int *xy) {
	int i, j;

	long min_cost = DJ_INFINITY;
	int min_pos = -1;

	long test_cost;
	int test_pos = -1;

	int sy = src / level_w;
	int sx = src % level_w;
	
	for (j = -1; j < 2; j++) {
		if (sy + j < 0 || sy + j >= level_h - 1) continue;
		for (i = -1; i < 2; i++) {
			if (i == 0 && j == 0) continue;
			if (sx + i < 0 || sx + i >= level_w - 1) continue;

			test_pos = LEVEL_POS(sx + i, sy + j);
			test_cost = cost[test_pos] + rand() % 10;
			if (test_cost < min_cost) {
				min_cost = test_cost;
				min_pos = test_pos;
			}

		}
	}

	if (min_pos != -1) {
		*xy = min_pos / level_w;
		*xx = min_pos % level_w;
	}

	return min_pos;
}

int build_path(const int *prev, int *next, int n, levelpos_t src, levelpos_t dest, int *nn) {
	int i = 0;
	while (1) {
		i++;
		if (dest == -1) return -1;
		next[n - i] = dest;
		if (prev[dest] == src) {
			break;
		}
		dest = prev[dest];
	}
	*nn = i;
	return n - i;
}

int get_path(const int *prev, levelpos_t src, levelpos_t dest, int *xx, int *xy) {
#if 0
	if (dest == -1) return -1;
	if (prev[dest] == src)
	{
		int y = dest / level_w;
		int x = dest - (y * level_w);
		*xx = x;
		*xy = y;
		return 0;
	}
	return get_path(prev, src, prev[dest], xx, xy);
#else
	while (1) {
		if (dest == -1) return -1;
		if (prev[dest] == src) {
			break;
		}
		dest = prev[dest];
	}
	*xy = (dest / level_w);
	*xx = (dest % level_w);
	return 0;
#endif
}

#ifdef ALLOW_DIAGONALS
	#define dir_turn_max 8
#else
	#define dir_turn_max 4
#endif
static char dir_turn_x[9] = { -1, 0, 1, 0, -1, 1, 1,-1, 0 };
static char dir_turn_y[9] = {  0,-1, 0, 1, -1, 1,-1, 1, 0 };
#if 0
static int offset_turn[9] = { 0 };
#endif
void dijkstra_fast(unit_t *u, long *d, int *prev, levelpos_t s, int pitch, int lines) {

	binh_list open_list = { 0 };

	int intops=0;

	int i, n, mini;
	int x, y, dx, dy;	

	long dist;

	int visited[GRAPHSIZE];
	int max_nodes = (pitch * lines);
	long infinity = max_nodes * max_nodes;
	
	int max_in = 0;
	int revis = 0;
	
	if (max_nodes > GRAPHSIZE) return;
//printf("Max nodes:%d\n", max_nodes);
#if 0
	/* Pre-calculate offsets */
	for (i = 0; i < dir_turn_max; i++) {
		offset_turn[i] = dir_turn_y[i] * lines + dir_turn_x[i];
	}
#endif
	for (i = 0; i < max_nodes; i++) {
		d[i] = infinity;
		prev[i] = -1; /* no path has yet been found to i */
		visited[i] = 0; /* the i-th element has not yet been visited */
	}
	d[s] = 0;

	/* Put ALL the nodes into open list */	
	//for (i = 0; i < max_nodes; i++) {
		//binhl_push(&open_list, i, d[i]);
	//}
	
	binhl_push(&open_list, s, 0);

	while (open_list.len) {

		mini = binhl_pop(&open_list);

		/* Revisit protection. Revisits happen very rarely 
		 * and can be remove completeley with some care.
		 * This check itself is pretty costly */		
		if (visited[mini]) { revis ++; continue; }
		visited[mini] ++;	

		/* We shall devise a better method of node search
		 * to remove this bit of math: */
		y = mini / pitch;		/* Get coords from id */ 
		x = mini % pitch;		/* Yuck! */

		for (n = 0; n < dir_turn_max; n++) {
#if 1
			/* All for the sake of this out of bounds searcher */
			if ((dy = y + dir_turn_y[n]) < 0 || 
				(dx = x + dir_turn_x[n]) < 0 || 
				dx > pitch-1 || dy > lines-1) continue;

			i = dy * pitch + dx; /* Get id from coords! :( */
#else
			i = mini + offset_turn[n];
			if (i < 0 || i >= pitch * lines - 1) continue;
#endif
			intops++;

			/* This function get called only once per dx, dy,
			 * the only overhead is the call itself */
			if ((dist = map_cost(u, x, y, dx, dy))) // or map_cost[mini][i] 
				if (d[mini] + dist < d[i]) {
					d[i] = d[mini] + dist;
					prev[i] = mini;
					//printf("Best path from %d to %d\n", i,mini);
					if (!visited[i]) /* Again, is this check useless ? */
						binhl_push(&open_list, i, d[i]);

                    if (open_list.len > max_in) max_in = open_list.len ; 
				}
		}
	}
	//printf("IN %d cycles, MAX IN LIS: %d, REVISITS: %d\n", intops, max_in, revis);
}


int find_flag(long *cost, faction_t *faction, int type, long *min, int *id) {
	int i, j;

	int test_pos;
	long test_cost;

	int pos = -1;
	long min_cost = DJ_INFINITY;
	int mini = -1;

	for (i = 0; i < faction->num_flags; i++) {
		flag_t *f = &faction->flags[i];

		if (f->type == type) {
			test_pos = LEVEL_POS(f->x, f->y);
			test_cost = cost[test_pos] / 2;
			if (test_cost < min_cost) {
				min_cost = test_cost;
				mini = i;
			}
		}
	}
	if (mini != -1) {
	    /* Pick closest corner */
		flag_t *f = &faction->flags[mini];
		pos = LEVEL_POS(f->x, f->y);

		for (j = 0; j < f->h; j++) {
			//if (j >= 0 && j < h->h) continue;
			for (i = 0; i < f->w; i++) {
				//if (i >= 0 && i < h->w) continue;
				test_pos = LEVEL_POS(f->x + i, f->y + j);
				test_cost = cost[test_pos];
				if (test_cost < min_cost) {
					min_cost = test_cost;
					pos = test_pos;
				}
			}
		}
	}
	*min = min_cost;
	*id = mini;
	return pos;
}

int find_entity(long *cost, int type, long *min, int s, int *id) {
	int pos = -1;
	int min_cost = DJ_INFINITY;
	int mini = -1;
	int i;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		unit_p *p = &bunits[u->tile];
		if (u->visiting) continue;
		if (u->mode == ANIM_DEATH) continue;
		if (u->faction == type) {
			int hp = LEVEL_POS(u->x, u->y - (p->h-1));
			long c = cost[hp];
			if (c < min_cost) {
				min_cost = c;
				mini = i;
			}
		}
	}
	if (mini != -1) {
	    /* Pick closest corner */
		unit_t *u = &units[mini];
		unit_p *p = &bunits[u->tile];
		pos = LEVEL_POS(u->x, u->y - (p->h-1));
		_printf("Hunting for unit %d\n", mini);
		int j;
		for (j = 0; j < p->h; j++) {
			//if (j >= 0 && j < h->h) continue;
			for (i = 0; i < p->w; i++) {
				//if (i >= 0 && i < h->w) continue;
				int hp = LEVEL_POS(u->x + i, u->y - (p->h-1) + j);
				long c = cost[hp];
				if (c < min_cost) {
					min_cost = c;
					pos = hp;
						//if (pos == s) return -1;
				}
			}
		}
	}
	*min = min_cost;
	*id = mini;
	//printf("Found : %d, min_cost: %d\n", pos, min_cost);
	return pos;
}

int find_place(long *cost, int type, long *min, int s, int *id) {
	int pos = -1;
	int min_cost = DJ_INFINITY;
	int mini = -1;
	int i;
	for (i = 0; i < num_houses; i++) {
		house_t *h = &houses[i];
		if (h->tile == type || h->built == 0) {
			int hp = LEVEL_POS(h->x, h->y);
			long c = cost[hp];
			if (c < min_cost) {
				min_cost = c;
				mini = i;
			}
		}
	}
	if (mini != -1) {
	    /* Pick closest corner */
		house_t *h = &houses[mini];
		pos = LEVEL_POS(h->x, h->y);
		int j;
		for (j = 0; j < h->h; j++) {
			//if (j >= 0 && j < h->h) continue;
			for (i = 0; i < h->w; i++) {
				//if (i >= 0 && i < h->w) continue;
				int hp = LEVEL_POS(h->x + i, h->y + j);
				long c = cost[hp];
				if (c < min_cost) {
					min_cost = c;
					pos = hp;
						//if (pos == s) return -1;
				}
			}
		}
	}
	*min = min_cost;
	*id = mini;
	//printf("Found : %d, min_cost: %d\n", pos, min_cost);
	return pos;
}

void unit_pick_destination(int i) {
	unit_t *u = &units[i];

	long d[GRAPHSIZE]; /* d[i] is the length of the shortest path between the source (s) and node i */
	int prev[GRAPHSIZE]; /* prev[i] is the node that comes right before i in the shortest path from the source to i*/
	int next[GRAPHSIZE];

	int unit_pos = LEVEL_POS(u->x, u->y);
	int target_pos;
	int target_x, target_y;

#ifdef DEBUG_PATHFIND
#define THE_D u->d
#define THE_PREV u->prev
#else
#define THE_D d
#define THE_PREV prev
#endif

	/* Now calculate all path costs */
	dijkstra_fast(u, THE_D, THE_PREV, unit_pos, level_w, level_h);

	int id = -1;

	long target_cost = 0;

	target_pos = -1; /* for safety */

	if (u->strategy == STRAT_WORK) {

		int htype = 2;
		if (u->carry_gold >= 100) htype = 7;

		target_pos = find_place(THE_D, htype, &target_cost, unit_pos, &id);
		
		if (target_pos != -1) {

			u->target_type = TARGET_HOUSE;
			u->target_id = id; 

		}
	}

	if (u->strategy == STRAT_HUNT) {

		target_pos = find_flag(THE_D, your, 0, &target_cost, &id);

		if (target_pos != -1) {

			u->target_type = TARGET_FLAG;
			u->target_id = id;

		}

		if (target_pos == -1) {
		
			target_pos = find_entity(THE_D, 1 - u->faction, &target_cost, unit_pos, &id);

			if (target_pos != -1) {
	
				u->target_type = TARGET_UNIT;
				u->target_id = id; 
	
			}

		}

	}

	//target_pos = get_closest(THE_D, unit_pos, &target_x, &target_y);

	//printf("Picked: %d, %d\n", target_x, target_y);

	if (target_pos == -1) return;

#ifdef DEBUG_PATHFIND
	u->tar_y = target_pos / level_w;
	u->tar_x = target_pos % level_w; 
#endif

	/* Build path */
	int len;
	int start = build_path(THE_PREV, next, GRAPHSIZE, unit_pos, target_pos, &len);
	if (start > -1) {
		int i;
		int maxi = 16;
		if (len < maxi) maxi = len; 
		for (i = 0; i < maxi; i++) {
			u->path[i] = next[start + i];
		}
		for (; i < 16; i++) {
			u->path[i] = -1;
		}
	
	}
return;

	/* Select a target */
	if (!get_path(THE_PREV, unit_pos, target_pos, &target_x, &target_y))
	{
		u->tx = target_x;
		u->ty = target_y;
	} else _printf("%s: No path to target\n", u->name);
}

void unit_pick_destination2(int i) {
	unit_t *u = &units[i];

	if (u->refresh.path || u->path[0] == -1) {

		u->refresh.path = 1;
		unit_pick_destination(i);
		u->refresh.path = 0;

	}

	if (u->path[0] != -1) {
		int i;
		int target_pos = u->path[0];
		for (i = 0; i < 16 - 1; i++) {
			u->path[i] = u->path[i + 1];
		}
		u->path[15] = -1;

		int target_y = target_pos / level_w;
		int target_x = target_pos % level_w;
	
		u->tx = target_x;
		u->ty = target_y;
	}

}

void unit_pick_strategy(int i) {
    unit_t *u = &units[i];
    
    if (u->top_desire == DESIRE_NONE) {
		_printf("%s has no desires.. Picking one at random.\n", u->name);    
		u->top_desire = rand() % MAX_DESIRES;
    }
    if (u->top_method == DESIRE_NONE) {
    	_printf("%s has no method to achive his desire.. Picking one at random.\n", u->name);
		u->top_method = rand() % MAX_DESIRES;
    }
	if (u->strategy == STRAT_NONE) {
		_printf("%s has no strategy.\n", u->name); 
		/* Let's pick one... */
		switch (u->top_method) {
			case DESIRE_GOLD:
				/* Easiest way to get gold is to work for your kingdom */
				u->strategy = STRAT_WORK;
			break;
			case DESIRE_LOVE:

			break;
			case DESIRE_POWER:

			break;
			default:
			break;
		}
	}
	if (u->tactic == TACT_NONE) {
		//_printf("%s has no tactic.\n", u->name); 
	}
    _printf("%s wants %s, his plan is to use %s\n", u->name, desire_names[u->top_desire],desire_names[u->top_method]);
    _printf("  - Strategy: %s, Tactic: %s\n", start_names[u->strategy], tact_names[u->tactic]); 
}

int map_cost(unit_t *u, int x, int y, int dx, int dy) {

	int h_ind, u_ind, f_ind;
	int manh = abs(u->x - dx) + abs(u->y - dy); 
	//if (manh > 0 && manh <= 2 && find_unitT(dx, dy) != -1) return DJ_INFINITY;

	int tile_cost = 0;

	tile_cost = 0;
	if (dx - x != 0 && dy - y != 0) tile_cost = 15; //diagonal movements should be more expensive

	if ((u_ind = find_unitT(dx, dy)) != -1) {
		unit_t *u2 = &units[u_ind];

		/* Itself... */
		if (u2 == u) {
			/* probably doesn't matter */
		}
		/* Ally */
		else if (u2->faction == u->faction) {

			if (u->strategy == STRAT_HEAL) {

				/* Damaged unit */
				if (u2->dmg) return 10;

			}	

		}
		/* Foe */
		else {

			if (u->strategy == STRAT_HUNT) {

				return 10;

			}	

		}

		/* For all other cases, treat it as solid tile (to move around) IF it's close */
		if (manh > 0 && manh <= 2 && u2 != u) return DJ_INFINITY;
	}

	if ((f_ind = find_flagT(dx, dy)) != -1) {
		flag_t *f = &your->flags[f_ind];

			if (u->strategy == STRAT_WORK) {
				/* Go over flag, but don't obsess over it */
				if (f->type == 0) return 20;
			}
			if (u->strategy == STRAT_HUNT) {
				/* Both flags are good */
				if (f->type == 0) return 20;
			}
	}

	if ((h_ind = find_houseT(dx, dy)) != -1) {
		house_t *h = &houses[h_ind];
			if (u->strategy == STRAT_WORK) {
				/* Build/Repair building */
				if (h->hp < h->max_hp) return 10;
				/* Mine gold */
				if (h->tile == 2 && u->carry_gold < 100) return 10;
				/* Unload gold */
				if (h->tile == 7 && u->carry_gold >= 100) return 10;
			}
		return DJ_INFINITY/2;// && dx != u->tx && dy != u->ty) return 500;
	}

	return 30 + tile_cost;
}

void unit_think(int i) {
	unit_t *u = &units[i];
	unit_p *p = &bunits[u->tile];

	unit_pick_strategy(i);

	/* Tmp Hack -- override strategy */ 
	switch (p->modus) {
		case MODUS_NONE: u->strategy = STRAT_NONE; break;
		case MODUS_WORKER: u->strategy = STRAT_WORK; break;
		case MODUS_SOLDIER: u->strategy = STRAT_HUNT; break;
		case MODUS_HUNTER: u->strategy = STRAT_HUNT; break;
		case MODUS_HEALER: u->strategy = STRAT_HEAL; break;
		case MODUS_CARRIER: u->strategy = STRAT_NONE; break;
		case MODUS_FOLLOWER: u->strategy = STRAT_NONE; break;
		case MODUS_BUFFER: u->strategy = STRAT_HEAL; break;
		case MODUS_DEBUFFER: u->strategy = STRAT_HEAL; break;
		case MODUS_EXPLORER: u->strategy = STRAT_HUNT; break;
	}

	if ((u->ox == 0 && u->oy == 0 && u->x == u->tx && u->y == u->ty) || u->refresh.path)
		unit_pick_destination2(i);
}