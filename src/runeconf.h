#ifndef _RUNE_CONF_H
#define _RUNE_CONF_H

#include <SDL.h>

#define MAX_BUNITS 128
#define MAX_BHOUSES 64

extern unit_p bunits[MAX_BUNITS];
extern house_p bhouses[MAX_BHOUSES];

extern Uint16 num_bunits;
extern Uint16 num_bhouses;


extern int cfg_load(const char *filename);

#endif