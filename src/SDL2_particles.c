#include "SDL2_particles.h"

void basic_particle_init(particle_t *p)
{
	p->x = 0;
	p->y = 0;
}

void basic_particle_move(particle_t *p)
{
	float x = p->x;
	float y = p->y;
	//Sint32 ttl = p->ttl--;
	//Uint8 size = p->size;
	float spd = p->spd;
	float a = p->ang;
	float c = cosf(a * M_PI / 180);  
	float s = sinf(a * M_PI / 180);
	float nx = x + spd * c;
	float ny = y + spd * s;
	p->ttl--;
	p->x = nx;
	p->y = ny;
	x = nx;
	y = ny;
}
void basic_particle_reset(particle_t *p)
{
#if 0
		p->x = rand() % 120;
		p->y = rand() % 120;
		p->ttl = 20 + rand() % 100;
		p->spd = 1;
		p->ang = 45 + rand() % 90 ;
		p->size = 2;
#else
		p->x = 0;
		p->y = 0;
		p->ttl = rand() % 80 ;//+ rand() % 20;
		p->spd = 1;
		p->ang = rand() % 30 - 90 - 15;
		p->size = 2;
#endif			
}

void PS_Update(particle_system *ps) {
	int i;
	int done = 0;
	for (i = 0; i < PARTICLES_PER_SYSTEM; i++) {
		ps->move(&ps->particle[i]);
		if (ps->particle[i].ttl <= 0) {
			if (ps->spawn < ps->limit) {
				ps->reset(&ps->particle[i]);
				ps->spawn++;
			} else done++;
		}
	}
	if (done >= PARTICLES_PER_SYSTEM) {
		ps->limit = 0;
	}
}

void PS_Render(SDL_Renderer *target, particle_system *ps, SDL_Color *colors, int num_colors, Uint32 bx, Uint32 by) {

	int i, j;
	int colorquant = PARTICLES_PER_SYSTEM / num_colors;
	for (i = 0; i < PARTICLES_PER_SYSTEM; i++) {
		Uint32 x = ps->particle[i].x;
		Uint32 y = ps->particle[i].y;
		Sint32 ttl = ps->particle[i].ttl--;
		//Uint8 size = ps->particle[i].size;
		if (ttl <= 0) continue;
		j = i % colorquant;
		//Uint32 color = SDL_MapRGB(surface->format, reds[j].r, reds[j].g, reds[j].b);
		//-//SDL_AlphaFill(surface, bx + x, by + y, size, size, color);
		//-SDL_PutPixel(surface, bx + x, by + y, color+ttl);
        SDL_SetRenderDrawColor(target, colors[j].r, colors[j].g, colors[j].b, 0);
		SDL_RenderDrawPoint(target, bx + x, by + y);
	}
}
