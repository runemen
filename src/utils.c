#include <SDL.h>

#include "rune.h"
#include "ui.h"

/* Try to find a window size closest to VGA (640x480) */ 
SDL_DisplayMode* getWindowSize(SDL_DisplayMode *desired) {
	int display = 0;

	SDL_DisplayMode desktop;
	static SDL_DisplayMode ret;

	if (SDL_GetDesktopDisplayMode(display, &desktop)) {
		fprintf(stderr, "Unable to get desktop display mode: %s\n", SDL_GetError());
		return NULL;
	}

	/* Desktop is weirdly small, use the whole of it... */
	if (desktop.w < desired->w || desktop.h < desired->h) {

		ret.w = desktop.w;
		ret.h = desktop.h;

	}
	else {

		/* Enlarge until we can't fit */
		Uint32 w = desired->w;
		Uint32 h = desired->h;

		do {
			ret.w = w;
			ret.h = h;
			w *= 2;
			h *= 2;
		} while (w < desktop.w && h < desktop.h);
	}

	return &ret;
}

/* Try to find fullscreen mode closest to VGA (640x480) */
SDL_DisplayMode* getFullscreenSize(SDL_DisplayMode *desired) {
	int display = 0;

	static SDL_DisplayMode best;

	int i;
	int best_mode = -1;
	int best_fits = 0;
	int num_modes = SDL_GetNumDisplayModes(display);

	/* Find smallest mode closest to desired */
	for (i = 0; i < num_modes; i++) {

		SDL_DisplayMode mode;
		if (SDL_GetDisplayMode(display, i, &mode)) {
			fprintf(stderr, "Unable to get display mode %d: %s\n", i, SDL_GetError());
			continue;
		}

		//printf("Mode %d -- %d x %d\n", i, mode.w, mode.h);

		/* Correct aspect ration (VGA) */
		if (mode.w % desired->w == 0 && mode.h % desired->h == 0) {

			/* AND it's smaller, pick it */
			if (best_mode == -1 || (mode.w < best.w && mode.h < best.h)) {

				best_mode = i;
				best_fits = 1;
				best.w = mode.w;
				best.h = mode.h;

			} 
		}
		/* WRONG aspect ratio */
		else {
			/* Still going to pick it, if we don't have anything better */
			if (best_mode == -1 || (best_fits == 0 && (mode.w < best.w && mode.h < best.h)) ) {

				best_mode = i;
				best_fits = 0;
				best.w = mode.w;
				best.h = mode.h;

			}
		}
	}

	return &best;
}

void generate_name(unit_t *u) {

	static const char first_names[][80] = {
	"Gaborn", "Jaz", "Talon", "Fallion",
	"Myrrima", "Rhianna", "Averan", "Erin",
	"Jas Laren", "Mendellas", "Roland",  
	"Saffira", "Skalbairn", "Chemoise", "Wuqaz", "Sarka",
	"Raj", "Feykaald", "Jureem", "Ivarian", "Aelgir",
	"Waggit",  "Conall", "Celinor", "Connall", "Spring",
	"Anders", "Zandaros", "Binnesman", "Paldane",
	"Solette", "Poll", "Faharaqin", "Kaul",
	"Ahten", "Sylvarresta", "Borenson", 
	};

	int i = rand() % (sizeof(first_names) / (sizeof(char) * 80));
	sprintf(u->name, "%s", first_names[i]);  
}

int count_fps(void) {

	static Uint32 wait = 0;
	static Uint32 fps = 0;
	static Uint32 fps_counter = 0;

	Uint32 tick = SDL_GetTicks();
	Uint32 pass = tick - wait;

	fps_counter++;

	if (pass >= 1000) {
		fps = fps_counter;
		fps_counter = 0;
		wait = tick;
	}

	return fps;
}

int profile(Uint8 id, const char *desc) {

	static Uint32 wait[256] = { 0 };

	Uint32 tick = SDL_GetTicks();
	Uint32 pass = tick - wait[id];

	if (desc != NULL) {
		printf("Profiler: %s: %d\n", desc, pass);	
	}
	wait[id] = tick;

	return 0;
}
