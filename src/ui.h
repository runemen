#ifndef _RUNE_UI_H
#define _RUNE_UI_H

#include <SDL.h>

#define PANE_WIDTH (128+10)

#define MINIMAP_PADDING 1

#define BOX_PADDING 4

#define SDL_InBounds(X, Y, RECT) \
	((X) >= (RECT)->x && (X) <= (RECT)->x + (RECT)->w && \
	 (Y) >= (RECT)->y && (Y) <= (RECT)->y + (RECT)->h)


typedef enum uiHoverMajor {
	overNothingMajor,
	overMap,
	overPane,
} uiHoverMajor; 

typedef enum uiHoverMinor {

	overNothingMinor,

	overUnit,
	overHouse,
	overFlag,

	overMinimap,
	overUnitBox,
	overHouseBox,
	overPinBox,

	overListName,
	overListPin,

	overFlagPlus,

	overUnitTab,
	overUnitPin,
	overUnitFlag,
	overUnitStat,
	overUnitSkill,
	overUnitName,

	overHouseTab,
	overHouseFlag,

	overFlagButton,
	overBuildButton,

} uiHoverMinor;

typedef struct ui_t ui_t;
struct ui_t {
	int x;		/* cursor position */
	int y;

	int vx;		/* viewport offset (scrolling) */
	int vy;

	int flingx;	/* desired viewport offset (for scrolling) */
	int flingy;

	int dragging; /* dragging with touch/middle button */
	int pushing;  /* holding the button/finger down */

	int faction; /* player's faction id */

	int unit;	/* selected unit */
	int house;	/* selected building */
	int flag;	/* selected flag */
	int builder;/* building to build */
	int setflag;/* flag to place */
	int stat;	/* selected stat */
	int btn;	/* ??? */
	int unittab;/* selected unit tab */
	int housetab;/* selected house tab */

    int hintType;
	int hint;

	uiHoverMajor hover_top;
	uiHoverMinor hover;
	int hover_id;
	int hover_xcollide;
	int hover_tx;
	int hover_ty;

	int no_mouse;	/* for touch devices */
	int log_width;	/* can't rely on DEFINES */
	int log_height;

	int freq;	/* just a counter, always ++ */

	/* layers */
	int draw_minimap;
	int draw_uibg;
	int draw_fog;
	int draw_path;
	int draw_pools;
	int draw_overlays;
	int draw_scent;
	int draw_log;
	
	/* debug */
	int game_speed;
	int fps;
};

/* Externals */
extern int find_unit(Uint32 x, Uint32 y);
extern int find_house(Uint32 x, Uint32 y);

/** **/

extern ui_t ui;
extern SDL_Rect minimap;
extern SDL_Rect buildbox;
extern SDL_Rect selbox;
extern SDL_Rect pinbox;
extern SDL_Rect hintbox;

extern SDL_Rect tabicon;
extern SDL_Rect unitpin;
extern SDL_Rect flagicon;
extern SDL_Rect plusbtn;

extern SDL_Rect top_pane;
extern SDL_Rect ui_pane;
extern SDL_Rect game_map;

/** **/

extern void init_ui(SDL_Renderer *renderer, int w, int h);

extern void reset_ui();

extern void track_mouse();
extern void track_mouse_map();
extern void track_mouse_ui();

#endif