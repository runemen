#ifndef _SDL2_PARTICLES_H
#define _SDL2_PARTICLES_H

#include <SDL.h>

#define PARTICLES_PER_SYSTEM 255

typedef struct particle_t {
	float x;
	float y;
	Sint32 ttl;
	float ang;
	Uint32 spd;
	Uint32 size;
} particle_t;

typedef struct particle_system {
	Sint16 x;
	Sint16 y;
	particle_t particle[PARTICLES_PER_SYSTEM];
	Uint16 limit;
	Uint16 spawn;
	void (*init)(particle_t *part);
	void (*move)(particle_t *part);
	void (*reset)(particle_t *part);
} particle_system;

extern void PS_Update(particle_system *ps);
extern void PS_Render(SDL_Renderer *target, particle_system *ps, SDL_Color *colors, int num_colors, Uint32 bx, Uint32 by);

/* default handlers */
extern void basic_particle_init(particle_t *p);
extern void basic_particle_move(particle_t *p);
extern void basic_particle_reset(particle_t *p);

#endif /* _SDL2_PARTICLES_H */