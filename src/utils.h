#ifndef _RUNE_UTILS_H
#define _RUNE_UTILS_H

#include <SDL.h>

#include "rune.h"

extern void generate_name(unit_t *u);

extern int count_fps(void);
extern int profile(Uint8 id, const char *desc);

extern SDL_DisplayMode* getWindowSize(SDL_DisplayMode *desired);
extern SDL_DisplayMode* getFullscreenSize(SDL_DisplayMode *desired);

#endif