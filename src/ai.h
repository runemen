#ifndef _RUNE_AI_H
#define _RUNE_AI_H

#define ALLOW_DIAGONALS

extern void unit_think(int id);

#define MAX_DESIRES	9
#define DESIRE_NONE 	0
#define DESIRE_RUNE 	1
#define DESIRE_GOLD 	2
#define DESIRE_POWER	3
#define DESIRE_LOVE 	4
#define DESIRE_PROTECT	5
#define DESIRE_DESTROY	6
#define DESIRE_KILLALL	7
#define DESIRE_HELPALL	8

#define MAX_STRATEGIES	5
#define STRAT_NONE	0
#define STRAT_WORK	1
#define STRAT_HUNT	2
#define STRAT_KILL	2
#define STRAT_HEAL	3
#define STRAT_HIDE	4

#define MAX_TACTICS	10
#define TACT_NONE	0
#define TACT_MINE	1
#define TACT_BUILD	2
#define TACT_REPAIR	3
#define TACT_TRAIN	4
#define TACT_BUY	5
#define TACT_CHARM	6
#define TACT_ATTACK	7
#define TACT_HIDE	8
#define TACT_RUN	9

#define MAX_MODUS	10
#define MODUS_NONE  	0
#define MODUS_WORKER	1
#define MODUS_HUNTER	2
#define MODUS_SOLDIER	3
#define MODUS_HEALER	4
#define MODUS_CARRIER	5
#define MODUS_FOLLOWER	6
#define MODUS_BUFFER	7
#define MODUS_DEBUFFER	8
#define MODUS_EXPLORER	9

#define TARGET_NONE	 0
#define TARGET_UNIT  1
#define TARGET_HOUSE 2
#define TARGET_FLAG  3
#define TARGET_TILE  4

#endif
