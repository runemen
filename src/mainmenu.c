#include <SDL.h>

#include "libs/lazyass/lazyass.h"
#include "libs/SDL_inprint/SDL2_inprint.h"

#include "draw.h"
#include "rune.h"
#include "ui.h"

SDL_Texture *bg;
SDL_Texture *sf;

#define BUTTON_PADDING 4

#ifndef SDL_InBounds
#define SDL_InBounds(X, Y, RECT) \
	((X) >= (RECT)->x && (X) <= (RECT)->x + (RECT)->w && \
	 (Y) >= (RECT)->y && (Y) <= (RECT)->y + (RECT)->h)
#endif

void draw_scale9x(SDL_Renderer *renderer, SDL_Rect *full, SDL_Rect *center, SDL_Rect *dst, SDL_bool stretch) {

	SDL_Rect top_left = { full->x, full->y, center->x, center->y };
	SDL_Rect bottom_left = { full->x, full->y + center->y + center->h, center->x, full->h - center->y - center->h };
	SDL_Rect top_right = { full->x + center->x + center->w, full->y, full->w - center->x - center->w, center->y };
	SDL_Rect bottom_right = { full->x + center->x + center->w, full->y  + center->y + center->h, full->w - center->x - center->w, full->h - center->y - center->h };

	SDL_Rect top_left_dst = { dst->x, dst->y, top_left.w, top_left.h };
	SDL_Rect bottom_left_dst = { dst->x, dst->y + dst->h - bottom_left.h, bottom_left.w, bottom_left.h };
	SDL_Rect top_right_dst = { dst->x + dst->w - top_right.w, dst->y, top_right.w, top_right.h };
	SDL_Rect bottom_right_dst = { dst->x + dst->w - bottom_right.w, dst->y + dst->h - bottom_right.h, bottom_right.w, bottom_right.h };

	SDL_Rect left = { full->x, full->y + center->y, center->x, center->h };
	SDL_Rect left_dst = { dst->x, dst->y + center->y, left.w, left.h }; 

	SDL_Rect right = { full->x + center->x + center->w, full->y + center->y, center->x, center->h };
	SDL_Rect right_dst = { dst->x + dst->w - right.w, dst->y + center->y, right.w, right.h };
	
	SDL_Rect top = { full->x + center->x, full->y, center->w, center->y };
	SDL_Rect top_dst = { dst->x + center->x, dst->y, top.w, top.h }; 

	SDL_Rect bottom = { full->x + center->x, full->y + center->y + center->h, center->w, center->y };
	SDL_Rect bottom_dst = { dst->x + center->x, dst->y + dst->h - bottom.h, bottom.w, bottom.h }; 

	SDL_Rect center_src = { full->x + center->x, full->y + center->y, center->w, center->h };
	SDL_Rect center_dst = { dst->x + center->x, dst->y + center->y, center->w, center->h };

	if (stretch == SDL_TRUE) {

		top_dst.w = dst->w - center->w;
		bottom_dst.w = dst->w - center->w;

		center_dst.w = dst->w - center->w;
		center_dst.h = dst->h - center->h;
		
		left_dst.h = dst->h - center->h;
		right_dst.h = dst->h - center->h;

		SDL_RenderCopy(renderer, tiles, &center_src, &center_dst);

		SDL_RenderCopy(renderer, tiles, &top, &top_dst);
		SDL_RenderCopy(renderer, tiles, &left, &left_dst);
		SDL_RenderCopy(renderer, tiles, &right, &right_dst);
		SDL_RenderCopy(renderer, tiles, &bottom, &bottom_dst);

	} else {
		
		for (center_dst.y = dst->y + center->y ; center_dst.y < dst->y + dst->h - center_dst.h; center_dst.y += center_dst.h) {
		for (center_dst.x = dst->x + center->x ; center_dst.x < dst->x + dst->w - center_dst.w; center_dst.x += center_dst.w) {
			SDL_RenderCopy(renderer, tiles, &center_src, &center_dst);
		} }
		
		for (top_dst.x = dst->x + center->x ; top_dst.x < dst->x + dst->w - top_dst.w; top_dst.x += top_dst.w) {
			SDL_RenderCopy(renderer, tiles, &top, &top_dst);
			SDL_RenderCopy(renderer, tiles, &bottom, &bottom_dst);
			bottom_dst.x += bottom_dst.w;
		}

		for (left_dst.y = dst->y + center->y ; left_dst.y < dst->y + dst->h - left_dst.h; left_dst.y += left_dst.h) {
			SDL_RenderCopy(renderer, tiles, &left, &left_dst);
			SDL_RenderCopy(renderer, tiles, &right, &right_dst);
			right_dst.y += right_dst.h;
		}

	}

	SDL_RenderCopy(renderer, tiles, &top_left, &top_left_dst);
	SDL_RenderCopy(renderer, tiles, &bottom_left, &bottom_left_dst);
	SDL_RenderCopy(renderer, tiles, &top_right, &top_right_dst);
	SDL_RenderCopy(renderer, tiles, &bottom_right, &bottom_right_dst);
}

void draw_pushbutton(SDL_Renderer *renderer, Uint32 x, Uint32 y, Uint8 tw, Uint8 th, int state) {

	SDL_Rect src = { TILE_BUTTON_X * TILE_W + state * TILE_W, TILE_BUTTON_Y * TILE_H, TILE_W, TILE_H };  

	SDL_Rect dst = { x, y, tw * TILE_W, th * TILE_H };

	SDL_Rect center = { TILE_W/4, TILE_H/4, TILE_W/2, TILE_H/2 };

	draw_scale9x(renderer, &src, &center, &dst, SDL_FALSE); 
}

Uint32 mouse_x = 0;
Uint32 mouse_y = 0;
Uint8  mouse_push = 0;
Sint16 mouse_hover = -1;

SDL_Rect buttons[16] = {
	{ 240, 180 + (TILE_H+BUTTON_PADDING) * 0, 8, 1 },
	{ 240, 180 + (TILE_H+BUTTON_PADDING) * 1, 8, 1 },
	{ 240, 180 + (TILE_H+BUTTON_PADDING) * 2, 8, 1 },
	{ 240, 180 + (TILE_H+BUTTON_PADDING) * 3, 8, 1 },
	{ 240, 180 + (TILE_H+BUTTON_PADDING) * 4, 8, 1 },
};
const char *button_names[16] = {
	"Debug Mode",
	"Single Player",
	"Multi Player",
	"Options",
	"Quit Game",
};
int button_ok[16] = {
	1,
	0,
	0,
	0,
	1,
};
int button_text_offx = 0;
int button_text_offy = 0; 

int max_buttons = 5; 

void adjust_buttons() {
	int i;
	
	Uint32 x = ui.log_width / 3;
	Uint32 y = ui.log_height / 4; 	

	int zoom = 1;
	
	if (ui.no_mouse) zoom = 2;


	if (zoom > 1) {
		button_text_offx = 2 * zoom;
		button_text_offy = 4 * zoom;
	} 

	for (i = 0; i < max_buttons; i++) {
	
		SDL_Rect *btn = &buttons[i];

		btn->x = x;
		btn->y = y + (TILE_H * zoom + BUTTON_PADDING) * i;
		btn->h = zoom;

	}

}

void draw_mainmenu(SDL_Renderer *renderer) {

	//SDL_SetRenderDrawColor(renderer, 0xff, 0xff, 0xff, 255);
    //SDL_RenderClear(renderer);

	Uint8 push_offset_x[5] = { 0, 0, 0, 0, 2 };
	Uint8 push_offset_y[5] = { 0, 0, 0, 0, 1 };
	SDL_Color color_offset[5] = {
		{ 0x00, 0x00, 0x00, 0x00 },
		{ 0x33, 0x33, 0x33, 0x33 }, 
		{ 0xCC, 0xCC, 0xCC, 0xCC },
		{ 0x33, 0x33, 0x33, 0x33 },
		{ 0xEE, 0xEE, 0xEE, 0xEE }, 
	};

	SDL_RenderCopy(renderer, bg, NULL, NULL);

	int i;
	for (i = 0; i < max_buttons; i++) {

		int hover = 0;

		if (mouse_hover == i) hover = 1 + mouse_push;
		
		if (button_ok[i]) hover += 2;
		else hover = 1;

		draw_pushbutton(renderer, buttons[i].x, buttons[i].y, buttons[i].w, buttons[i].h, hover);
		incolor1(&color_offset[hover]);
		inprint(renderer, button_names[i], 
			buttons[i].x + BUTTON_PADDING + push_offset_x[hover] + button_text_offx, 
			buttons[i].y + BUTTON_PADDING + push_offset_y[hover] + button_text_offy);

	}

	/* Draw cursor */
	draw_tile(renderer, 22, 4 + (mouse_hover != -1 ? 1 : 0) + mouse_push, mouse_x - 4, mouse_y - 4);
}

void track_mouse_mainmenu() {

	int i;
	mouse_hover = -1;
	for (i = 0; i < max_buttons; i++) {
	
		SDL_Rect test = { buttons[i].x, buttons[i].y, buttons[i].w * TILE_W, buttons[i].h * TILE_H };
		
		if (SDL_InBounds(mouse_x, mouse_y, &test)) {
		
			mouse_hover = i;
			break;
		}
	}
	if (mouse_hover == -1) mouse_push = 0;
}

int do_mainmenu_event(SDL_Event *e) {
	if (e->type == SDL_MOUSEMOTION && e->motion.which != SDL_TOUCH_MOUSEID) {
		mouse_x = e->motion.x;
		mouse_y = e->motion.y;
	}
	if ((e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP)  && e->motion.which != SDL_TOUCH_MOUSEID) {
		mouse_x = e->button.x;
		mouse_y = e->button.y;
	}
	if (e->type == SDL_FINGERUP) {
		mouse_x = (e->tfinger.x * ui.log_width);
		mouse_y = (e->tfinger.y * ui.log_height);
		track_mouse_mainmenu();

		if (mouse_hover != -1) {
			switch (mouse_hover) {
				case 0:
					return 1;
					break;
				case 4:
					return -1;
					break;
				default: break;
			
			}		
		}
	}
	if (e->type == SDL_MOUSEBUTTONDOWN  && e->motion.which != SDL_TOUCH_MOUSEID) {
		mouse_push = 1;
	}
	if (e->type == SDL_MOUSEBUTTONUP  && e->motion.which != SDL_TOUCH_MOUSEID) {
		mouse_push = 0;

		if (mouse_hover != -1) {
			switch (mouse_hover) {
				case 0:
					return 1;
					break;
				case 4:
					return -1;
					break;
				default: break;
			
			}		
		}
	}
	track_mouse_mainmenu();
	return 0;
}

/* Returns -1 to quit game, 1 to continue somewhere... */
int mainmenu_loop(SDL_Renderer *renderer) {

	int done = 0;

	SDL_ShowCursor(SDL_FALSE);

	SDL_Color white = { 0xff, 0xff, 0xff, 0xff };

	bg = ASS_LoadTexture("data/endowment.bmp", NULL);
	tiles = ASS_LoadTexture("data/gfx/runelord.bmp", &white);

	adjust_buttons();

	while (!done)
	{
	    SDL_Event event = { 0 };
		while (SDL_PollEvent(&event)) {
			done = do_mainmenu_event(&event);
			if (event.type == SDL_QUIT) done = -1;
			if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) done = -1;
		}

		draw_mainmenu(renderer);

        SDL_RenderPresent(renderer);
		SDL_Delay(10);
	}

	ASS_FreeTexture(bg);

	SDL_ShowCursor(SDL_TRUE);

	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 255);
    SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

	return done;
}