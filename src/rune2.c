#include "libs/savepng/savepng.h"
#include "libs/lazyass/lazyass.h"
#include "libs/binaryheap/binhl.h"
#include "libs/SDL_inprint/SDL2_inprint.h"

#include "rune.h"
#include "runeconf.h"
#include "mana.h"
#include "game.h"
#include "ui.h"
#include "draw.h"
#include "utils.h"
#include "ai.h"
#include "mainmenu.h"

void init_factions() {

	memset(factions, 0, sizeof(faction_t) * 8);

	strcpy(factions[0].title, "Your Kingdom");
	factions[0].color.r = 0;
	factions[0].color.g = 0;
	factions[0].color.b = 0xff;
	factions[0].gold = 10000;

	strcpy(factions[1].title, "Reavers");
	factions[1].color.r = 0xff;
	factions[1].color.g = 0;
	factions[1].color.b = 0;
	factions[1].gold = 10000;

}

void clear_house(int id) {
	house_t *h = &houses[id];
	h->tile = 0;
	h->w = 2;
	h->h = 2;

	memset(h->flag_id, -1, sizeof(Sint8) * MAX_FACTIONS);

	/* Assign visual refs */
	h->axis_refs[HAXIS_TYPE] = &h->tile;
	h->axis_refs[HAXIS_STATE] = &h->built;
	h->axis_refs[HAXIS_FRAME] = &h->frame;
}

void clear_unit(int id) {
	unit_t *u = &units[id];

	memset(u, 0, sizeof(unit_t));

	u->refresh.path = 1;

	memset(u->flag_id, -1, sizeof(Sint8) * MAX_FACTIONS);

	u->axis_refs[UAXIS_TYPE] = &u->tile;
	u->axis_refs[UAXIS_MODE] = &u->mode;
	u->axis_refs[UAXIS_FRAME] = &u->frame;
	u->axis_refs[UAXIS_FACEFRAME] = &u->faceframe;
}

void init_houses() {
	int i;
	for (i = 0; i < MAX_HOUSES; i++) {
		clear_house(i);
	}
	num_houses = 0;
}

void init_units() {
	int i;
	for (i = 0; i < MAX_UNITS; i++) {
		clear_unit(i);
	}
	num_units = 0;
}

void remove_flag(int id) {
	flag_t *f = &your->flags[id];

	if (f->house) {
		f->house->flag_id[ui.faction] = -1; 
	}
	if (f->unit) {
		f->unit->flag_id[ui.faction] = -1;
	}
	
	if (id < your->num_flags) {
		memcpy(&your->flags[id], &your->flags[your->num_flags - 1], sizeof(flag_t));
		if (ui.flag == your->num_flags - 1) ui.flag = id;
	}
	your->num_flags--;
	if (ui.flag == id) ui.flag = -1;
}

int add_flag(Uint8 tx, Uint8 ty, Uint8 type) {

	int id = your->num_flags;

	flag_t *f = &your->flags[id];

	f->type = type;
	f->reward = 100;
	
	your->gold -= 100;

	if (type == 0) {
		f->x = tx;
		f->y = ty;
		f->w = 1;
		f->h = 1;
	} else {

		if (ui.hover == overUnit) {
			unit_t *u = &units[ui.hover_id]; 
			f->unit = u;
		
			f->w = bunits[u->tile].w;
			f->h = bunits[u->tile].h;

			u->flag_id[ui.faction] = id;

		} else if (ui.hover == overHouse) {
		
			f->house = &houses[ui.hover_id];

			f->w = houses[ui.hover_id].w;
			f->h = houses[ui.hover_id].h;
			
			houses[ui.hover_id].flag_id[ui.faction] = id;

		} else {
			printf("Error!\n");
		}
	
	}

	f->num_units = 0;

	your->num_flags++;

	return id;
} 
void add_reward() {

	flag_t *f = &your->flags[ui.flag];

	if (your->gold < 100) {

		log_add(&gamelog, "Not enough gold, your majesty!");

		return;
	}

	your->gold -= 100;
	f->reward += 100;
}

int add_house(Uint32 tx, Uint32 ty) {
	int id = num_houses;

	house_p *m = &bhouses[ui.builder];	

	house_t *h = &houses[id];
	clear_house(id);

	h->tile = ui.builder;//m->tile;
	h->x = tx;
	h->y = ty;
	h->w = m->w;
	h->h = m->h;

	h->built = 0;
	h->hp = 1;
	h->max_hp = 100;

	h->faction = 0;/* :( */

	h->capacity = h->w * h->h;

	your->gold -= m->gold_cost;

	num_houses++;

	return id;
}

void stress_unit(int id, int dmg) {
	unit_t *u = &units[id];
	u->dmg += dmg;
}

void kill_unit(int id) {
	unit_t *u = &units[id];
	u->tile = 0;

//	remove_wtf(id);
	/* When devotee dies, his link to lord is removed */
	if (u->link) {
		///* Lord is alarmed */
		//unit_t *l = u->link;
		//while (l->link) l = l->link;
		//l->baloon = 1;

		u->link->ref_count[u->link_stat]--;
		u->link->ref_counts--;
		u->link = NULL;
	}
	/* When lord dies, his devotees are freed! */
	if (u->ref_count[u->link_stat]) {
		int i;
		for (i = 0; i < num_units; i++) {
			if (units[i].link == u) {
				units[i].baloon = 1;
				units[i].link = NULL;
				u->ref_count[u->link_stat]--;
				u->ref_counts--;
			}
		}
	}
	rebuild_pools();
}

int spawn_unit(Uint8 type, Uint8 faction, Uint8 x, Uint8 y) {
	int i;
	int id = num_units++; //TODO: proper handling

	unit_t *u = &units[id];
	unit_p *p = &bunits[type];

	clear_unit(id);

	generate_name(u);

	u->tile = type;
	u->faction = faction;
	u->tx = u->x = x;
	u->ty = u->y = y;

	for (i = 0; i < MAX_STAT; i++) {
		u->base_stat[i] = p->base_stat[i];
	}

	u->color = 0x6c441c;
	
	return id;	
}

int find_flagT(Uint8 tx, Uint8 ty) {
#if 0
	int i;
	for (i = 0; i < your->num_flags; i++) {
		flag_t *f = &your->flags[i];
		if (tx >= f->x && tx <= f->x+(f->w-1) &&
		    ty >= f->y && ty <= f->y+(f->h-1))
		{
			return i;
		}
	}
	return -1;
#else
	return flag_grid_i[ty][tx];
#endif	
}

int find_houseT(Uint8 tx, Uint8 ty) {
#if 0
	int i;
	for (i = 0; i < num_houses; i++) {
		house_t *h = &houses[i];
		if (tx >= h->x && tx <= h->x+(h->w-1) &&
			ty >= h->y && ty <= h->y+(h->h-1))
		{
			return i;
		}
	}
	return -1;
#else
	return house_grid_i[ty][tx];
#endif
}

int find_house(Uint32 x, Uint32 y) {
	return find_houseT( 
		(x + ui.vx - game_map.x) / TILE_W, 
		(y + ui.vy - game_map.y) / TILE_H
	);
}

int find_unitT(Uint8 tx, Uint8 ty) {
#if 0
	int i;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		unit_p *p = &bunits[u->tile];
		if (u->visiting) continue;
		if (u->mode == ANIM_DEATH) continue;
		if (tx >= u->x && tx <= u->x+(p->w-1) &&
			ty <= u->y && ty >= u->y-(p->h-1) )
		{
			return i;
		}
	}
	return -1;
#else
	/*unit_t *u = unit_grid[ty][tx];
	if (u != NULL) {
		if (u->visiting) return -1;
		if (u->mode == ANIM_DEATH) return -1;
	}*/
	return unit_grid_i[ty][tx];
#endif
}

int find_unit(Uint32 x, Uint32 y) {
	x = x + ui.vx - game_map.x;
	y = y + ui.vy - game_map.y;

	int i;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		unit_p *p = &bunits[u->tile];
		if (u->visiting) continue;
		SDL_Rect pos;

		pos.x =  u->x           * TILE_W + u->ox;
		pos.y = (u->y-(p->h-1)) * TILE_H + u->oy;
		
		pos.w = TILE_W * p->w;
		pos.h = TILE_H * p->h;
		
		if (SDL_InBounds(x, y, &pos)) 
		{
			return i;
		}
	}
	return -1;
}

int xcollide(Uint8 tx, Uint8 ty, Uint8 w, Uint8 h) {
	int i, j;
	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			//if (fog[ty + j][tx + i]) return 1;
			if (find_houseT(tx + i, ty + j) != -1) return 1;
			if (find_unitT (tx + i, ty + j) != -1) return 1;
		}
	}
	return 0;
}

#include "SDL2_particles.h"

particle_system psystems[128];
int num_psystems = 0;

void add_fire(Sint16 x, Sint16 y, Uint16 limit) {
	int id = num_psystems++;
	particle_system *ps = &psystems[id];

	ps->x = x;
	ps->y = y;
	ps->limit = limit;
	ps->init = &basic_particle_init;
	ps->move = &basic_particle_move;
	ps->reset = &basic_particle_reset;
	ps->spawn = 0;
}

void update_particles() {
	int i;
	for (i = 0; i < num_psystems; i++) {
		particle_system *ps = &psystems[i];
		PS_Update(ps);
	}
}

void draw_particles(SDL_Renderer *target, Uint32 bx, Uint32 by) {

	int i;
	for (i = 0; i < num_psystems; i++) {
		particle_system *ps = &psystems[i];
		if (!ps->limit) continue;
		PS_Render(target, ps, reds, 1, bx, by);
	}

}

void init_fog() {
	int tx, ty;

	for (ty = 0; ty < level_h; ty++) {
		for (tx = 0; tx < level_w; tx++) {
			fog[ty][tx] = 1;
			scent_human[ty][tx] = 0;
	} }

}

void calc_fog() {

	int tx, ty;

	for (ty = 0; ty < level_h; ty++) {
		for (tx = 0; tx < level_w; tx++) {
		//	fog[ty][tx] = 1;
		if (scent_human[ty][tx] > 0) scent_human[ty][tx]--;
		if (scent_human[ty][tx] > 255) scent_human[ty][tx] = 255;
	} }

	int i;

	for (i = 0; i < num_houses; i++) {
		house_t *h = &houses[i];
		int j, k, bl = h->h/2;
		for (j = -h->h+bl; j < h->h+bl+1; j++) {
			if (h->y + j < 0) continue;
			if (h->y + j >= level_h) break;
			for (k = -h->w; k < h->w+1; k++) {
				if (h->x + k < 0) continue;
				if (h->x + k >= level_w) break;
				fog[h->y + j][h->x + k] = 0;
			}
		}
	}
	
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		unit_p *p = &bunits[u->tile];
		int j, k;
		for (j = -1; j < 2; j++) {
			if (u->y + j < 0) continue;
			if (u->y + j >= level_h) break;
			for (k = -1; k < 2; k++) {
				if (u->x + k < 0) continue;
				if (u->x + k >= level_w) break;
				if (fog[u->y + j][u->x + k] == 1) {
					u->exp += 1;
				}
				fog[u->y + j][u->x + k] = 0;
				scent_human[u->y + j][u->x + k] += p->base_scent[SCENT_HUMAN];	
			}
		}
	
	}
}

void add_peasant();
void init_game() {

	level_w = LEVEL_W;
	level_h = LEVEL_H;

	init_factions();

	your = &factions[0];
	ui.faction = 0;

	reset_ui();
	
	log_reset(&gamelog);
	log_add(&gamelog, "Started");

	add_fire(100,100,25000);

	num_pools = 0;

	ui.vx = 0; 
	ui.vy = 0;	

	prepare_colors();

    tiles = ASS_LoadTexture("data/gfx/runelord.bmp", &white);

	cfg_load("data/rune.cfg");

	uibg = ASS_LoadTexture("data/gfx/woodui.bmp", &white);

	small_font = ASS_LoadTexture("data/fonts/oddball6x8.bmp", &magenta);

	mid_font = ASS_LoadTexture("data/fonts/webby8.bmp", &black);

	large_font = NULL; /* aka inline */

	infont(large_font);

	init_units();

	init_houses();

	init_fog();

	int i;
	for (i = 0; i < 50; i++) {

		add_peasant();

	}

	units[0].x = units[0].y = units[0].tx = units[0].ty = 0;	

	units[1].tx = units[1].x = (units[1].x - 3);
	units[1].ty = units[1].y = (units[1].y + 2);
	units[2].tx = units[2].x = (units[2].x - 2);
	units[2].ty = units[2].y = (units[2].y + 4);
//	random_links();
}

void focus_on_unit() {
	unit_t *u = &units[ui.unit];
	int nvx = u->tx * TILE_W - ui.log_width / 2;
	int nvy = u->ty * TILE_H - ui.log_height / 2;

	ui.flingx = nvx - ui.vx;
	ui.flingy = nvy - ui.vy;
}

void do_update_grids(void) {
	int i, j, k;
	
	for (j = 0; j < level_h; j++)
	for (i = 0; i < level_w; i++) {

		unit_grid[j][i] = NULL;
		house_grid[j][i] = NULL;

		flag_grid_i[j][i] = -1;
		unit_grid_i[j][i] = -1;
		house_grid_i[j][i] = -1;

	}

	for (i = 0; i < your->num_flags; i++) {
		flag_t *flag = &your->flags[i];
		for (k = 0; k < flag->h; k++) for (j = 0; j < flag->w; j++) {

			flag_grid_i[flag->y + k][flag->x + j] = i;

		}
	}

	for (i = 0; i < num_houses; i++) {
		house_t *house = &houses[i];
		for (k = 0; k < house->h; k++) for (j = 0; j < house->w; j++) {

			house_grid[house->y + k][house->x + j] = house; 	
			house_grid_i[house->y + k][house->x + j] = i;

		}
	}

	for (i = 0; i < num_units; i++) {
		unit_t *unit = &units[i];
		unit_p *proto = &bunits[unit->tile];
		if (unit->visiting) continue;
		if (unit->mode == ANIM_DEATH) continue;
		for (k = 0; k < proto->h; k++) for (j = 0; j < proto->w; j++) {

			unit_grid[unit->y - (proto->h-1) + k][unit->x + j] = unit; 	
			unit_grid_i[unit->y - (proto->h-1) + k][unit->x + j] = i;
		}
	}
}

void do_update_links(void) {
	update_stats(1);
//	rebuild_pools();
	collect_pools();
	distrib_pools();
	update_stats(0);
}

void do_scroll_out() {
	if (ui.unit == -1) return;
	unit_t *su = &units[ui.unit];

	int fs[MAX_UNITS];

	int f = 0;
	int i;
	for (i = 0; i < num_units; i++) if (units[i].link == su) fs[f++] = i;
	if (f > 0) { 
		i = rand() % f;
		printf("Rand is: %d\n", i);
		ui.unit = fs[i];
		focus_on_unit();
	} 
}

void do_scroll_in() {
	if (ui.unit == -1) return;
	unit_t *u = &units[ui.unit];

	if (u->link) {
		ui.unit = u->link_id;
		focus_on_unit();
	}
}

void do_pin_click() {

	if (ui.hover == overListName) {

		ui.unit = ui.hover_id;

	}
	if (ui.hover == overListPin) {

		units[ui.hover_id].pin = 0;

	}

}

void do_minimap_click() {
	int x = ui.x - minimap.x;
	int y = ui.y - minimap.y;

	int zY = (level_h * TILE_W) / minimap.h;
	int zX = (level_w * TILE_H) / minimap.w;

	int vx = x * zX;
	int vy = y * zY;

	ui.vx = vx - game_map.w / 2;
	ui.vy = vy - game_map.h / 2;
	
	ui.flingx = 0;
	ui.flingy = 0;
}


void do_button() {
	unit_t *u = &units[ui.unit];
#define U_PEASANT 1
#define U_MILITIA 3
#define U_ARCHER 4
#define U_KNIGHT 5
#define U_FLAMER 6
	int btn = ui.btn;
	if (u->tile == U_PEASANT) {
		int cost = 0;
		int next = U_PEASANT;
		switch (btn) {
			case 0:	next = U_MILITIA;	cost = 250;		break;
			case 1:	next = U_ARCHER; u->base_stat[S_DEX]+=5;	cost = 200;		break;
			case 2:	next = U_KNIGHT;	cost = 350;		break;
			case 3:	next = U_FLAMER+rand()%4;	cost = 500;		break;
		default: break;
		}
		if (cost) {
			your->gold -= cost;
			u->tile = next;
		} 
	}
#undef U_PEASANT
#undef U_MILITIA
#undef U_ARCHER
#undef U_KNIGHT
#undef U_FLAMER
}

void do_event_cancel() {
	if (ui.stat != -1) {
		ui.stat = -1;
		return;
	}
	ui.flag = -1;
	ui.setflag = -1;
	ui.unit = -1;
	ui.builder = -1; 
	ui.house = -1;
	ui.stat = -1;
	ui.btn = -1;
	return;
}

void do_event_click(Uint32 x, Uint32 y) {

	//printf("Clicked on %d, %d\n", x, y);

	/* Setting flags? */
	if (ui.setflag != -1) {
	
		if (ui.hover_top == overMap) {
		
			if (ui.hover_xcollide != ui.setflag || ui.hover == overFlag) {

				log_add(&gamelog, "Can't place here");

			} else if (your->gold < 100) {

				log_add(&gamelog, "Not enough gold");

			} else {

				/* erect flag */
				ui.flag = add_flag(ui.hover_tx, ui.hover_ty, ui.setflag);
				ui.setflag = -1;

			}

		}

		return;
	}

	/* Building houses? */
	if (ui.builder != -1) {

		if (ui.hover_top == overMap) {

			if (ui.hover_xcollide) {
				log_add(&gamelog, "Can't place here");
				return;
			} else {

				/* build house */
				ui.house = add_house(ui.hover_tx, ui.hover_ty);
				ui.builder = -1;

			}
		}
		return;
	}

	switch (ui.hover) {

		/* UI PANE */

		case overMinimap:

			do_minimap_click();		
		
		break;
		case overFlagButton:
		
			ui.setflag = ui.hover_id;
		
		break;
		case overBuildButton:

			ui.builder = ui.hover_id;

		break;
		case overPinBox:
		case overListName:
		case overListPin:

			do_pin_click();

		break;
		case overHouseTab:

			ui.housetab = ui.hover_id;
	
		break;
		case overUnitTab:

			ui.unittab = ui.hover_id;

		break;
		case overUnitPin:

			units[ui.unit].pin = 1 - units[ui.unit].pin;		

		break;
		case overHouseFlag:

			ui.flag = houses[ui.unit].flag_id[ui.hover_id];
			if (ui.flag != -1) ui.house = -1;

		break;
		case overUnitFlag:

			ui.flag = units[ui.unit].flag_id[ui.hover_id];
			if (ui.flag != -1) ui.unit = -1;

		break;
		case overUnitStat:

			/* Start rune linking */
			if (units[ui.unit].link == NULL) ui.stat = ui.hover_id;

		break;
		case overUnitSkill:

			/* Start upgrade */
			if (units[ui.unit].link == NULL) ui.btn = ui.hover_id;

			do_button();
			ui.btn = -1;

		break;
		case overFlagPlus:

			add_reward();

		break;

		/* GAME MAP */

		case overUnit:

			/* rune-link */
			if (ui.stat != -1) {
				if (ui.hover_id != ui.unit) { // add other verefications, like factions, costs, etc

					add_link(ui.unit, ui.hover_id, ui.stat);

					ui.unit = ui.hover_id; /* Also select */

					/* Stop rune-linking, if it's not possible */
					if (units[ui.unit].link != NULL || units[ui.unit].ref_count[ui.stat] > 1) ui.stat = -1;
				}
			} 
			/* select */
			else {
				ui.house = -1;
				ui.flag = -1; 
				ui.unit = ui.hover_id;
			}

		break;
		case overHouse:

			ui.unit = -1;
			ui.flag = -1;
			ui.house = ui.hover_id;

		break;
		case overFlag:

			ui.unit = -1;
			ui.house = -1;
			ui.flag = ui.hover_id;

		break;
		default:

			if (ui.no_mouse) {
				do_event_cancel();
			}
		
		break;	
	}

}

void debug_dump_animation(animation_t *a) {

	//SDL_Rect src = { a->plane.x, a->plane.y, a->plane.w, a->plane.h };
	
	printf("Plane: %d, %d -- %d, %d\n", a->plane.x, a->plane.y, a->plane.w, a->plane.h );

	int i;
	//...offset by each modifier axis
	for (i = 0; i < a->num_axises; i++) {

		Uint8 ask = a->axis_modifier[i];
		//Uint8 ref = *(u->axis_refs[ask]);
		//src.x += ref * a->axis_offset[i].w + a->axis_offset[i].x;
		//src.y += ref * a->axis_offset[i].h + a->axis_offset[i].y;

		printf("    Axis %d -- modifier %d, mult by X %d Y %d, plus X %d Y %d\n",
			i, ask,
			a->axis_offset[i].w,
			a->axis_offset[i].h,
			a->axis_offset[i].x,
			a->axis_offset[i].y );

    }
}
void debug_dump_unitp(unit_p *p) {

	printf("BODY:\n");
	debug_dump_animation(&p->body);
	printf("FACE:\n");
	debug_dump_animation(&p->face);

}
void debug_dump_unit(unit_t *u) {
	debug_dump_unitp(&bunits[u->tile]);
}

void add_reaver() {

	int id = spawn_unit(22, 1, rand() % level_w, rand() % level_h);
	unit_t *u = &units[id];

	strcpy(u->name, "Path Finder");

}

void add_rat() {
	int id = spawn_unit(17, 1, rand() % level_w, rand() % level_h);
	unit_t *u = &units[id];
	strcpy(u->name, "Ratticate");
}

void add_peasant() {

	int id = spawn_unit(1, 0, rand() % level_w, rand() % level_h);
	unit_t *u = &units[id];

	generate_name(u);
}


void do_event_key(int sym, int scan) {

	if (sym == SDLK_AC_BACK) do_event_cancel();
	if (scan == SDL_SCANCODE_MENU) add_reaver();
	if (sym == SDLK_AC_SEARCH) ui.draw_minimap = 1- ui.draw_minimap;
	//if (scan == SDL_SCANCODE_MENU) show_menu();

		if (sym == SDLK_m) debug_dump_unitp(&bunits[units[ui.unit].tile]);

		if (sym == SDLK_1) ui.game_speed = 25;
		if (sym == SDLK_5) ui.game_speed = 1;	

		if (sym == SDLK_s) ui.draw_scent = 1 - ui.draw_scent;
		if (sym == SDLK_t) ui.draw_path = 1 - ui.draw_path;
		if (sym == SDLK_f) ui.draw_fog = 1 - ui.draw_fog;
		if (sym == SDLK_l) ui.draw_overlays = 1 - ui.draw_overlays;	
		if (sym == SDLK_p) ui.draw_pools = 1 - ui.draw_pools;
	
		if (sym == SDLK_z) add_reaver();
		if (sym == SDLK_x) add_rat();
	    if (sym == SDLK_a) add_peasant();
		if (sym == SDLK_n) units[ui.unit].tile++;
		if (sym == SDLK_q) { units[ui.unit].top_desire++; units[ui.unit].top_method = 0; } 
		if (sym == SDLK_k) stress_unit(ui.unit,1);
		if (sym == SDLK_r) rebuild_pools();
		if (sym == SDLK_c) collect_pools();
		//if (sym == SDLK_x) prop_pools();
		if (sym == SDLK_d) distrib_pools();
		if (sym == SDLK_e && ui.unit) units[ui.unit].carry_gold += 100;
		if (sym == SDLK_i) units[ui.unit].energy += 5;
		if (sym == SDLK_b) {
			if (ui.unit != -1) {
				units[ui.unit].tx = units[ui.unit].x = ui.hover_tx;
				units[ui.unit].ty = units[ui.unit].y = ui.hover_ty;
			}
		}
		if (sym == SDLK_g) {
			if (ui.unit != -1) {
				units[ui.unit].tx = ui.x / 16;
				units[ui.unit].ty = ui.y / 16;
			}
			if (ui.house != -1) {
				houses[ui.house].hp += 20;
				if (houses[ui.house].hp >= houses[ui.house].max_hp) {
					houses[ui.house].hp = houses[ui.house].max_hp;
					houses[ui.house].built = 1;
				}
			}
			return;
		}
}

void do_event_mouse(SDL_Event *e) {

	int tracking_mouse = 0;

	if (e->type == SDL_MOUSEMOTION) {
	
		if (e->motion.which == SDL_TOUCH_MOUSEID) return;
	
		ui.x = e->motion.x;
		ui.y = e->motion.y;

		if (ui.dragging) {
			ui.flingx -= e->motion.xrel;
			ui.flingy -= e->motion.yrel;
		}

		track_mouse();
	}
	if (e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP) {

		if (e->button.which == SDL_TOUCH_MOUSEID) return;

		ui.x = e->button.x;
		ui.y = e->button.y;

		tracking_mouse = 1;
		track_mouse();
	}

	if (e->type == SDL_MOUSEBUTTONDOWN) {
		if (e->button.button == 2) { /* hold middle button */
			ui.dragging = 1;
		}
		if (e->button.button == 1) { /* left click (handled on mousedown for smoother UI animations) */
			do_event_click(ui.x, ui.y);
			ui.pushing = 1;
		}
	}

	if (e->type == SDL_MOUSEBUTTONUP) {
		if (e->button.button == 2) { /* release middle button */
			ui.dragging = 0;
		}
		if (e->button.button == 3) { /* right click */
			do_event_cancel();
		}
		if (e->button.button == 1) { /* release left button */
			ui.pushing = 0;
		}
	}

	/* Mouse moved */
	if (tracking_mouse) track_mouse();
}

void do_event(SDL_Event *e) {

	if (e->type == SDL_FINGERDOWN) {

		ui.x = (int) (e->tfinger.x * ui.log_width);
		ui.y = (int) (e->tfinger.y * ui.log_height);

		track_mouse();
		do_event_click(ui.x, ui.y);
	}

	if (e->type == SDL_FINGERMOTION) {

		if (e->tfinger.fingerId == 0) {

			ui.flingx -= (int) (e->tfinger.dx * ui.log_width);
			ui.flingy -= (int) (e->tfinger.dy * ui.log_height);

		}
	}

	if (e->type == SDL_MOUSEMOTION
	||  e->type == SDL_MOUSEBUTTONDOWN
	||  e->type == SDL_MOUSEBUTTONUP) {

		do_event_mouse(e);

	}

	if (e->type == SDL_KEYDOWN) {

		do_event_key(e->key.keysym.sym, e->key.keysym.scancode);

	}

	if (e->type == SDL_MOUSEWHEEL) {

		if (e->wheel.y > 0)     	do_scroll_in();
		else if (e->wheel.y < 0)	do_scroll_out();

	}

}


void do_unit_in_house(unit_t *u, house_t *h) {

	/* Unloading gold */
	if (u->carry_gold >= 100 && h->tile == 7) {
		u->progress++;
		if (u->progress >= 10) {
			u->progress = 0;
			u->carry_gold -= 100;
			your->gold += 100;
		}
	}
	else
	/* Mining inside */
	if (h->tile == 2 && u->carry_gold < 100) {
		u->progress++;
		u->stat_bid[S_STR] = 1;
		if (u->progress >= 10) {
			u->progress = 0;
			int mined = u->calc_stat[S_STR] * 3;
			if (mined == 0) mined = 1;
			u->carry_gold += mined;
		}
	}
	else
	/* Seeking shelter? */ 
	if (u->dmg) {
		u->stat_bid[S_MET] = 1;
		u->progress++;
		if (u->progress >= 10) {
			u->progress = 0;
			int healed = u->calc_stat[S_MET] * 3;
			if (healed > u->dmg) healed = u->dmg;
			u->dmg -= healed;
		}
	}
	else
	/* Leaving */
	{
		u->visiting = NULL;
		h->visitors--;
	}
}

void compute_stats() {
	int i;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];

		u->max_hp = u->calc_stat[S_CON] * 10;
		if (u->max_hp < 1) u->max_hp = 1;

		/* TODO: better formulas than this PoS? */
		u->calc_skill[SKILL_BUILD] = u->calc_stat[S_STR] + u->base_skill[SKILL_BUILD];
		u->calc_skill[SKILL_REPAIR] = u->calc_stat[S_STR] + u->base_skill[SKILL_REPAIR];
		u->calc_skill[SKILL_MELEE] = u->calc_stat[S_STR] + u->base_skill[SKILL_MELEE];
		u->calc_skill[SKILL_RANGED] = u->calc_stat[S_STR] + u->base_skill[SKILL_RANGED];
		u->calc_skill[SKILL_RIDE] = u->calc_stat[S_STR] + u->base_skill[SKILL_RIDE];
		u->calc_skill[SKILL_MAGERY] = u->calc_stat[S_STR] + u->base_skill[SKILL_MAGERY];
	}
} 


void unit_claim_flag(unit_t *u, int j) {
	flag_t *flag = &your->flags[j];

	if (flag->type == 0) {
		u->gold += flag->reward;
		u->baloon = 2;

		remove_flag(j);
	}
}

void do_unit_hit_unit(unit_t *u, int j) {
	unit_t *target = &units[j];

	if (target->faction == u->faction) return;

	u->mode = ANIM_HIT;

	stress_unit(j, 2);
}

void do_unit_hit_house(unit_t *u, int k) {
	house_t *h = &houses[k];
	/* Attacking ? */
	if (u->faction != h->faction) {


	}
	/* Building ? */
	if (h->built == 0) {
		u->progress++;
		if (u->progress >= 10) {
			u->progress = 0;
			h->hp++;
		}
		if (h->hp >= h->max_hp) {
			h->built = 1;
		}
		u->mode = ANIM_HIT;
	}
	else
	/* Entering mine */
	if (h->tile == 2)
	{
		if (!u->visiting && h->visitors < h->capacity)
		{
			u->visiting = h;
			h->visitors++;
		}
	}
	else
	/* Mining ? */	
	if (h->tile == 2) {
		u->progress++;
		if (u->progress >= 10) {
			u->progress = 0;
			u->gold++;
		}
	}
	else
	/* Visiting (to unload gold) ? */
	if (h->tile == 7 && u->carry_gold >= 100) {
		if (!u->visiting && h->visitors < h->capacity)
		{
			u->visiting = h;
			h->visitors++;
		}	
	}
	//printf("%s HITTING HOUSE %s\n", u->name, h->title);
}

void do_unit_walk(unit_t *u) {
	unit_p *p = &bunits[u->tile];
	int next_x = 0;
	int next_y = 0;

	int h_ind, u2_ind, f_ind;
	int i, j;

	if (u->x < u->tx) next_x = + 1;
	if (u->x > u->tx) next_x = - 1;
	if (u->y < u->ty) next_y = + 1;
	if (u->y > u->ty) next_y = - 1;
//printf("Walk: %d, %d\n", next_x, next_y);
#ifndef ALLOW_DIAGONALS	
	if (next_x && next_y) { if (rand() % 2) next_y = 0; else next_x = 0; }
#endif

	for (j = 0; j < p->h; j++) {
	for (i = 0; i < p->w; i++) {

		f_ind = find_flagT(next_x + u->x + i, next_y + u->y - (p->h-1) + j);
		if (f_ind != -1) {
				unit_claim_flag(u, f_ind);
		}

		h_ind = find_houseT(next_x + u->x + i, next_y + u->y - (p->h-1) + j);
		if (h_ind != -1) {
				do_unit_hit_house(u, h_ind);
				u->refresh.path = 1;
			return;
		}

		u2_ind = find_unitT(next_x + u->x + i, next_y + u->y - (p->h-1) + j);
		if (u2_ind != -1) {
				if (&units[u2_ind] == u) {
					//printf("Found ownself :(\n");
					continue;
				}
				do_unit_hit_unit(u, u2_ind);
				u->refresh.path = 1;
			return;
		}

	} }

	u->ox = - next_x * TILE_W;	
	u->oy = - next_y * TILE_H;

	next_x += u->x;
	next_y += u->y;

	u->x = next_x;
	u->y = next_y;

	if (fog[u->y][u->x] == 1) {
		u->exp += 1;
		fog[u->y][u->x] = 0;
	}

	u->mode = ANIM_WALK;
	if (u->carry_gold >= 100) {
		u->mode = ANIM_CARRY;
	}
}

void progress_animation(animation_t *anim, Uint8 anim_id, Uint8 *frame, Sint8 *frame_counter) {

	Sint8 counter = *(frame_counter);

	if (counter >= anim->num_frames[anim_id]) {
		counter = 0;
	}
	if (anim->frame[anim_id][counter] < 0) {
		counter += anim->frame[anim_id][counter];
	}
	if (counter < 0) {
		counter = 0;
	}

	*(frame) = anim->frame[anim_id][counter];
	*(frame_counter) = ++counter;
}

void do_unit_anim(unit_t *u) {
	unit_p *p = &bunits[u->tile];

	Uint8 anim_id;

	if (u->carry_gold) {
		u->mode = ANIM_CARRY;
	}

	anim_id = u->mode;

	progress_animation(&p->body, anim_id, &u->frame, &u->framecnt);

	u->faceframe = p->face.frame[ANIM_FACE][0]; /* faceframe is always first frame for now... */
}

void do_house_anim(house_t *h) {

	house_p *p = &bhouses[h->tile];

	progress_animation(&p->body, 0, &h->frame, &h->framecnt);
}

void do_map_scroll() {
	
	int step = 8;
	
	if (ui.flingy > 0) {
		if (step > ui.flingy) step = ui.flingy;
		ui.vy += step;
		ui.flingy -= step;
	}
	if (ui.flingy < 0) {
		if (-step < ui.flingy) step = -ui.flingy;
		ui.vy -= step;
		ui.flingy += step; 
	}
	if (ui.flingx > 0) {
		if (step > ui.flingx) step = ui.flingx;
		ui.vx += step;
		ui.flingx -= step;
	}
	if (ui.flingx < 0) {
		if (-step < ui.flingx) step = -ui.flingx;
		ui.vx -= step;
		ui.flingx += step;
	}

/* Edges */

	int view_w = game_map.w;
	int view_h = game_map.h;

	int max_w = level_w * TILE_W;
	int max_h = level_h * TILE_H;

	if (ui.y >= 480-1) ui.vy += step;
	if (ui.y <= 0) ui.vy -= step;
	if (ui.x >= 640-1) ui.vx += step;
	if (ui.x <= 0) ui.vx -= step;

	if (ui.vx > max_w - view_w) ui.vx = max_w - view_w;
	if (ui.vy > max_h - view_h) ui.vy = max_h - view_h;
	if (ui.vy < 0) ui.vy = 0;
	if (ui.vx < 0) ui.vx = 0;
}

void do_tick() {

	binh_list unit_list = { 0 };

	int max_energy = 1000;
	int i;
	for (i = 0; i < num_units; i++) {
		binhl_push(&unit_list, i, max_energy - units[i].energy);
	}

	ui.freq++;
	if (ui.freq > 1000) ui.freq = 0;

	//int i;
	int xunits = 0;
	//for (i = 0; i < num_units; i++) {
	while (unit_list.len) {
		xunits++;
		i = binhl_pop(&unit_list);
		unit_t *u = &units[i];
		if (u->energy == 0) break;
		if (u->visiting) {
			if (u->energy >= ECOST_VISIT) {
				do_unit_in_house(u, u->visiting);

				u->energy -= ECOST_VISIT;
			}
			continue;
		}
		if (u->ox != 0 || u->oy != 0) {
			if (u->energy >= ECOST_SLIDE) {
#ifndef ALLOW_DIAGONALS
				if (u->ox) 	u->ox += (u->ox > 0 ? -1 : 1);
				else	    u->oy += (u->oy > 0 ? -1 : 1);
#else
				u->ox += (u->ox > 0 ? -1 : 1);
				u->oy += (u->oy > 0 ? -1 : 1);
#endif
				u->energy -= ECOST_SLIDE;
			}
			continue;
		}
		if (u->x != u->tx || u->y != u->ty) {
		
			if (u->energy >= ECOST_WALK) {
				do_unit_walk(u);
				do_update_grids();
				u->energy -= ECOST_WALK;
			}
		} else u->mode = 0;
	}
	update_particles();
	do_update_grids();
	if (ui.hover_top == overMap) track_mouse_map();
}

void do_phase() {
	do_update_links();
	compute_stats();

	int i;
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
#ifndef DEBUG_AI
		u->energy += 5;
		u->energy += u->calc_stat[S_DEX] * 3;
		if (u->energy > (u->calc_stat[S_DEX]+1) * 10) u->energy = (u->calc_stat[S_DEX]+1) * 10;
#endif
		if (u->dmg) {
			if (u->mode != ANIM_DEATH) u->framecnt = 0;
			u->mode = ANIM_DEATH;
			u->energy = 0;
//			continue;
		}

		do_unit_anim(u);
	}
	calc_fog();
}

void do_turn() {
	int i;
	profile(0, NULL);
	for (i = 0; i < num_units; i++) {
		unit_t *u = &units[i];
		u->baloon = 0;
		if (u->mode == ANIM_DEATH) continue;
		unit_think(i);
	}
	profile(0, "Unit think");
}

void time_tick() {

	static Uint32 wait = 0;
	static Uint32 ticks = 0;
	static Uint32 phases = 0;

	Uint32 tick = SDL_GetTicks();
	Uint32 pass = tick - wait;

	do_map_scroll();

	if (pass > ui.game_speed) {
		wait = tick;
		do_tick();
		ticks++;
	}
	if (ticks >= 10) {
		ticks = 0;
		do_phase();
		phases++;
	}
	if (phases >= 10) {
		phases = 0;
		do_turn();
		turns++;
	}
}

int rune_main(int argc, char* argv[]) {
	int width = 640;
	int height = 480;

    SDL_Window* window;
	SDL_Renderer *screen;
	SDL_DisplayMode *display = NULL;
	SDL_DisplayMode basic;

	Uint32 fullscreen = 0;// SDL_WINDOW_FULLSCREEN;

	/* Init SDL */
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "SDL initialization failed: %s\n", SDL_GetError());
		exit(-1);
	}

	/* Pick resolution */
	basic.w = width;
	basic.h = height;
#if 1
	if (fullscreen)	display = getFullscreenSize( &basic);
	else        	display = getWindowSize( &basic);
#endif
	if (display == NULL) {
		fprintf(stderr, "Warning: can't find suitable display mode, reverting to 640x480.\n");
		display = &basic;
	}

    /* Create window */
    window = SDL_CreateWindow("RuneMen",
                    600, SDL_WINDOWPOS_CENTERED,
                    display->w, display->h,
                    SDL_WINDOW_SHOWN | fullscreen);

	if (window == NULL) {
		fprintf(stderr, "Window creation failed: %s\n", SDL_GetError());
		SDL_Quit();
		exit(-1);
	}

	SDL_GetWindowSize(window, &width, &height);

    /* Create renderer */
    screen = SDL_CreateRenderer(window, -1, 0);

	if (screen == NULL) {
		fprintf(stderr, "Renderer creation failed: %s\n", SDL_GetError());
		SDL_DestroyWindow(window);
		SDL_Quit();
		exit(-1);
	}

    /* Scale the window / ui */
    init_ui(screen, width, height);

	/* Draw lines, points and rects with alpha blending */
    SDL_SetRenderDrawBlendMode(screen, SDL_BLENDMODE_BLEND); 

    ASS_Init(screen);

	/* Turn on SDL mixer */
	init_sound();

	inrenderer(screen);
	prepare_inline_font();	

	//test music
	ASS_Sound *music = ASS_LoadSound("data/music/reavers.ogg");
	Mix_PlayChannel(-1, music, 0);

	int next = mainmenu_loop(screen);
	if (next == -1) goto done;	
	int loops = 300000;	

	init_game();

//log system info
	log_addf(&gamelog, "Window size: %d x %d", width, height);

	while (loops)
	{
	    SDL_Event event = { 0 };
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) loops = 0;
			if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) loops = 0;
			do_event(&event);
		}

		time_tick();

		draw_screen(screen);

		ui.fps = count_fps();

        SDL_RenderPresent(screen);
		SDL_Delay(10);
	}
done:
	fprintf(stdout, "Normal termination...\n");

	ASS_Quit();
	kill_inline_font();
	SDL_DestroyRenderer(screen);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
