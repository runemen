#include <SDL.h>
#include "game.h"
#include "rune.h"
#include "runeconf.h"
#include "ui.h"

SDL_Rect ui_pane = { -PANE_WIDTH - 10, 0, PANE_WIDTH + 10, -0 };
SDL_Rect top_pane = { 0, 0, -0, 20 };
SDL_Rect game_map = { 0, 20, -PANE_WIDTH, -20 };

SDL_Rect minimap = { -PANE_WIDTH, 20, PANE_WIDTH, 128+4 }; 
SDL_Rect buildbox = { -PANE_WIDTH, 154, PANE_WIDTH, 48 };
SDL_Rect selbox = { -PANE_WIDTH, 154, PANE_WIDTH, 128 };

SDL_Rect unitpin = { -24, 154 + 4, 16, 16 };
SDL_Rect flagicon = { -24, 112, 16, 16 };
SDL_Rect tabicon = { -PANE_WIDTH + 8, 0, 40, 8 };
SDL_Rect pinbox = { -PANE_WIDTH, 190, PANE_WIDTH, 150 };
SDL_Rect hintbox = { -PANE_WIDTH, -128, PANE_WIDTH, 128 };

SDL_Rect plusbtn = { -42, 154 + 31, 16, 16 };

ui_t ui;

void adjust_boxes() {

	SDL_Rect *boxes[] = {
		&minimap, &buildbox, &selbox, &pinbox, &hintbox,

		&unitpin, &plusbtn, &flagicon, &tabicon, 

		&ui_pane, &top_pane, &game_map,
	};
	int num = sizeof(boxes) / sizeof(SDL_Rect*);

	int i;

	for (i = 0; i < num; i++) {

		SDL_Rect *box = boxes[i];

		if (box->x < 0) box->x += ui.log_width;
		if (box->y < 0) box->y += ui.log_height;
		if (box->w < 0) box->w += ui.log_width;
		if (box->h < 0) box->h += ui.log_height;

	}

	buildbox.y = minimap.y + minimap.h + 1;

	ui_pane.h = ui.log_height;
	top_pane.w = ui.log_width;

	unitpin.y = selbox.y + 4; 
	plusbtn.y = selbox.y + 31;
	flagicon.y += selbox.y;
	tabicon.y += selbox.y;
	tabicon.y += selbox.h;
}

/* Do horrible things... */
void adjust_ui() {

	ui.draw_minimap = 0;

	ui.draw_overlays = 1;

	ui.no_mouse = 1;

}

void init_ui(SDL_Renderer *renderer, int w, int h) {

	ui.log_width = w;
	ui.log_height = h;

	/* wood box */
	ui.draw_uibg = 1;

	if (w % 320 == 0 && h % 240 == 0) { /* 320 x 240 base, good old VGA */
		ui.log_width = 640;
		ui.log_height = 480;
	}

	if (w % 800 == 0 && h % 600 == 0) { /* 800 x 600 base, good old SVGA */
		ui.log_width = 400;
		ui.log_height = 300;	
	}

	if (w % 320 == 0 && h % 188 == 0) { /* 320 x 188 base -- wicked, but acceptable */
		ui.log_width = 640;
		ui.log_height = 376;
	}

	if (w % 240 == 0 && h % 160 == 0) { /* 240 x 160 base -- very small screen */
		ui.log_width = 480;
		ui.log_height = 320;
	}
#if 0
	if (h < 400) { /* Very small screen, double the zoom */

		ui.log_width /= 2;
		ui.log_height /= 2;
	
	} 
#endif
	if (ui.log_height < 400) { /* Very small base, adjust gui accordingly */

		minimap.y = 0;	

		hintbox.y = minimap.y;

		selbox.y = ui.log_height - selbox.h;

		ui.draw_uibg = 0;

		buildbox.w = TILE_W * 4;
		buildbox.x = ui.log_width - buildbox.w - TILE_W/2;

		buildbox.h = ui.log_height - buildbox.y; 
	}

	SDL_RenderSetLogicalSize(renderer, ui.log_width, ui.log_height);

	adjust_boxes();
#ifdef MOBILE_OS
	ui.no_mouse = 1;
#else
	ui.no_mouse = 0;
#endif
}

void reset_ui() {
	ui.x = 0;
	ui.y = 0;
	
	ui.vx = 0;
	ui.vy = 0;

	ui.dragging = 0;

	ui.unit = ui.house = ui.flag = -1;
	ui.builder = ui.setflag = ui.stat = ui.btn = -1;

	ui.hintType = -1;

	ui.hover_top = overNothingMajor;
	ui.hover = overNothingMinor;

	/* layers */
	ui.draw_fog = 0;
	ui.draw_path = 0;
	ui.draw_pools = 0;
	ui.draw_overlays = 0;
	ui.draw_scent = 0;
	ui.draw_log = 1;

	/* debug */
	ui.game_speed = 25;
	ui.fps = 0;

	/* for touch/small-screen devices, use different ui */
#ifdef MOBILE_OS
	adjust_ui();
#endif
}

void update_hintbox() {

	switch (ui.hover) {
	
		case overBuildButton:

			ui.hintType = 0;
			ui.hint = ui.hover_id;
			break;

		case overUnitStat:

			ui.hintType = 1;
			ui.hint = ui.hover_id;
			break;

		default:
			ui.hintType = -1;
			ui.hint = -1;
			break;
	}

	/* Hacks */
	if (ui.builder != -1) {

		ui.hintType = 0;
		ui.hint = ui.builder;
	
	}
	if (ui.stat != -1) {
	
		ui.hintType = 1;
		ui.hint = ui.stat;	
	
	}
	
}

void track_mouse() {

	/* Given X and Y mouse position, determine various UI-related states... */


	ui.hover = overNothingMinor;
	ui.hover_id = 0;

	if (SDL_InBounds(ui.x, ui.y, &ui_pane)) {

		ui.hover_top = overPane;

		track_mouse_ui();

	} else if (SDL_InBounds(ui.x, ui.y, &top_pane)) {

		ui.hover_top = overNothingMajor;

	} else {

		ui.hover_top = overMap;	

		track_mouse_map();

	}

	update_hintbox();
}

void track_mouse_map() {

	Uint32 unit_i, house_i, flag_i;

	ui.hover_xcollide = 0;
	ui.hover = overNothingMinor;

	if (ui.builder != -1) {
		house_p *h = &bhouses[ui.builder];	

		int lx = ui.x + ui.vx - game_map.x;
		int ly = ui.y + ui.vy - game_map.y;

		int cx = -(TILE_W * h->w / 2) + (TILE_W/2);
		int cy = -(TILE_H * h->h / 2) + (TILE_H/2);

		lx += cx;
		ly += cy;

		ui.hover_tx = (lx / TILE_W);
		ui.hover_ty = (ly / TILE_H);

		ui.hover_xcollide = xcollide(ui.hover_tx, ui.hover_ty, h->w, h->h);	

		return;
	}

	ui.hover_tx = (ui.x + ui.vx - game_map.x) / TILE_W;
	ui.hover_ty = (ui.y + ui.vy - game_map.y) / TILE_H;

	if (ui.setflag != -1) {

		ui.hover_xcollide = xcollide(ui.hover_tx, ui.hover_ty, 1, 1);	

		if (find_flagT(ui.hover_tx, ui.hover_ty) != -1) ui.hover_xcollide = 2;
	}

	unit_i = find_unit(ui.x, ui.y);
	if (unit_i != -1) {
		ui.hover = overUnit;
		ui.hover_id = unit_i;
		return;
	}
	house_i = find_house(ui.x, ui.y);
	if (house_i != -1) {
		ui.hover = overHouse;
		ui.hover_id = house_i;
		if (houses[house_i].flag_id[ui.faction] != -1
		&& ui.hover_tx == houses[house_i].x
		&& ui.hover_ty == houses[house_i].y) {
			ui.hover = overHouseFlag;
			ui.hover_id = ui.faction;
		}
		return;
	}
	flag_i = find_flagT(ui.hover_tx, ui.hover_ty);
	if (flag_i != -1) {
		ui.hover = overFlag;
		ui.hover_id = flag_i;
		return;
	}
}

void track_mouse_ui() {

	int i;

	/* Minimap */
	if (SDL_InBounds(ui.x, ui.y, &minimap)) {

		ui.hover = overMinimap;	

		return;
	}

	/* Top-level menu (i.e. build buttons and unit-list) */
	if (ui.house == -1 && ui.unit == -1 && ui.flag == -1) {
	/* Name on the pinbox */
		if (SDL_InBounds(ui.x, ui.y, &pinbox)) {
		
			ui.hover = overPinBox;
			ui.hover_id = -1;

			SDL_Rect line = { 0, 0, 120, 16 };
			SDL_Rect pinbtn = { 0, 0, 16, 16 };	
	
			line.x = pinbox.x;
			line.y = pinbox.y;
			pinbtn.x = pinbox.x + line.w;
			pinbtn.y = pinbox.y;
	
			for (i = 0; i < num_units; i++) {
				unit_t *u = &units[i];
				if (u->tile && u->pin) {
	
					if (SDL_InBounds(ui.x, ui.y, &line)) {
						ui.hover = overListName;
						ui.hover_id = i;
						return;
					}
					if (SDL_InBounds(ui.x, ui.y, &pinbtn)) {
						ui.hover = overListPin;
						ui.hover_id = i;
						return;
					}
					line.y += line.h;
					pinbtn.y += line.h;
				}
			}
	
			return;
		}	

	}

	/* One of the build buttons */
	if (ui.house == -1 && ui.unit == -1 && ui.flag == -1) {
		SDL_Rect button = {
			buildbox.x,
			buildbox.y,
			16,
			16,
		};
		for (i = 0; i < 2; i++) {
			if (SDL_InBounds(ui.x, ui.y, &button)) {

				ui.hover = overFlagButton;
				ui.hover_id = i;

				return;
			}

			button.x += 17;
			if (button.x >= buildbox.x + buildbox.w - TILE_W/2) {
				button.x = buildbox.x;
				button.y += 17;
			}
		}
		for (i = 0; i < HMENU_ITEMS; i++) {
	
			if (SDL_InBounds(ui.x, ui.y, &button)) {

				ui.hover = overBuildButton;
				ui.hover_id = i;

				break;
			}

			button.x += 17;
			if (button.x >= buildbox.x + buildbox.w - TILE_W/2) {
				button.x = buildbox.x;
				button.y += 17;
			}
		}
	}

	/* Something on the flagbox */
	if (ui.flag != -1) {
	
		/* Plus */
		if (SDL_InBounds(ui.x, ui.y, &plusbtn)) {

			ui.hover = overFlagPlus;

			return;	
		}
	
	}

	/* Something on the housebox */
	if (ui.house != -1) {
	
		/* Flag icon */
		if (SDL_InBounds(ui.x, ui.y, &flagicon)) {

			ui.hover = overHouseFlag;
			ui.hover_id = 0;

			return;
		}


		/* One of the tab icons */
		SDL_Rect button;
		button.x = tabicon.x;
		button.y = tabicon.y;
		button.w = tabicon.w;
		button.h = tabicon.h;
		for (i = 0; i < MAX_HOUSETABS; i++) {
			if (SDL_InBounds(ui.x, ui.y, &button)) {
				ui.hover = overHouseTab;
				ui.hover_id = i;
				return;
			}
			button.x += tabicon.w;
			if (button.x + tabicon.w >= ui.log_width) {
				button.x = tabicon.x;
				button.y += tabicon.h;
			}
		}

	}

	/* Something on the unitbox */
	if (ui.unit != -1) {

		/* Pin */
		if (SDL_InBounds(ui.x, ui.y, &unitpin)) {

			ui.hover = overUnitPin;
			ui.hover_id = ui.unit;

			return;	
		}

		/* Flag icon */
		if (SDL_InBounds(ui.x, ui.y, &flagicon)) {

			ui.hover = overUnitFlag;
			ui.hover_id = 0;

			return;
		}

	}

	/* One of the tab icons */
	if (ui.unit != -1) {
		SDL_Rect button;
		button.x = tabicon.x;
		button.y = tabicon.y;
		button.w = tabicon.w;
		button.h = tabicon.h;
		for (i = 0; i < MAX_UNITTABS; i++) {
			if (SDL_InBounds(ui.x, ui.y, &button)) {
				ui.hover = overUnitTab;
				ui.hover_id = i;
				return;
			}
			button.x += tabicon.w;
			if (button.x + tabicon.w >= ui.log_width) {
				button.x = tabicon.x;
				button.y += tabicon.h;
			}
		}
	}

	/* One of the stat/skill buttons */
	if (ui.unit != -1) {
		SDL_Rect button = {
			selbox.x + BOX_PADDING + BOX_PADDING,
			selbox.y + BOX_PADDING + BOX_PADDING + BOX_PADDING + 1,
			16,
			13,
		};

		for (i = 0; i < MAX_STAT; i++) {
			button.y += 14;
			if (SDL_InBounds(ui.x, ui.y, &button)) {
				ui.hover = overUnitStat;
				ui.hover_id = i;
			}
		}

		button.y -= 14 * MAX_STAT;
		button.x += 64;

		for (i = 0; i < MAX_STAT; i++) {
			button.y += 14;
			if (SDL_InBounds(ui.x, ui.y, &button)) {
				ui.hover = overUnitSkill;
				ui.hover_id = i;
			}
		}
	}
	
}