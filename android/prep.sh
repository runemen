#!/bin/sh

echo " * * * "
echo "Preparing android build directory"
echo "You'll need android-sdk, android-ndk, SDL2 sources and ant"
echo "You'll also need open-jdk and open-jre"
echo " * * * "

# See if user has 'ant' installed
if [ `which ant` = "" ]; then
    echo "Can't find `ant` program."
    echo "Install it using your package manager."
    echo "NOTE: If `ant-optinal` exists in your package manager, install it too."
    exit 1
fi

## NDK
# See if NDK is in the Makefile.local already
NDK_BUILD=`cat Makefile.local | grep NDK_BUILD= | sed 's/NDK_BUILD=//'`
if [ "$NDK_BUILD" = "" ]; then
    echo "ndk-build not found in Makefile.local"

    # See if NDK is in the PATH
    NDK_BUILD=`which ndk-build`

    if [ "$NDK_BUILD" = "" ]; then
	echo "ndk-build not found in PATH."

	echo -n "Enter path to android-ndk:"
	read -r TMP
	NDK=`echo ${TMP} | sed 's/\/$//'` # remove trailing slash

	NDK_BUILD="${NDK}/ndk-build"
    else
	echo "Found ndk-build at $NDK_BUILD"
    fi

else
    echo "Found ndk-build in Makefile.local, $NDK_BUILD"
fi

# Verify NDK-BUILD is correct
NDK_TEST=`${NDK_BUILD} --version | grep GNU`
if [  "${NDK_TEST}" != "" ]; then
    echo "(re)Writing Makefile.local"
    echo "NDK_BUILD=${NDK_BUILD}" > Makefile.local
    echo "WWW_PATH=/tmp" >> Makefile.local
else
    echo "Path to ndk-build is wrong."
    exit 1
fi

## SDK
# See if SDK is in the local.properties already
ANDROID_SDK_PATH=`cat local.properties | grep sdk.dir= | sed 's/sdk\.dir=//'`
if [ "$ANDROID_SDK_PATH" = "" ]; then
    # Or ask user to enter it
    echo -n "Enter path to android-sdk:"
    read -r TMP
    ANDROID_SDK_PATH=`echo ${TMP} | sed 's/\/$//'` # remove trailing slash
else
    echo "Using sdk.dir from local.properties."
fi

# See if SDK path is correct
if [ -f ${ANDROID_SDK_PATH}/platform-tools/adb ]; then
    # WRITE SDK PATH
    echo "(re)Writing local.properties"
    echo "sdk.dir=$ANDROID_SDK_PATH" > local.properties
    echo "target=android-17" >> local.properties
else
    echo "Error: Unable to find ${ANDROID_SDK_PATH}/platform-tools/adb"
    echo "Aborting"
    exit 1
fi

## SDL2 sources

# See if it's already symlinked/copied
if [ -f jni/SDL/README.android ]; then
    echo "SDL2 sources found"
else
    echo -n "Enter path to SDL source dir:"
    read -r TMP
    SDL_SOURCE_PATH=`echo ${TMP} | sed 's/\/$//'` # remove trailing slash

    # Verify SDL_SOURCE_PATH is correct
    if [ -f "${SDL_SOURCE_PATH}/README.android" ]; then
        echo "Symlinking to jni/SDL"
        ln -s ${SDL_SOURCE_PATH} jni/SDL
    else
	echo "Error: Unable to find ${SDL_SOURCE_PATH}/README.android"
        echo "Aborting"
        exit 1
    fi

fi
