#!/bin/sh

ANDROID_SDK_PATH=`cat local.properties | grep sdk.dir= | sed s/sdk\.dir=//`
ADB_PATH=${ANDROID_SDK_PATH}/platform-tools/adb

${ADB_PATH} shell am start -a android.intent.action.MAIN -n net.mindloop.sdl/.RuneMen
