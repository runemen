LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include

RUNE_PATH := ../../../src

# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.cpp \
	android_main.c \
	$(RUNE_PATH)/libs/binaryheap/binhl.c \
	$(RUNE_PATH)/libs/libcsslike/hash/hash.c \
	$(RUNE_PATH)/libs/libcsslike/hash/stash.c \
	$(RUNE_PATH)/libs/libcsslike/cssdom.c \
	$(RUNE_PATH)/libs/lazyass/lazyass.c \
	$(RUNE_PATH)/sdl2-x.c $(RUNE_PATH)/font2.c $(RUNE_PATH)/SDL2_particles.c \
	$(RUNE_PATH)/mainmenu.c \
	$(RUNE_PATH)/ai.c \
 	$(RUNE_PATH)/utils.c \
 	$(RUNE_PATH)/draw.c \
 	$(RUNE_PATH)/mana.c \
 	$(RUNE_PATH)/game.c \
 	$(RUNE_PATH)/ui.c \
 	$(RUNE_PATH)/runeconf.c \
 	$(RUNE_PATH)/rune2.c

LOCAL_SHARED_LIBRARIES := SDL2

LOCAL_LDLIBS := -lGLESv1_CM -llog

include $(BUILD_SHARED_LIBRARY)
